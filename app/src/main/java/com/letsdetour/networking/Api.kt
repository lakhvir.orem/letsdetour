package com.letsdetour.networking

import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.WebUrls
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class Api {

    companion object{
        val instance= Api().getRetrofitService()
    }
fun getRetrofitService(): RetrofitApi {
    val interceptor = HttpLoggingInterceptor()
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
    val httpClient = OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .readTimeout(30, TimeUnit.SECONDS)
        .connectTimeout(30, TimeUnit.SECONDS)
        .build()
    val retrofit=Retrofit.Builder().baseUrl(WebUrls.PLACES_API_URL).client(httpClient).addConverterFactory(GsonConverterFactory.create()).build()
    return retrofit.create(RetrofitApi::class.java)
}

}