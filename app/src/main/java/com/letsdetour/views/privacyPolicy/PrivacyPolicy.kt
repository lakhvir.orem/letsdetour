package com.letsdetour.views.privacyPolicy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.PrivacyPolicyBinding
import com.letsdetour.views.mainActivity.MainActivity

class PrivacyPolicy:Fragment() {
    lateinit var privacyPolicyVm: PrivacyPolicyVm
    lateinit var privacyPolicyBinding: PrivacyPolicyBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        privacyPolicyBinding= PrivacyPolicyBinding.inflate(LayoutInflater.from(context))
        return privacyPolicyBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        privacyPolicyVm=PrivacyPolicyVm(requireContext(),this)
        privacyPolicyBinding.privacyPolicyVm=privacyPolicyVm
        MainActivity.mainBinding.btmNav.visibility=View.GONE
        MainActivity.mainBinding.ivAddHome.visibility=View.GONE
    }
}