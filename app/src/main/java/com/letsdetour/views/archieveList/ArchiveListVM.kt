package com.letsdetour.views.archieveList

import android.app.Activity
import android.content.Context
import androidx.lifecycle.ViewModel

class ArchiveListVM(val context: Context):ViewModel() {
    var archieveListAdapter=ArchieveListAdapter()
    fun onClickBack(){
        (context as Activity).onBackPressed()
    }
}