package com.letsdetour.views.archieveList

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.letsdetour.R
import com.letsdetour.databinding.ActivityArchiveListBinding

class ArchiveList : AppCompatActivity() {
    lateinit var archiveListVM: ArchiveListVM
    lateinit var archiveListBinding: ActivityArchiveListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        archiveListBinding=DataBindingUtil.setContentView(this,R.layout.activity_archive_list)
        archiveListVM=ArchiveListVM(this)
        archiveListBinding.archiveListVM=archiveListVM
    }
}