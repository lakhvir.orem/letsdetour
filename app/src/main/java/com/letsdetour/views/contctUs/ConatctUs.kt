package com.letsdetour.views.contctUs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.ContactUsBinding
import com.letsdetour.views.mainActivity.MainActivity

class ConatctUs:Fragment() {
    lateinit var contactUsVM: ContactUsVM
    lateinit var contactUsBinding: ContactUsBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        contactUsBinding= ContactUsBinding.inflate(LayoutInflater.from(context))
        return contactUsBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        contactUsVM= ContactUsVM(requireContext(),this)
        contactUsBinding.contactUsVM=contactUsVM
        MainActivity.mainBinding.btmNav.visibility=View.GONE
        MainActivity.mainBinding.ivAddHome.visibility=View.GONE
    }
}