package com.letsdetour.views.contctUs

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Patterns
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.letsdetour.R
import com.letsdetour.response.MessageRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.mainActivity.MainActivity
import retrofit2.Response

class ContactUsVM(val context: Context, val conatctUs: ConatctUs):ViewModel() {

    var message=ObservableField("")
    var email=ObservableField("")
    var naem=ObservableField("")
    fun onClickBack(){
        (context as Activity).onBackPressed()
    }

    fun validate():Boolean{
        when{
            naem.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(context,conatctUs.contactUsBinding.btLogin,context.getString(R.string.enter_your_name))
                return false
            }
            email.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(context,conatctUs.contactUsBinding.btLogin,context.getString(R.string.enter_your_email))
                return false
            }
            !email.get().toString().trim().matches(Patterns.EMAIL_ADDRESS.pattern().toRegex()) -> {
                MethodsUtil.showSnackBar(
                    context,
                    conatctUs.contactUsBinding.btLogin,
                    context.getString(R.string.please_enter_your_valid_email)
                )
                return false
            }
            message.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(context,conatctUs.contactUsBinding.btLogin,context.getString(R.string.enter_your_message))
                return false
            }
            else->{
                return true

            }
        }
    }

    fun contactUs() {
        if (validate()){
            if (MethodsUtil.isNetworkAvailable(context)) {
                try {
                    var token="Bearer "+PrefferenceFile.retrieveKey(context,CommonKeys.TOKEN).toString()
                    RetrofitCall.callService(context, true, token, object :
                        RequestProcess<Response<MessageRes>> {
                        override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                            return retrofitApi.contactAdmin(naem.get().toString(),email.get().toString(),message.get().toString())
                        }
                        override fun onResponse(res: Response<MessageRes>) {
                            if (res.isSuccessful) {
                                val response = res.body()!!
                                showToast(context,response.message)
                                context.startActivity(Intent(context,MainActivity::class.java))
                                (context as Activity).finishAffinity()
                            }
                            else {
                                showToast(context,res.message().toString())
                            }
                        }

                        override fun onException(message: String) {}
                    })
                } catch (e: Exception) {
                    e.printStackTrace()
                }


            } else {
                CommonAlerts.alert(context, context.getString(R.string.internet_issue))
            }
        }

    }
}