package com.letsdetour.views.setting

import android.app.Activity
import android.content.Context
import androidx.annotation.NonNull
import androidx.lifecycle.ViewModel
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions

import com.letsdetour.utils.MethodsUtil
import com.letsdetour.utils.PrefferenceFile
import com.letsdetour.utils.diconnectFromFb
import com.letsdetour.views.changePassword.ChangePassword


class SettingVM(val context: Context):ViewModel() {


    fun onClickBack(){
        (context as Activity).onBackPressed()
    }

    fun onCLickSetting(){
        MethodsUtil.loadFragment(context, ChangePassword())
    }

    fun onClickLogout(){
        diconnectFromFb()
/*        Auth.GoogleSignInApi.signOut(googleApiClient)
            .setResultCallback {
                PrefferenceFile.logout(context)
            }*/

        val gsoBuilder = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail()
        (context as Activity)?.let {
            GoogleSignIn.getClient(context as Activity, gsoBuilder.build())?.signOut()
        }

      //  signInClient=GoogleSignInClient()
        PrefferenceFile.logout(context)
    }
}