package com.letsdetour.views.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.Bindable
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.SettingBinding
import com.letsdetour.views.mainActivity.MainActivity

class Setting:Fragment() {
    lateinit var settingVM: SettingVM
    lateinit var settingBinding: SettingBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        settingBinding= SettingBinding.inflate(LayoutInflater.from(context))
        return settingBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        settingVM= SettingVM(requireContext())
        settingBinding.settingVM=settingVM
        MainActivity.mainBinding.ivAddHome.visibility=View.GONE
        MainActivity.mainBinding.btmNav.visibility=View.GONE
    }
}