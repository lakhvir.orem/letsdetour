package com.letsdetour.views.userList

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import com.letsdetour.R
import com.letsdetour.response.FollowerListRes
import com.letsdetour.response.MessageRes
import com.letsdetour.response.TravelPlanDetailsRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.forwardEmail.ForwardEmail
import com.letsdetour.views.mainActivity.MainActivity
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class UserListVM(val context: Context, val userList: UserList) : ViewModel() ,FollowerCheckedInterface {
    var userListAdaper = UserListAdaper(context,this)
    var ListEmpty=ObservableBoolean(false)
    var useridList = ArrayList<String>()
    var followerList=""
    var planid=""
    var permissionType="1"
    var sharePlanList=ArrayList<FollowerListRes.Data>()
    fun onClickBack() {
        (context as Activity).onBackPressed()
    }

    companion object{
        var isTravelPlanUsers=false
    }

    init {
        UserListAdaper.followerCheckedInterface=this
        searchData()
        getBundleData()
    }

    fun getBundleData(){
        if (userList.arguments!=null){
            var data=userList.arguments
            if (data?.containsKey(CommonKeys.TRAEVELID)!!){
                planid=data.getString(CommonKeys.TRAEVELID).toString()
                isTravelPlanUsers=false
                getFollowers()

            }
            if (data?.containsKey(CommonKeys.FOLLOWERSLIST)){
                planid=data.getString(CommonKeys.TRAEVELID2).toString()
                var list=data?.getParcelableArrayList<TravelPlanDetailsRes.Data.Followers>(CommonKeys.FOLLOWERSLIST)
                Log.e("FollowerList","whichPlan"+list.toString())
                if (list!=null){
                    for (i in 0 until list.size){
                        var model=FollowerListRes.Data()
                        model.id=list[i].id
                        if (list[i].user_image!=null){
                            model.user_image= list[i].user_image.toString()
                        }
                        model.user_name=list[i].user_name
                        model.user_id=list[i].user_id
                        model.follower_id=list[i].follower_id.toInt()
                        sharePlanList.add(model)
                    }
                    userListAdaper.addDataInlist(sharePlanList)
                    isTravelPlanUsers=true
                    userList.userListBinding.tvTitleName.text="UserList"
                    userList.userListBinding.tvSeachData.visibility=View.GONE
                    userList.userListBinding.btLogin.visibility=View.GONE
                }

            }
        }
    }
    private fun  searchData(){
        userList.userListBinding.etSearchName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(s: CharSequence, i: Int, i1: Int, i2: Int) {
                try {
                    userListAdaper.filter(s.toString())

                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun afterTextChanged(editable: Editable) {
            }
        })
    }

    fun getFollowers() {
        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token =
                    "Bearer " + PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<FollowerListRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<FollowerListRes> {
                        return retrofitApi.follower_list("1")
                    }

                    override fun onResponse(res: Response<FollowerListRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            var list = response.data

                            userListAdaper.addDataInlist(list)
                        } else {
                            showToast(context, res.message().toString())
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }

    }

    fun alertForPermission(){
        val builder = AlertDialog.Builder(context)
        val customLayout: View = LayoutInflater.from(context)
            .inflate(
                com.letsdetour.R.layout.alert_for_permission,
                null
            )
        builder.setView(customLayout)
        var dialog=builder.create()
        dialog.getWindow()?.setBackgroundDrawableResource(android.R.color.transparent);

        val view = customLayout.findViewById(com.letsdetour.R.id.tvView1) as TextView
        val edit = customLayout.findViewById(com.letsdetour.R.id.tvEdit) as TextView

        view.setOnClickListener {
            permissionType="1"
            dialog.dismiss()
            sharePlan("0")
        }
        edit.setOnClickListener {
            permissionType="2"
            dialog.dismiss()
            sharePlan("0")
        }
        dialog.show()
    }


    fun sharePlan(status:String) {
        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token =
                    "Bearer " + PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.addPlanFollower(planid,followerList,status,permissionType)
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            showToast(context,response.message.toString())

                        } else {
                            showToast(context, res.message().toString())
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }

    }

    override fun getCheckedValue(userId: String) {
        useridList.add(userId)
        followerList=TextUtils.join(",",useridList)
        Log.e("userList", "Add==>>$followerList")

    }

    override fun removeUserValue(userId: String) {
        if (useridList.contains(userId)){
            useridList.remove(userId)
            followerList=  TextUtils.join(",",useridList)
            Log.e("userList", "Remove==>>$followerList")

        }
    }
}