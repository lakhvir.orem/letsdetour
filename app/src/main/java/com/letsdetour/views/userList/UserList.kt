package com.letsdetour.views.userList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.UserListBinding

class UserList:Fragment() {
    lateinit var userListVM: UserListVM
    lateinit var userListBinding: UserListBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        userListBinding= UserListBinding.inflate(LayoutInflater.from(context))
        return userListBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userListVM= UserListVM(requireContext(),this)
        userListBinding.userListVM=userListVM
    }
}