package com.letsdetour.views.userList

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.letsdetour.R
import com.letsdetour.response.FollowerListRes
import com.letsdetour.response.MessageRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.profile.Profile
import kotlinx.android.synthetic.main.followers_adapter.view.*
import kotlinx.android.synthetic.main.userlist_adapter.view.*
import kotlinx.android.synthetic.main.userlist_adapter.view.civFolow
import retrofit2.Response

class UserListAdaper(val context: Context,val userListVM: UserListVM) :BaseAdapter(R.layout.userlist_adapter) {

    var followerList=ArrayList<FollowerListRes.Data>()
    var tempList=ArrayList<FollowerListRes.Data>()

    companion object{
        var followerCheckedInterface:FollowerCheckedInterface?=null
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.apply {
            followerList.let {
                tvFollowTick.text=followerList[position].user_name
                Glide.with(context).load(followerList[position].user_image).error(R.drawable.user_avatar).into(civFolow)
            }

            if (UserListVM.isTravelPlanUsers){
                ivChecked.visibility= View.GONE
                ivDelete.visibility= View.VISIBLE
            }else{
                ivChecked.visibility= View.VISIBLE
                ivDelete.visibility= View.GONE
            }

            if (followerList[position].isChecked){
                ivChecked.setImageResource(R.drawable.ic_round_checkbox_selected)

            }else{
                ivChecked.setImageResource(R.drawable.ic_round_checkbox_normal)

            }

            ivDelete.setOnClickListener {
                removeFollowers(followerList[position].follower_id.toString(),userListVM.planid.toInt(),position)
            }

            ivChecked.setOnClickListener {
                if (followerList[position].isChecked){
                    followerList[position].isChecked=false
                    ivChecked.setImageResource(R.drawable.ic_round_checkbox_normal)
                    notifyDataSetChanged()
                    followerCheckedInterface!!.removeUserValue(followerList[position].user_id.toString())
                }else{
                    followerList[position].isChecked=true
                    notifyDataSetChanged()
                    ivChecked.setImageResource(R.drawable.ic_round_checkbox_selected)
                    followerCheckedInterface!!.getCheckedValue(followerList[position].user_id.toString())

                }
            }

            civFolow.setOnClickListener {
                var bundle=Bundle()
                bundle.putString(CommonKeys.OtherProfile,"other")
                bundle.putString(CommonKeys.USERID,followerList[position].user_id.toString())
                var profile=Profile()
                profile.arguments=bundle
                MethodsUtil.loadFragment(context,profile)
            }
        }
    }

    override fun getItemCount(): Int {
        return followerList.size

    }

    fun addDataInlist(list: List<FollowerListRes.Data>) {
        if (!list.isNullOrEmpty()){
            followerList.clear()
            tempList.clear()
            followerList.addAll(list)
            tempList.addAll(list)
            notifyDataSetChanged()
        }
    }

    fun removeFollowers(folloerId: String?, planId: Int,position: Int) {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token="Bearer "+ PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.removePlanFollower(folloerId.toString(),planId.toString())
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        val response = res.body()
                        if (res.isSuccessful && res.code()==200) {
                            Log.e("RemoveFollowersRes", "==>>$response")
                            showToast(context,response?.message.toString())
                            followerList.removeAt(position)
                            tempList.removeAt(position)
                            notifyDataSetChanged()
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }

    fun filter( name:String){

        Log.e("Checkanem",name+tempList.size)
        if (name.isNotEmpty()){
            followerList.clear()
            for (items in tempList){

                Log.e("itemsFriendlyName",items.user_name)
                if (items.user_name.toString().toLowerCase().contains(name.toLowerCase())){
                    followerList.add(items)
                }

            }
            if (followerList.isNotEmpty()){
                userListVM.ListEmpty.set(false)
            }else{
                userListVM.ListEmpty.set(true)
            }
            notifyDataSetChanged()
        }

        else
        {

            followerList.clear()
            followerList.addAll(tempList)
            notifyDataSetChanged()

        }

    }
}