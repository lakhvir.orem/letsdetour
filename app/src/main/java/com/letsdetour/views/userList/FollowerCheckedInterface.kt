package com.letsdetour.views.userList

interface FollowerCheckedInterface {
    fun getCheckedValue(userId:String)
    fun removeUserValue(userId:String)

}