package com.letsdetour.views.featuredListing

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.FeaturedListingBinding
import com.letsdetour.response.TravelPlanDetailsRes
import com.letsdetour.views.mainActivity.MainActivity

class FeaturedListing:Fragment(),ScrollrecyelerInterface {
    lateinit var featuredListingVM: FeaturedListingVM
    lateinit var featuredListingBinding: FeaturedListingBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        featuredListingBinding= FeaturedListingBinding.inflate(LayoutInflater.from(context))
        return featuredListingBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        featuredListingVM=FeaturedListingVM(requireContext())
        featuredListingBinding.featuredListingVM=featuredListingVM
        FeatureListAdapter.scrollrecyelerInterface=this
        MainActivity.mainBinding.btmNav.visibility=View.GONE
        MainActivity.mainBinding.ivAddHome.visibility=View.GONE

    }

    override fun scrollToPos(pos: Int, routes: List<TravelPlanDetailsRes.Data.Date.Route>) {
        if (pos==2 || pos==3){
            featuredListingBinding.rvLeftRight.smoothScrollToPosition(3)
        }
        if (pos==1){
            featuredListingBinding.rvLeftRight.smoothScrollToPosition(0)
        }
    }


}