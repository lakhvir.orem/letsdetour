package com.letsdetour.views.featuredListing

import android.content.Context
import com.letsdetour.R
import com.letsdetour.utils.BaseAdapter
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.views.featuredDetails.FeaturedDetails


class FeaturedListAdapter2(val context: Context): BaseAdapter(R.layout.featured_adapter_layout) {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            MethodsUtil.loadFragment(context,FeaturedDetails())
        }
    }

    override fun getItemCount(): Int {
        return 5
    }
}