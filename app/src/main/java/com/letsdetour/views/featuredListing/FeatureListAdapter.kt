package com.letsdetour.views.featuredListing

import android.content.Context
import android.graphics.Typeface
import android.view.View
import com.letsdetour.R
import com.letsdetour.response.TravelPlanDetailsRes
import com.letsdetour.utils.BaseAdapter
import kotlinx.android.synthetic.main.featured_static_listing.view.*

class FeatureListAdapter(val context: Context, val list: ArrayList<TravelPlanDetailsRes.Data.Date>, val requiredPos: Int): BaseAdapter(R.layout.featured_static_listing) {

    var selectedPos=0
    companion object{
        var scrollrecyelerInterface:ScrollrecyelerInterface?=null
    }

    init {
        selectedPos=requiredPos
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.apply {
            tvRestaurant.text=list[position].plan_date
            if (selectedPos==0){
                holder.itemView.tvRestaurant.typeface = Typeface.DEFAULT_BOLD;
                holder.itemView.tvRestaurant.setTextColor(context.resources.getColor(R.color.black))
                holder.itemView.tvFeatureView.visibility=View.VISIBLE
                holder.itemView.tvRestaurant.typeface = Typeface.DEFAULT_BOLD;
            }
        }


        holder.itemView.setOnClickListener {
            selectedPos=position
            scrollrecyelerInterface!!.scrollToPos(position,list[position].routes)
            notifyDataSetChanged()
        }

        if (selectedPos==position){
            holder.itemView.tvRestaurant.typeface = Typeface.DEFAULT_BOLD;
            holder.itemView.tvRestaurant.setTextColor(context.resources.getColor(R.color.black))
            holder.itemView.tvFeatureView.visibility=View.VISIBLE
        }

        else{
            holder.itemView.tvRestaurant.setTextColor(context.resources.getColor(R.color.light_grey))
            holder.itemView.tvRestaurant.typeface = Typeface.DEFAULT;
            holder.itemView.tvFeatureView.visibility=View.GONE
            holder.itemView.tvRestaurant.typeface = Typeface.DEFAULT_BOLD;
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}