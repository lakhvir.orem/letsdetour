package com.letsdetour.views.featuredListing

import android.app.Activity
import android.content.Context
import androidx.lifecycle.ViewModel
import com.letsdetour.response.TravelPlanDetailsRes
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.views.createPlan.CreatePlan


class FeaturedListingVM(val context: Context):ViewModel() {

    var list=ArrayList<TravelPlanDetailsRes.Data.Date>()

    var featureListAdapter=FeatureListAdapter(context, list, -1)

    var featureListAdapter2= FeaturedListAdapter2(context)

    init {
      /*  list.add("ACCOMODATION")
        list.add("RESTAURANTS")
        list.add("ATTRACTIONS")
        list.add("NIGHTLIFE")
*/
        featureListAdapter.notifyDataSetChanged()
    }
    fun onClickBack(){
        (context as Activity).onBackPressed()
    }

    fun onClickCreatePlan(){
        MethodsUtil.loadFragment(context,CreatePlan())
    }
}