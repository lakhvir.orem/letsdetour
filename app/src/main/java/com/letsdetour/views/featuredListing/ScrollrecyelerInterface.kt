package com.letsdetour.views.featuredListing

import com.letsdetour.response.TravelPlanDetailsRes

interface ScrollrecyelerInterface {
    fun scrollToPos(pos: Int, routes: List<TravelPlanDetailsRes.Data.Date.Route>)
}