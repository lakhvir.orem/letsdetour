package com.letsdetour.views.splash

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.letsdetour.R
import com.letsdetour.utils.CommonKeys
import com.letsdetour.utils.PrefferenceFile
import com.letsdetour.views.mainActivity.MainActivity
import com.letsdetour.views.walkThrough.Walkthrough

class Splash : AppCompatActivity() {
    private var mDelayHandler: Handler? = null
    private var SplashDelay: Long = 1500
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /** Hiding Title bar of this activity screen */
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = Color.TRANSPARENT
        }

        setContentView(R.layout.activity_splash)

        /* val animation1: Animation = AnimationUtils.loadAnimation(
            this,
            R.anim.slide_down
        )
        ivSplash.startAnimation(animation1)*/

        mDelayHandler = Handler()
        mDelayHandler!!.postDelayed(mRunable, SplashDelay)
    }

    private var mRunable: Runnable = Runnable {
        if (!isFinishing) {
            var isUserLogin=PrefferenceFile.retrieveKey(this,CommonKeys.ISUSERLOGIN)
            if (isUserLogin=="yes"){
                startActivity(Intent(this,MainActivity::class.java))

            }else{
                startActivity(Intent(this,Walkthrough::class.java))
            }
            finish()
        }
    }
    override fun onDestroy() {

        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunable)
        }
        super.onDestroy()
    }


}