package com.letsdetour.views.travelPlans

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.letsdetour.R
import com.letsdetour.response.CountryListRes
import com.letsdetour.response.GetPlanListRes
import com.letsdetour.utils.CommonAlerts
import com.letsdetour.utils.CommonKeys
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.utils.PrefferenceFile
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.createPlan.CreatePlan
import retrofit2.Response

class TravelPlansVM(val context: Context, val travelPlans: TravelPlans):ViewModel(){
    var travelAdapter=TravelAdapter(context)

    init {
        getPlanList()
    }

    fun onClickCreatePlan(){
        MethodsUtil.loadFragment(context, CreatePlan())
    }

    private fun getPlanList() {

        if (MethodsUtil.isNetworkAvailable(context)) {
            var token =
                "Bearer " + PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()

            try {
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<GetPlanListRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<GetPlanListRes> {
                        return retrofitApi.getPanList()
                    }

                    override fun onResponse(res: Response<GetPlanListRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.data != null) {
                                var travelList = response.data
                                Log.e("TravelPlanList", "Res$travelList")
                                travelAdapter.addDatainList(travelList)
                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    travelPlans.travelPlansBinding.tvTitleName,
                                    response.message
                                )
                            }
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }


}