package com.letsdetour.views.travelPlans

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.TravelPlansBinding
import com.letsdetour.views.mainActivity.MainActivity

class TravelPlans:Fragment() {
    lateinit var travelPlansVM: TravelPlansVM
    lateinit var travelPlansBinding: TravelPlansBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        travelPlansBinding= TravelPlansBinding.inflate(LayoutInflater.from(context))
        return travelPlansBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        travelPlansVM=TravelPlansVM(requireContext(),this)
        travelPlansBinding.travelPlansVM=travelPlansVM
        MainActivity.mainBinding.btmNav.visibility=View.VISIBLE
        MainActivity.mainBinding.ivAddHome.visibility=View.VISIBLE
        MainActivity.mainBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }
}