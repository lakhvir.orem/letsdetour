package com.letsdetour.views.travelPlans

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.letsdetour.R
import com.letsdetour.databinding.CommonAlertBinding
import com.letsdetour.response.GetPlanListRes
import com.letsdetour.response.MessageRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.createPlan.CreatePlan
import com.letsdetour.views.travelsDetails.TravelDetails
import kotlinx.android.synthetic.main.travel_plans_adapter.view.*
import retrofit2.Response

class TravelAdapter(val context: Context):BaseAdapter(R.layout.travel_plans_adapter) {
    var palnList=ArrayList<GetPlanListRes.Data>()
        var selectedPos=-1
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.apply {
            holder.itemView.let {
                tvTitleName.text=palnList[position].address
                tvDate.text=MethodsUtil.setDateFormatToDisplay(palnList[position].start_date)+" to "+MethodsUtil.setDateFormatToDisplay(palnList[position].end_date)
                tvRate.text=MethodsUtil.getFutureDatesTime(palnList[position].start_date,palnList[position].end_date)
            }
        }
        holder.itemView.ivMore.setOnClickListener {
            selectedPos=position
            notifyDataSetChanged()
        }
        holder.itemView.setOnClickListener {
            selectedPos=-1
            notifyDataSetChanged()
        }
        holder.itemView.setOnClickListener {
            var bundle=Bundle()
            bundle.putString(CommonKeys.TRAEVELID,palnList[position].id.toString())
            var travelDetails=TravelDetails()
            travelDetails.arguments=bundle
            MethodsUtil.loadFragment(context,travelDetails)
        }

        holder.itemView.ivDelete.setOnClickListener {
            alertForDelte("Are you sure to you want to delete this plan","delete",position)
        }
        holder.itemView.ivSharePlan.setOnClickListener {
            share(palnList[position].address)
        }

        holder.itemView.ivUpdate.setOnClickListener {
            selectedPos=-1
            notifyDataSetChanged()
            var bundle=Bundle()
            bundle.putParcelable(CommonKeys.PlanData,palnList[position])
            val createPlans=CreatePlan()
            createPlans.arguments=bundle
            MethodsUtil.loadFragment(context,createPlans)
        }

        if (selectedPos==position){
            holder.itemView.cvOptions.visibility=View.VISIBLE
        }else{
            holder.itemView.cvOptions.visibility=View.GONE
        }
    }

    override fun getItemCount(): Int {
        return palnList.size

    }

    fun addDatainList(list:List<GetPlanListRes.Data>){
        if (!list.isNullOrEmpty()){
            palnList.clear()
            palnList.addAll(list)
            notifyDataSetChanged()
        }
    }

    fun share(url:String) {
        MethodsUtil.hideSoftKeyboard(context as Activity)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, url)
        context.startActivity(Intent.createChooser(intent, "Share via"))
    }

     fun deletePlan(position: Int) {

        if (MethodsUtil.isNetworkAvailable(context)) {
            var token =
                "Bearer " + PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()

            try {
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.deletePlan(palnList[position].id.toString())
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        if (res.code()==200) {
                            showToast(context,res.body()?.message.toString())
                            palnList.removeAt(position)
                            selectedPos=-1
                            notifyDataSetChanged()
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }


    fun alertForDelte(message: String, type: String, position: Int){
        try{
            val builder = AlertDialog.Builder(context)
            val layout = CommonAlertBinding.inflate(LayoutInflater.from(context),null)
            builder.setCancelable(false)
            builder.setView(layout.root)
            val tvMessage = layout.tvMessage
            val clOk = layout.clOk
            val clYes = layout.clYes
            val clNo = layout.clNo
            val dialog = builder.create()

            tvMessage.text = message

            if (type=="delete"){
                clOk.visibility=View.GONE
                clNo.visibility=View.VISIBLE
                clYes.visibility=View.VISIBLE
            }else{
                clOk.visibility=View.VISIBLE
                clNo.visibility=View.GONE
                clYes.visibility=View.GONE
            }

            clOk.setOnClickListener {
                dialog.dismiss()
            }
            clYes.setOnClickListener {
                deletePlan(position)
                dialog.dismiss()
            }

            clNo.setOnClickListener {
                dialog.dismiss()
            }

            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }catch (e:Exception){
            e.printStackTrace()
        }
    }
}