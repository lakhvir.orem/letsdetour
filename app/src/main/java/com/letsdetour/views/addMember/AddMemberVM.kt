package com.letsdetour.views.addMember

import android.app.Activity
import android.content.Context
import androidx.lifecycle.ViewModel

class AddMemberVM(val context: Context):ViewModel() {

    var addMemberAdapter=AddMemberAdapter()

    fun onClickBack(){
        (context as Activity).onBackPressed()
    }
}