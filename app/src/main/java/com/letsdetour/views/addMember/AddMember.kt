package com.letsdetour.views.addMember

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.letsdetour.R
import com.letsdetour.databinding.ActivityAddMemberBinding

class AddMember : AppCompatActivity() {
    lateinit var addMemberVM: AddMemberVM
    lateinit var addMemberBinding: ActivityAddMemberBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addMemberBinding=DataBindingUtil.setContentView(this,R.layout.activity_add_member)
        addMemberVM=AddMemberVM(this)
        addMemberBinding.addMemberVM=addMemberVM
    }
}