package com.letsdetour.views.termsAndConditions

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.os.Build
import android.util.Log
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.lifecycle.ViewModel
import com.letsdetour.utils.dismissProgress
import com.letsdetour.utils.initProgress
import com.letsdetour.utils.showToast

class TermsAndConditionVM(val context: Context, val termsAndConditions: TermsAndConditions):ViewModel() {

    init {
        loadWebView()
    }

    fun onClickBack(){
        (context as Activity).onBackPressed()
    }

    fun loadWebView(){
        initProgress(context)
        termsAndConditions.termsAndConditionsBinding.webViewTerms.settings.javaScriptEnabled = true // enable javascript
        termsAndConditions.termsAndConditionsBinding.webViewTerms.webViewClient = object : WebViewClient() {
            override fun onReceivedError(
                view: WebView?,
                errorCode: Int,
                description: String?,
                failingUrl: String?
            ) {
            }

            override fun onPageCommitVisible(view: android.webkit.WebView?, url: String?) {
                super.onPageCommitVisible(view, url)
                view?.clearCache(true)

                Log.e("Visible","-------------->")
                dismissProgress()
            }


            @TargetApi(Build.VERSION_CODES.M)
            override fun onReceivedError(
                view: WebView?,
                req: WebResourceRequest,
                rerr: WebResourceError
            ) {
// Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(
                    view,
                    rerr.errorCode,
                    rerr.description.toString(),
                    req.url.toString()
                )
            }
        }
        termsAndConditions.termsAndConditionsBinding.webViewTerms.loadUrl("https://dev.appmantechnologies.com/letsdetour/terms")
    }
}