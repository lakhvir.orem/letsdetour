package com.letsdetour.views.termsAndConditions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.TermsAndConditionsBinding
import com.letsdetour.views.mainActivity.MainActivity

class TermsAndConditions:Fragment() {
    lateinit var termsAndConditionVM: TermsAndConditionVM
    lateinit var termsAndConditionsBinding: TermsAndConditionsBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        termsAndConditionsBinding= TermsAndConditionsBinding.inflate(LayoutInflater.from(context))
        return termsAndConditionsBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        termsAndConditionVM= TermsAndConditionVM(requireContext(),this)
        termsAndConditionsBinding.termsAndConditionVM=termsAndConditionVM
        MainActivity.mainBinding.btmNav.visibility=View.GONE
        MainActivity.mainBinding.ivAddHome.visibility=View.GONE
    }
}