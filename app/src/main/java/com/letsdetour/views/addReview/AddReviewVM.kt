package com.letsdetour.views.addReview

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.chaek.android.RatingBar

class AddReviewVM(val context: Context):ViewModel() {

    fun onClickBack(){
        (context as Activity).onBackPressed()
    }

    var listner= RatingBar.RatingBarListener {
            p0 -> Log.e("setRating", "==$p0")
    }
}