package com.letsdetour.views.addReview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.AddReviewBinding

class AddReview:Fragment() {
    lateinit var addReviewVM: AddReviewVM
    lateinit var addReviewBinding: AddReviewBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        addReviewBinding= AddReviewBinding.inflate(LayoutInflater.from(context))
        return addReviewBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addReviewVM= AddReviewVM(requireContext())
        addReviewBinding.addReviewVM=addReviewVM
    }
}