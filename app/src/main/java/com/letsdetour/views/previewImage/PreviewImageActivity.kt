package com.letsdetour.views.previewImage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.letsdetour.R
import com.letsdetour.utils.CommonKeys
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_preview_image.*

class PreviewImageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview_image)
        if (intent.hasExtra(CommonKeys.IMAGE)){
            var image=intent.getStringExtra(CommonKeys.IMAGE)
            Picasso.get().load(image).placeholder(R.drawable.ic_default_upload_image).into(previewImage)
        }
        ivBacke.setOnClickListener {
            onBackPressed()
        }
    }
}