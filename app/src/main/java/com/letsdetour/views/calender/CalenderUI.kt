package com.letsdetour.views.calender

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import com.google.android.material.datepicker.MaterialDatePicker
import com.letsdetour.R
import com.letsdetour.views.travelPlans.TravelAdapter
import com.savvi.rangedatepicker.CalendarPickerView
import com.savvi.rangedatepicker.SubTitle
import kotlinx.android.synthetic.main.activity_calender_ui.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import org.joda.time.DateTime




class CalenderUI : AppCompatActivity() {

    var calendar:CalendarPickerView?=null
    var travelAdapter:UpcomingAdapter?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calender_ui)
        travelAdapter= UpcomingAdapter(this)
        rvUpcoming.adapter=travelAdapter

        ivBack.setOnClickListener {
            onBackPressed()
        }
        initCalender()
    }

    fun initCalender(){

        val nextYear = Calendar.getInstance()
        nextYear.add(Calendar.YEAR, 1)

        val lastYear = Calendar.getInstance()
        lastYear.add(Calendar.YEAR, -1)

        calendar = findViewById(R.id.calendar_view)
        val ivmenu = findViewById<ImageView>(R.id.ivmenu)
        val list = ArrayList<Int>()
        list.add(2)

        calendar?.deactivateDates(list)
        val arrayList = ArrayList<Date>()
        try {
            val dateformat = SimpleDateFormat("dd-MM-yyyy")
          //  val strdate = "12-10-2021"
          //  val strdate2 = "16-10-2021"
         //   val newdate = dateformat.parse(strdate)
         //   val newdate2 = dateformat.parse(strdate2)
           // arrayList.add(newdate)
            //arrayList.add(newdate2)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        calendar?.init(
            lastYear.time,
            nextYear.time,
            SimpleDateFormat("MMM, yyyy", Locale.getDefault())
        ) //
            ?.inMode(CalendarPickerView.SelectionMode.RANGE) //
          //  ?.withDeactivateDates(list)
            ?.withSubTitles(getSubTitles())
            ?.withHighlightedDates(arrayList)

        calendar?.scrollToDate(Date())


        ivmenu.setOnClickListener(View.OnClickListener {
            Toast.makeText(
                this@CalenderUI,
                "list " + calendar?.selectedDates.toString(),
                Toast.LENGTH_LONG
            ).show()
        })

    }

    //₹1000
    private fun getSubTitles(): ArrayList<SubTitle>? {
        val subTitles = ArrayList<SubTitle>()
        val tmrw = Calendar.getInstance()
        tmrw.add(Calendar.DAY_OF_MONTH, 1)
        subTitles.add(SubTitle(tmrw.time, ""))
        return subTitles
    }
}