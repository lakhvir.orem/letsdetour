package com.letsdetour.views.calender

import android.content.Context
import android.view.View
import com.letsdetour.R
import com.letsdetour.utils.BaseAdapter
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.views.travelsDetails.TravelDetails
import kotlinx.android.synthetic.main.travel_plans_adapter.view.*


class UpcomingAdapter(val context: Context): BaseAdapter(R.layout.travel_plans_adapter) {
    var selectedPos=-1
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.ivMore.setOnClickListener {
            selectedPos=position
            notifyDataSetChanged()
        }
        holder.itemView.setOnClickListener {
            selectedPos=-1
            notifyDataSetChanged()
        }

        if (selectedPos==position){
            holder.itemView.cvOptions.visibility= View.VISIBLE
        }else{
            holder.itemView.cvOptions.visibility= View.GONE
        }
    }

    override fun getItemCount(): Int {
        return 5

    }
}