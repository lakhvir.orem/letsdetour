package com.letsdetour.views.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.NotifcaionsBinding
import com.letsdetour.views.mainActivity.MainActivity

class Notifications:Fragment() {
    lateinit var notificationsVM: NotificationsVM
    lateinit var notificationBinding:NotifcaionsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        notificationBinding= NotifcaionsBinding.inflate(LayoutInflater.from(context))
        return notificationBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        notificationsVM= NotificationsVM(requireContext(),this)
        notificationBinding.notificationsVM=notificationsVM
        MainActivity.mainBinding.ivAddHome.visibility=View.GONE
        MainActivity.mainBinding.btmNav.visibility=View.GONE
    }
}