package com.letsdetour.views.notifications

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import com.letsdetour.R
import com.letsdetour.response.MessageRes
import com.letsdetour.response.NotificationRes
import com.letsdetour.response.TravelPlanDetailsRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import kotlinx.android.synthetic.main.notifcation_adapter.view.*
import retrofit2.Response

class NotificationAdapter(val context: Context):BaseAdapter(R.layout.notifcation_adapter) {
    var notifiList=ArrayList<NotificationRes.Data>()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.apply {
            notifiList.let {
                tvWelcome.text=notifiList[position].title
                var image=notifiList[position].user_image
                if (!image.isNullOrEmpty()){
                    Glide.with(context).load(image).error(R.drawable.user_avatar).into(civHome)
                }
            }
            if (notifiList[position].status!=0){
                btCreateAnAcc.visibility=View.GONE
                btAccept.visibility=View.GONE
            }else{
                btCreateAnAcc.visibility=View.VISIBLE
                btAccept.visibility=View.VISIBLE
            }

            btAccept.setOnClickListener {
                acceptReject(notifiList[position].status_id.toString(),"1",btAccept,btCreateAnAcc,position)
            }

            btCreateAnAcc.setOnClickListener {
                acceptReject(notifiList[position].status_id.toString(),"0",btAccept,btCreateAnAcc,position)
            }
        }

    }

    override fun getItemCount(): Int {
        return notifiList.size
    }

    fun addDatainList(list: List<NotificationRes.Data>){
        if (!list.isNullOrEmpty()){
            notifiList.clear()
            notifiList.addAll(list)
            notifyDataSetChanged()
        }
    }

    fun acceptReject(statusId: String?,status: String?,view1:TextView,view2:TextView,position: Int) {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token="Bearer "+ PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.acceptRejectRequest(statusId.toString(),status.toString())
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        val response = res.body()
                        if (res.isSuccessful && res.code()==200) {
                            Log.e("Travel DetailsRes", "==>>$response")
                            showToast(context,response?.message.toString())
                            if (notifiList[position].status==0){
                                if (status=="1"){
                                    view1.visibility=View.GONE
                                    view2.visibility=View.GONE
                                    notifiList[position].status=1
                                    notifyDataSetChanged()

                                }else if (status=="0"){
                                    notifiList.removeAt(position)
                                    notifyDataSetChanged()

                                }
                            }

                            notifyDataSetChanged()
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }
}