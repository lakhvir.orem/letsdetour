package com.letsdetour.views.notifications

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.letsdetour.R
import com.letsdetour.response.NotificationRes
import com.letsdetour.utils.CommonAlerts
import com.letsdetour.utils.CommonKeys
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.utils.PrefferenceFile
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import retrofit2.Response

class NotificationsVM(val context: Context,val notifications: Notifications):ViewModel() {
    var notificationAdapte=NotificationAdapter(context)
    fun onClickBack(){
        (context as Activity).onBackPressed()
    }
    init {
        getNotification()
    }

    private fun getNotification() {

        if (MethodsUtil.isNetworkAvailable(context)) {
            var token =
                "Bearer " + PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()

            try {
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<NotificationRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<NotificationRes> {
                        return retrofitApi.notification_list()
                    }

                    override fun onResponse(res: Response<NotificationRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.data != null) {
                                var travelList = response.data
                                Log.e("getNotiRes", "Res$travelList")
                                notificationAdapte.addDatainList(travelList)
                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    notifications.notificationBinding.tvTitleName,
                                    response.message
                                )
                            }
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }


}