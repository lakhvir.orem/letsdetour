package com.letsdetour.views.aboutUs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.letsdetour.databinding.AboutUsBinding
import com.letsdetour.views.mainActivity.MainActivity

class AboutUs:Fragment() {
    lateinit var aboutUsBinding: AboutUsBinding
    lateinit var aboutusVM: AboutusVM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        aboutUsBinding= AboutUsBinding.inflate(LayoutInflater.from(context))
        return aboutUsBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        aboutusVM= AboutusVM(requireContext())
        aboutUsBinding.aboutusVM=aboutusVM
        MainActivity.mainBinding.btmNav.visibility=View.GONE
        MainActivity.mainBinding.ivAddHome.visibility=View.GONE
    }
}