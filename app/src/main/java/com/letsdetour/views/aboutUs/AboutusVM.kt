package com.letsdetour.views.aboutUs

import android.app.Activity
import android.content.Context
import androidx.lifecycle.ViewModel

class AboutusVM(val context: Context):ViewModel() {

    fun onClickBack(){
        (context as Activity).onBackPressed()
    }
}