package com.letsdetour.views.account_details

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.github.dhaval2404.imagepicker.ImagePicker
import com.letsdetour.databinding.AccountDetailsBinding
import com.letsdetour.utils.showToast
import com.letsdetour.views.mainActivity.MainActivity
import com.rilixtech.widget.countrycodepicker.CountryCodePicker
import kotlinx.android.synthetic.main.account_details.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import java.io.File

class AccountDetials:Fragment() {
    lateinit var accountDetailsBinding: AccountDetailsBinding
    lateinit var accountDetailsVM: AccountDetailsVM
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        accountDetailsBinding= AccountDetailsBinding.inflate(LayoutInflater.from(context))
        return accountDetailsBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        accountDetailsVM= AccountDetailsVM(requireContext(),this)
        accountDetailsBinding.accountDetailsVM=accountDetailsVM
        MainActivity.mainBinding.btmNav.visibility=View.GONE
        MainActivity.mainBinding.ivAddHome.visibility=View.GONE

        tvCountryCode2.setOnCountryChangeListener(CountryCodePicker.OnCountryChangeListener { selectedCountry ->
            accountDetailsVM.country.set(selectedCountry.name)
            accountDetailsVM.countryCode.set(selectedCountry.phoneCode.toInt())

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            val filePath = ImagePicker.getFilePath(data)!!

            if (accountDetailsVM.type=="profile"){
                civProfile.setImageURI(fileUri)
                accountDetailsVM.imageFile = File(filePath)

            }else{
                ivCover.setImageURI(fileUri)
                accountDetailsVM.coverFile = File(filePath)


            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            showToast(requireContext(), ImagePicker.getError(data))
        } else {
            showToast(requireContext(), "Task Cancelled")
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            121 -> {
                var read = grantResults[0] == PackageManager.PERMISSION_GRANTED
                var write = grantResults[1] == PackageManager.PERMISSION_GRANTED
                if (grantResults.isNotEmpty() && read && write) {
                    accountDetailsVM.openCamera()
                } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) && !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ) {
                    rationale()
                } else {
                    ActivityCompat.requestPermissions(
                        context as Activity, arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 121
                    )
                }
            }
        }
    }
    private fun rationale() {
        val builder: AlertDialog.Builder =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AlertDialog.Builder(
                    requireContext(),
                    android.R.style.Theme_Material_Light_Dialog_Alert
                )
            } else {
                AlertDialog.Builder(requireContext())
            }
        builder.setTitle("Mandatory Permissions")
            .setMessage("Manually allow permissions in App settings")
            .setPositiveButton("Proceed") { dialog, which ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", (context as Activity).packageName, null)
                intent.data = uri
                startActivityForResult(intent, 1)
            }
            .setCancelable(false)
            .show()
    }
}