package com.letsdetour.views.account_details

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.letsdetour.R
import com.letsdetour.response.CountryListRes
import com.letsdetour.response.LoginRes
import com.letsdetour.response.RegisterRes
import com.letsdetour.response.UpdateProfileRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.mainActivity.MainActivity
import com.letsdetour.views.verifyMobile.VerifyMobile
import kotlinx.android.synthetic.main.account_details.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File

class AccountDetailsVM(val context: Context, val accountDetails: AccountDetials):ViewModel() {

    var type=""
    var country=ObservableField("")
    var city=ObservableField("")
    var bio=ObservableField("")
    var userName=ObservableField("")
    var coverImage=ObservableField("")
    var image=ObservableField("")
    var email=ObservableField("")
    var countryCode=ObservableInt(91)
    var phone=ObservableField("")
    var coverFile:File?=null
    var imageFile:File?=null
    var user: LoginRes?=null
    var isVar=ObservableBoolean(false)
    val sharedPreferenceUtils by lazy { SharedPreferenceUtils(context) }


    init {
        getCountryList()
        getDetailsApi()
    }

    fun getPrefData(){
      var   user=getPref().getprefObject<LoginRes>(CommonKeys.USER)
        userName.set(user.user.user_name)
        email.set(user.user.email)
        bio.set(user.user.bio)
        phone.set(user.user.phone)
        country.set(user.user.country)
        city.set(user.user.city)
        city.set(user.user.city)
        var profileImage=user.user.image
        var coverImg=user.user.cover_image
        if (!profileImage.isNullOrEmpty()){
            image.set(profileImage)
        }
        if (!coverImg.isNullOrEmpty()){
            coverImage.set(coverImg)
        }
    }

    fun onclickBack(){
        (context as Activity).onBackPressed()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun onclickCover(){
      type="cover"
        checkPermission()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun onClickProfile(){
        type="profile"
        checkPermission()
    }
    @RequiresApi(Build.VERSION_CODES.M)
     fun checkPermission() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) !=
            PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                context as Activity, arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 121
            )
        } else {

            openCamera()

        }
    }


    fun openCamera() {
        ImagePicker.with(accountDetails)
            .compress(1024)
            .crop()
            .maxResultSize(400, 400)
            .start()
    }

    private fun getCountryList() {

        if (MethodsUtil.isNetworkAvailable(context)) {

            try {
                RetrofitCall.callService(context, false, "", object :
                    RequestProcess<Response<CountryListRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<CountryListRes> {
                        return retrofitApi.getCountryList()
                    }

                    override fun onResponse(res: Response<CountryListRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.data != null) {
                                Log.e("SignUpRegister", "Res$response")
                                var countyList = response.data
                                setCountrySpinner(countyList)
                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    accountDetails.accountDetailsBinding.btSignUp,
                                    response.message
                                )
                            }
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun setCountrySpinner(countyList: List<CountryListRes.Data>) {
        val countyNameList = countyList.map { it.name }
        val countryIdList = countyList.map { it.id }

        Log.e("countyNameList", "===>>>" + countyNameList.size)
        val adapter =
            ArrayAdapter<String>(
                context,
                android.R.layout.simple_spinner_dropdown_item,
                countyNameList
            )
        accountDetails.accountDetailsBinding.spinnerCountry.setAdapter(adapter)

        accountDetails.accountDetailsBinding.spinnerCountry.setOnTouchListener { v, event ->
            accountDetails.accountDetailsBinding.spinnerCountry.showDropDown()
            true
        }

        accountDetails.accountDetailsBinding.spinnerCountry.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                country.set(adapter.getItem(position).toString())
                val index = countyNameList.indexOf(country.get().toString())
                Log.e("selectValCountry", "===>>" + country.get().toString())
                var selectid = countryIdList[index]
                Log.e("SelectedidCountry", "==>>$selectid")
                //  getCityList(selectid)

            }
    }

    private fun getDetailsApi() {

        if (MethodsUtil.isNetworkAvailable(context)) {
            var token="Bearer "+PrefferenceFile.retrieveKey(context,CommonKeys.TOKEN).toString()

            Log.e("UpdatedToken", "==>>$token")
            try {
                RetrofitCall.callService(context, true, "", object :
                    RequestProcess<Response<LoginRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<LoginRes> {
                        return retrofitApi.getUserDetails(token,"application/json")
                    }

                    override fun onResponse(res: Response<LoginRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.user != null) {
                                Log.e("getuserDetails", "Res$response")

                                userName.set(response.user.user_name)
                                email.set(response.user.email)
                                bio.set(response.user.bio)
                                phone.set(response.user.phone)
                                country.set(response.user.country)
                                accountDetails.accountDetailsBinding.spinnerCountry.setText(country.get().toString())

                                city.set(response.user.city)
                                countryCode.set(response.user.country_code)
                                accountDetails.accountDetailsBinding.tvCountryCode2.setCountryForPhoneCode(countryCode.get())
                                var profileImage=response.user.image
                                var coverImg=response.user.cover_image
                                if (!profileImage.isNullOrEmpty()){
                                    image.set(profileImage)
                                }
                                if (!coverImg.isNullOrEmpty()){
                                    coverImage.set(coverImg)
                                }


                                if (userName.get().toString().isNullOrEmpty()){
                                    userName.set(PrefferenceFile.retrieveKey(context,CommonKeys.FIRSTNAME))
                                }
                                if (image.get().toString().isNullOrEmpty()){
                                    if (!PrefferenceFile.retrieveKey(context, CommonKeys.IMAGE).isNullOrEmpty()) {
                                        image.set(PrefferenceFile.retrieveKey(context, CommonKeys.IMAGE))
                                    }
                                }

                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    accountDetails.accountDetailsBinding.btSignUp,
                                    response.message
                                )
                            }
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }


     fun updateProfile() {

         if (MethodsUtil.isNetworkAvailable(context)) {

             try {

                 var token="Bearer "+PrefferenceFile.retrieveKey(context,CommonKeys.TOKEN).toString()

                 Log.e("UpdatedToken", "==>>$token")

                 var builder = MultipartBody.Builder()
                 builder.apply {
                     setType(MultipartBody.FORM)
                     addFormDataPart("bio", bio.get().toString())
                     addFormDataPart("user_name", userName.get().toString())
                     addFormDataPart("privacy", "1")
                     addFormDataPart("device_type", "android")
                     addFormDataPart("device_token", "djshdjhjh")
                     addFormDataPart("phone", phone.get().toString())
                     addFormDataPart("country", country.get().toString())
                     addFormDataPart("country_code", countryCode.get().toString())
                     addFormDataPart("city", city.get().toString())

                     if (coverFile!=null){
                         addPartBody(this,"cover_image",coverFile?.path)
                     }else{
                         addFormDataPart("cover_image", coverImage.get().toString())
                     }
                     if (imageFile!=null){
                         addPartBody(this,"image",imageFile?.path)
                     }else{
                         addFormDataPart("image", image.get().toString())
                     }

                     RetrofitCall.callService(context, true, "", object :
                         RequestProcess<Response<LoginRes>> {
                         override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<LoginRes> {
                             return retrofitApi.updateProfile(token,"application/json",
                                 builder.build()
                             )
                         }

                         override fun onResponse(res: Response<LoginRes>) {
                             if (res.isSuccessful) {
                                 val response = res.body()!!
                                 if (response.user != null) {
                                     Log.e("UpdateProfile", "Res$response")
                                     Toast.makeText(context, response.message, Toast.LENGTH_LONG)
                                         .show()

                                    // sharedPreferenceUtils.setprefObject(CommonKeys.USER, response)

                                     if (!response.user.image.isNullOrEmpty()){

                                         PrefferenceFile.storeKey(context,CommonKeys.IMAGE,response.user.image)
                                     }
                                     PrefferenceFile.storeKey(context,CommonKeys.FIRSTNAME,response.user.user_name)


                                     context.startActivity(
                                         Intent(
                                             context,
                                             MainActivity::class.java
                                         ))
                                 } else {
                                     MethodsUtil.showSnackBar(
                                         context,
                                         accountDetails.accountDetailsBinding.btSignUp,
                                         response.message
                                     )
                                 }
                             }
                         }

                         override fun onException(message: String) {}
                     })
                 }


             } catch (e: Exception) {
                 e.printStackTrace()
             }

         } else {
             CommonAlerts.alert(context, context.getString(R.string.internet_issue))
         }
     }

}