package com.letsdetour.views.profile

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.letsdetour.R
import com.letsdetour.response.LoginRes
import com.letsdetour.utils.CommonAlerts
import com.letsdetour.utils.CommonKeys
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.utils.PrefferenceFile
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.home.FeedAdapter
import com.letsdetour.views.setting.Setting
import retrofit2.Response

class ProfileVM(val context: Context, val profile: Profile):ViewModel(){

    init {
    }
}