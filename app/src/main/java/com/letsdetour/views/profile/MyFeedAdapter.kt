package com.letsdetour.views.profile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.letsdetour.R
import com.letsdetour.response.UserDetailsRes
import com.letsdetour.utils.CommonKeys
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.utils.PrefferenceFile
import com.letsdetour.views.comments.Comments
import com.letsdetour.views.createPost.CreatePost
import kotlinx.android.synthetic.main.feed_adapter.view.*

class MyFeedAdapter(val context: Context):com.letsdetour.utils.BaseAdapter(R.layout.feed_adapter){
    var feedList=ArrayList<UserDetailsRes.User.Feed>()
    var status="1"
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.apply {
            feedList.let {
                tvFollow.visibility=View.GONE
                tvLikes.visibility=View.GONE
                tvComments.visibility=View.GONE
                tvLike.visibility=View.GONE
                Glide.with(context).load(feedList[position].user_image).error(R.drawable.user_avatar).into(civHome)
                Glide.with(context).load(feedList[position].image).error(R.drawable.ic_default_upload_image).into(ivFeedImage)
                tvWelcome.text= feedList[position].user_name
                tvDesc.text=feedList[position].detail
                tvTime.text= MethodsUtil.getLastMessageTime(feedList[position].created_at)
            }
        }
        holder.itemView.tvComment.setOnClickListener {
            var bundle= Bundle()
            bundle.putString(CommonKeys.FeedId,feedList[position].id.toString())
            val comments= Comments()
            comments.arguments=bundle
            MethodsUtil.loadFragment(context,comments)
        }

        holder.itemView.setOnClickListener {
            var bundle= Bundle()
            bundle.putParcelable(CommonKeys.FeedData,feedList[position])
            val createPost= CreatePost()
            createPost.arguments=bundle
            MethodsUtil.loadFragment(context,createPost)
        }

        holder.itemView.tvShare.setOnClickListener {
            share(feedList[position].image)
        }

    }

    override fun getItemCount(): Int {
        return feedList.size
    }

    fun addDataInlist(list: List<UserDetailsRes.User.Feed>){
        if (!list.isNullOrEmpty()){
            feedList.clear()
            feedList.addAll(list)
            Log.e("FeedListSize","==>>"+feedList.size)
            notifyDataSetChanged()
        }

    }

    fun share(url:String) {
        MethodsUtil.hideSoftKeyboard(context as Activity)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, url)
        context.startActivity(Intent.createChooser(intent, "Share via"))
    }

}