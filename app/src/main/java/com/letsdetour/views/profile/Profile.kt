package com.letsdetour.views.profile

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.letsdetour.databinding.ProfileBinding
import com.letsdetour.views.mainActivity.MainActivity
import com.google.android.gms.maps.CameraUpdateFactory

import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.letsdetour.R
import com.letsdetour.response.HomeDataRes
import com.letsdetour.response.LoginRes
import com.letsdetour.response.MessageRes
import com.letsdetour.response.UserDetailsRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.followers.Followers
import com.letsdetour.views.home.FeedAdapter
import com.letsdetour.views.setting.Setting
import kotlinx.android.synthetic.main.feed_adapter.view.*
import kotlinx.android.synthetic.main.profile.*
import retrofit2.Response


class Profile:Fragment(), OnMapReadyCallback {
    private var mMap: GoogleMap? = null
    var root: View? = null
    var feedAdapter: MyFeedAdapter?=null
    var image=ObservableField("")
    var userName=ObservableField("")
    var followerVal=ObservableField("")
    var followStatus=ObservableField("")
    var userId=ObservableField("")
    var followingVal=ObservableField("")
    var followerid=ObservableField("")
    var feedList: List<HomeDataRes.Data.Feed>?=null

    companion object{
        var openDrawerInterface:OpenDrawerInterface?=null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (root != null) {
            val parent = root?.parent as? ViewGroup
            parent?.removeView(root)
        }
        try {
            root = inflater.inflate(R.layout.profile, container, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }
        return root
        //  return routeBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment =activity?.supportFragmentManager!!
            .findFragmentById(com.letsdetour.R.id.map2) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        MainActivity.mainBinding.ivAddHome.visibility=View.VISIBLE
        MainActivity.mainBinding.btmNav.visibility=View.VISIBLE
        MainActivity.mainBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        feedAdapter= MyFeedAdapter(requireContext())
        rvFedd.adapter=feedAdapter
        setClicks()
        getBundleData()


        btFollow.setOnClickListener {

            if (followStatus.get().toString()=="0"){
                addFollow("1",followerid.get().toString())
            }
            if (followStatus.get().toString()=="1"){
                addFollow("0",followerid.get().toString())
            }
        }
    }

    fun getBundleData(){
        if (this.arguments!=null){
            var data=this.arguments
            if (data!!.containsKey(CommonKeys.OtherProfile)){
                var string=data.getString(CommonKeys.OtherProfile)
               userId.set(data.getString(CommonKeys.USERID))
                if (string=="other"){
                    ivBack.visibility=View.VISIBLE
                    ivNotifi.visibility=View.GONE
                    ivMenu.visibility=View.GONE
                    btFollow.visibility=View.VISIBLE
                    MainActivity.mainBinding.ivAddHome.visibility=View.GONE
                    MainActivity.mainBinding.btmNav.visibility=View.GONE
                    tvFollowing.isEnabled=false
                    tvFollowers.isEnabled=false
                    tvFollowingsVal.isEnabled=false
                    tvFollowersVal.isEnabled=false
                    getOtheruserDetails()

                }
            }
        }
        else{
            MainActivity.mainBinding.ivAddHome.visibility=View.VISIBLE
            MainActivity.mainBinding.btmNav.visibility=View.VISIBLE
            getDetailsApi()

        }
    }

    fun setClicks(){

        ivNotifi.setOnClickListener {
            MethodsUtil.loadFragment(requireContext(),Setting())
        }

        ivBack.setOnClickListener {
               (context as Activity).onBackPressed()
        }

        tvItinerary.setOnClickListener {
            viewCheckins.visibility=View.VISIBLE
            viewPosts.visibility=View.GONE
            rvFedd.visibility=View.GONE
            consMap.visibility=View.VISIBLE
            tvCheckins.setTextColor(context?.resources!!.getColor(R.color.black))
            tvIti.setTextColor(context?.resources!!.getColor(R.color.black))
            tvPostCount.setTextColor(context?.resources!!.getColor(R.color.light_grey))
            tvPosts.setTextColor(context?.resources!!.getColor(R.color.light_grey))
        }

        tvOverView.setOnClickListener {
            rvFedd.isFocusable=false
            tvPostCount.isFocusable=true
            viewCheckins.visibility=View.GONE
            viewPosts.visibility=View.VISIBLE
            rvFedd.visibility=View.VISIBLE
            consMap.visibility=View.GONE
            tvPostCount.setTextColor(context?.resources!!.getColor(R.color.black))
            tvPosts.setTextColor(context?.resources!!.getColor(R.color.black))
            tvCheckins.setTextColor(context?.resources!!.getColor(R.color.light_grey))
            tvIti.setTextColor(context?.resources!!.getColor(R.color.light_grey))
        }

        ivMenu.setOnClickListener {
            openDrawerInterface!!.onClickMenu()
        }
        tvFollowers.setOnClickListener {
            gotoFolower()
        }
        tvFollowing.setOnClickListener {
            gotoFolower()
        }
        tvFollowingsVal.setOnClickListener {
            gotoFolower()
        }
        tvFollowersVal.setOnClickListener {
            gotoFolower()
        }

    }


    fun gotoFolower(){
        var bundle=Bundle()
        bundle.putString(CommonKeys.FOLLOWERS,followerVal.get().toString())
        bundle.putString(CommonKeys.FOLLOWINGS,followingVal.get().toString())
        var followers=Followers()
        followers.arguments=bundle
        MethodsUtil.loadFragment(requireContext(),followers)
    }

    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        val sydney = LatLng(-34.0, 151.0)
        mMap!!.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }

    private fun getDetailsApi() {

        if (MethodsUtil.isNetworkAvailable(requireContext())) {
            var token="Bearer "+ PrefferenceFile.retrieveKey(requireContext(), CommonKeys.TOKEN).toString()

            Log.e("UpdatedToken", "==>>$token")
            try {
                RetrofitCall.callService(requireContext(), true, token, object :
                    RequestProcess<Response<UserDetailsRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<UserDetailsRes> {
                        return retrofitApi.getUserDetail()
                    }

                    override fun onResponse(res: Response<UserDetailsRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.user != null) {

                                userName.set(response.user.user_name)
                                tvFollowersVal.text=response.user.total_followers
                                followerVal.set(response.user.total_followers)
                                followingVal.set(response.user.total_following)
                                tvFollowingsVal.text=response.user.total_following
                                tvTitleDetails.text=response.user.user_name
                                tvRate.text=response.user.bio
                                consProfile.visibility=View.VISIBLE

                                var list=response.user.feeds

                                if (response.user.feeds?.isNotEmpty()!!){
                                    var size=list?.size.toString()
                                    Log.e("FeedList", "==$size")
                                    tvPostCount.text= "$size Posts"
                                    feedAdapter?.addDataInlist(list!!)
                                }
                                var profileImage=response.user.image
                                var coverImg=response.user.cover_image
                                if (!profileImage.isNullOrEmpty()){
                                    image.set(profileImage)
                                    Glide.with(requireContext()).load(profileImage).error(R.drawable.user_avatar).into(civProfile)
                                }
                                if (!coverImg.isNullOrEmpty()){
                                    Glide.with(requireContext()).load(coverImg).error(R.drawable.ic_chat_image).into(ivCurrentLoc)
                                }

                                if (userName.get().toString().isNullOrEmpty()){
                                    userName.set(PrefferenceFile.retrieveKey(requireContext(), CommonKeys.FIRSTNAME))
                                }
                                if (image.get().toString().isNullOrEmpty()){
                                    if (!PrefferenceFile.retrieveKey(requireContext(), CommonKeys.IMAGE).isNullOrEmpty()) {
                                        Glide.with(requireContext()).load(PrefferenceFile.retrieveKey(requireContext(), CommonKeys.IMAGE)).error(R.drawable.user_avatar).into(civProfile)
                                    }
                                }

                            } else {
                                MethodsUtil.showSnackBar(
                                    requireContext(),
                                    civProfile,
                                    response.message
                                )
                            }
                        }
                        else if (res.code()==401) {
                            PrefferenceFile.logout(requireContext())
                            MethodsUtil.showSnackBar(
                                requireContext(),
                                tvFollowersVal,
                                res.message().toString()
                            )
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(requireContext(), context?.getString(R.string.internet_issue)!!)
        }
    }

    private fun getOtheruserDetails() {

        if (MethodsUtil.isNetworkAvailable(requireContext())) {
            var token="Bearer "+ PrefferenceFile.retrieveKey(requireContext(), CommonKeys.TOKEN).toString()

            Log.e("UpdatedToken", "==>>$token")
            try {
                RetrofitCall.callService(requireContext(), true, token, object :
                    RequestProcess<Response<UserDetailsRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<UserDetailsRes> {
                        return retrofitApi.otherDetails(userId.get().toString())
                    }

                    override fun onResponse(res: Response<UserDetailsRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.user != null) {
                                Log.e("getuserDetails", "Res$response")

                                userName.set(response.user.user_name)
                                tvFollowersVal.text=response.user.total_followers
                                followerVal.set(response.user.total_followers)
                                followingVal.set(response.user.total_following)
                                tvFollowingsVal.text=response.user.total_following
                                tvTitleDetails.text=response.user.user_name
                                tvRate.text=response.user.bio
                                consProfile.visibility=View.VISIBLE
                                followerid.set(response.user.id.toString())

                                if (response.user.follow_status=="1"){
                                    btFollow.text="Unfollow"
                                    followStatus.set("1")
                                }
                                else if  (response.user.follow_status=="2"){
                                    btFollow.text="Requested"
                                    followStatus.set("2")

                                }else{
                                    btFollow.text="Follow"
                                    followStatus.set("0")

                                }

                                var list=response.user.feeds

                                var size=list?.size.toString()
                                Log.e("FeedList", "==$size")
                                tvPostCount.text= "$size Posts"

                                feedAdapter?.addDataInlist(list!!)
                                var profileImage=response.user.image
                                var coverImg=response.user.cover_image
                                if (!profileImage.isNullOrEmpty()){
                                    image.set(profileImage)
                                    Glide.with(requireContext()).load(profileImage).error(R.drawable.user_avatar).into(civProfile)

                                }
                                if (!coverImg.isNullOrEmpty()){
                                    Glide.with(requireContext()).load(coverImg).error(R.drawable.ic_chat_image).into(ivCurrentLoc)
                                }


                           /*     if (userName.get().toString().isNullOrEmpty()){
                                    userName.set(PrefferenceFile.retrieveKey(requireContext(), CommonKeys.FIRSTNAME))
                                }
                                if (image.get().toString().isNullOrEmpty()){
                                    if (!PrefferenceFile.retrieveKey(requireContext(), CommonKeys.IMAGE).isNullOrEmpty()) {
                                        Glide.with(requireContext()).load(PrefferenceFile.retrieveKey(requireContext(), CommonKeys.IMAGE)).error(R.drawable.user_avatar).into(civProfile)

                                    }
                                }*/

                            } else {
                                MethodsUtil.showSnackBar(
                                    requireContext(),
                                    civProfile,
                                    response.message
                                )
                            }
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(requireContext(), context?.getString(R.string.internet_issue)!!)
        }
    }

    fun addFollow(status: String, followerId: String) {

        if (MethodsUtil.isNetworkAvailable(requireContext())) {
            try {
                var token="Bearer "+PrefferenceFile.retrieveKey(requireContext(),CommonKeys.TOKEN).toString()

                RetrofitCall.callService(requireContext(), true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.addFollow(status,followerId)
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (res.code() == 200) {
                                Log.e("LoginRes", "Res$response")
                                showToast(requireContext(),response.message)
                                if (status=="1"){
                                    followStatus.set("2")
                                    btFollow.text="Requested"
                                }
                                else if(status=="0"){
                                    followStatus.set("0")
                                    btFollow.text="Follow"

                                }

                            } else {
                                showToast(requireContext(),response.message)
                            }
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(requireContext(), context?.getString(R.string.internet_issue)!!)
        }
    }
}