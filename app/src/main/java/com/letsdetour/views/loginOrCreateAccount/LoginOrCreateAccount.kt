package com.letsdetour.views.loginOrCreateAccount

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.letsdetour.R
import com.letsdetour.databinding.ActivityLoginOrCreateAccountBinding
import com.letsdetour.utils.diconnectFromFb
import com.letsdetour.utils.dismissProgress
import com.letsdetour.utils.initProgress
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login_or_create_account.*

class LoginOrCreateAccount : AppCompatActivity() {
    lateinit var callbackManager: CallbackManager
     lateinit var auth: FirebaseAuth
    lateinit var signInClient: GoogleSignInClient
    lateinit var gso:GoogleSignInOptions
    var RC_SIGN_IN=121

    lateinit var loginOrCreateVM: LoginOrCreateVM
    lateinit var loginOrCreateAccountBinding: ActivityLoginOrCreateAccountBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = Color.TRANSPARENT
        }

        loginOrCreateAccountBinding=DataBindingUtil.setContentView(this,R.layout.activity_login_or_create_account)
        callbackManager = CallbackManager.Factory.create()
        loginOrCreateVM= LoginOrCreateVM(this,this)
        loginOrCreateAccountBinding.loginOrCreateVM=loginOrCreateVM
        auth = Firebase.auth
        fbSignin()
        configueGoogleSignIn()
        ivGoogle.setOnClickListener {

            signInIntent()
        }

    }

    fun configueGoogleSignIn() {
        try {
            gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
            signInClient = GoogleSignIn.getClient(this, gso)
          //  MySingleTonClass.googleSigninCient=signInClient
        }catch (e:Exception){
            e.printStackTrace()
        }
        // Configure Google Sign In

    }

    fun signInIntent() {
        val loginIntent: Intent = signInClient.signInIntent
            startActivityForResult(loginIntent, RC_SIGN_IN)
    }

    fun fbSignin(){

        ivFacebook.setOnClickListener {

            LoginManager.getInstance().logInWithReadPermissions(this, listOf("email","public_profile"));

        }

        LoginManager.getInstance().registerCallback(callbackManager,object :
            FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                Log.e("onSuccess", "Exe$result")
                initProgress(this@LoginOrCreateAccount)
                loginOrCreateVM.getFacebookData(result)
            }

            override fun onCancel() {
                Toast.makeText(this@LoginOrCreateAccount,"Cancel",  Toast.LENGTH_SHORT).show()
                diconnectFromFb()
                dismissProgress()

            }

            override fun onError(error: FacebookException?) {
                Toast.makeText(this@LoginOrCreateAccount,"error ${error.toString()}",  Toast.LENGTH_SHORT).show()
                Log.e("P","${error.toString()}")
                diconnectFromFb()
                dismissProgress()

            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        Log.e("getData","==>>"+data?.extras)
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)!!
                Log.d("Succeesss", "firebaseAuthWithGoogle:" + account.id)
                initProgress(this)
                loginOrCreateVM.firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {

                Log.w("Faileddd", "Google sign in failed", e)
                dismissProgress()
            }
        }
    }
}