package com.letsdetour.views.loginOrCreateAccount

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.appevents.UserDataStore.LAST_NAME
import com.facebook.login.LoginResult
import com.google.firebase.auth.GoogleAuthProvider
import com.letsdetour.R
import com.letsdetour.response.CommonLoginRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.login.Login
import com.letsdetour.views.mainActivity.MainActivity
import com.letsdetour.views.signUp.SignUp
import retrofit2.Response

class LoginOrCreateVM(val context: Context, val loginOrCreateAccount: LoginOrCreateAccount):ViewModel() {
    var socailToken= ObservableField("")
    var gender=""
    var fbName=""
    var loginStatus= ObservableBoolean(false)
    var image=ObservableField("")
    var phone=ObservableField("")
    var email=ObservableField("")
    var name=ObservableField("")
    val sharedPreferenceUtils by lazy {SharedPreferenceUtils(context)}

    init {
        checkLoginStatus()
    }

    fun checkLoginStatus(){
        var accessToken = AccessToken.getCurrentAccessToken()
        var isLoggedIn = accessToken != null && !accessToken.isExpired
        loginStatus.set(isLoggedIn)

        Log.e("CheckStatus", "login${loginStatus.get()}")
    }

    fun getFacebookData(result: LoginResult?) {
        var request = GraphRequest.newMeRequest(
            result?.accessToken
        ) { `object`, response ->
            try {
                dismissProgress()
                Log.e("FBData", "Rees$`object`")
                val id = `object`.getString("id")
                socailToken.set(id)
                image.set("https://graph.facebook.com/$id/picture?width=200&height=150")
                Log.i("profile_pic", image.get().toString() + "")
                if (`object`.has(CommonKeys.FIRSTNAME)) {
                    name.set(`object`.getString("first_name"))
                }
                 if (`object`.has(CommonKeys.LASTNAME)) {
                    var  LastName = `object`.getString("last_name")
                     name.set(name.get().toString()+" "+LastName.toString())
                 }
                if (`object`.has(CommonKeys.Email)) {
                    email.set(`object`.getString("email"))
                }
                if (`object`.has(CommonKeys.GENDER)) {


                }
                Log.e("TAGfbEmailID", email.get().toString())
                Log.e("imaGE", image.get().toString())
                Log.e("TAGfbName", name.get().toString())
                Log.e("TAGfbID", socailToken.get().toString())

                PrefferenceFile.storeKey(context,CommonKeys.IMAGE,image.get().toString())
                PrefferenceFile.storeKey(context,CommonKeys.FIRSTNAME,name.get().toString())


                socialLoginApi("2")

            } catch (error: Exception) {
                error.printStackTrace()
                Log.e("DATA: ","${error.toString()}")
            }
        }

        val parameters = Bundle()
        parameters.putString(
            "fields",
            "id, first_name, last_name, email, gender, birthday,age_range, location"
        )
        request.parameters = parameters
        request.executeAsync()
    }

     fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        loginOrCreateAccount.auth.signInWithCredential(credential)
            .addOnCompleteListener(context as Activity) { task ->
                if (task.isSuccessful) {
                    val user = loginOrCreateAccount.auth.currentUser
                    Log.d("TAGGoogleSigninSuccess", "signInWithCredential:success$user")

                    if (user!=null){
                        try {

                            Log.w("GoogleUid", "GoogleUiD" +user.providerData[0].uid)
                            socailToken.set(user.providerData[0].uid)
                            name.set(user.providerData[0].displayName.toString())
                            email.set(user.providerData[0].email.toString())
                            phone.set(user.providerData[0].phoneNumber.toString())
                            image.set(user.providerData[0].photoUrl.toString())
                            dismissProgress()
                            socialLoginApi("1")

                            PrefferenceFile.storeKey(context,CommonKeys.IMAGE,user.providerData[0].photoUrl.toString())
                            PrefferenceFile.storeKey(context,CommonKeys.FIRSTNAME,user.providerData[0].displayName.toString())


                        }catch (e:Exception){
                            e.printStackTrace()
                        }

                    }

                } else {
                    Log.w("TAGGoogleSigninFaliour", "signInWithCredential:failure", task.exception)
                   showToast(context, "Authentication Failed.")
                    dismissProgress()
                }

            }
    }


    fun onclickLogin(){
        context.startActivity(Intent(context,Login::class.java))

    }

    fun onClickSignUp(){
        context.startActivity(Intent(context, SignUp::class.java))
    }

    fun socialLoginApi(type:String) {

        if (MethodsUtil.isNetworkAvailable(context)) {

            try {
                RetrofitCall.callService(context, true, "", object :
                    RequestProcess<Response<CommonLoginRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<CommonLoginRes> {
                        return retrofitApi.socialLogin(socailToken.get().toString(), type,email.get().toString(),"android","jdhjdhfjdhf","","",name.get().toString())
                    }

                    override fun onResponse(loginResponse: Response<CommonLoginRes>) {
                        if (loginResponse.isSuccessful) {
                            val response = loginResponse.body()!!
                            Log.e("LoginRes", "Res$response")

                            if (response.token != null) {

                                PrefferenceFile.storeKey(
                                    context,
                                    CommonKeys.TOKEN,
                                    response.token.toString()
                                )
                                PrefferenceFile.storeKey(context, CommonKeys.ISUSERLOGIN, "yes")
                                PrefferenceFile.storeKey(context,CommonKeys.USERID,response.user.id.toString())

                                context.startActivity(
                                    Intent(
                                        context,
                                        MainActivity::class.java
                                    )
                                )
                                (context  as Activity).finishAffinity()
                              //  sharedPreferenceUtils.setprefObject(CommonKeys.USER, response)

                            } else {
                               showToast(context,response.message)
                            }
                        }else{
                            showToast(context,loginResponse.errorBody()!!.string())

                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }
}