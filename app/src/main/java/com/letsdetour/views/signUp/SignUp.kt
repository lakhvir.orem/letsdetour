package com.letsdetour.views.signUp

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.github.dhaval2404.imagepicker.ImagePicker
import com.letsdetour.R
import com.letsdetour.databinding.ActivitySignUpBinding
import com.letsdetour.utils.showToast
import com.rilixtech.widget.countrycodepicker.CountryCodePicker.OnCountryChangeListener
import kotlinx.android.synthetic.main.activity_sign_up.*
import java.io.File


@Suppress("DEPRECATION")
class SignUp : AppCompatActivity() {
    lateinit var signUpVM: SignUpVM
    lateinit var signUpBinding: ActivitySignUpBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = Color.TRANSPARENT
            getWindow().decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        }
        signUpBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)
        signUpVM = SignUpVM(this, this)
        signUpBinding.signUpVM = signUpVM

        tvCountryCode.setOnCountryChangeListener(OnCountryChangeListener {
            selectedCountry ->
            signUpVM.country.set(selectedCountry.name)
            signUpVM.countryCode.set(selectedCountry.phoneCode)
            Toast.makeText(this, "Selected Country " + selectedCountry.name+"==="+selectedCountry.phoneCode, Toast.LENGTH_SHORT).show() })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            civSignUp.setImageURI(fileUri)
            signUpVM.isImageSet.set(true)
            //You can also get File Path from intent
            val filePath = ImagePicker.getFilePath(data)!!
            signUpVM.file = File(filePath)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            showToast(this, ImagePicker.getError(data))
        } else {
            showToast(this, "Task Cancelled")
        }
    }


    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String?>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            121 -> {
                var read = grantResults[0] == PackageManager.PERMISSION_GRANTED
                var write = grantResults[1] == PackageManager.PERMISSION_GRANTED
                if (grantResults.isNotEmpty() && read && write) {
                    signUpVM.openCamera()
                } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) && !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ) {
                    rationale()
                } else {
                    ActivityCompat.requestPermissions(
                        this, arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 121
                    )
                }
            }
        }
    }
    private fun rationale() {
        val builder: AlertDialog.Builder =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AlertDialog.Builder(
                    this,
                    android.R.style.Theme_Material_Light_Dialog_Alert
                )
            } else {
                AlertDialog.Builder(this)
            }
        builder.setTitle("Mandatory Permissions")
            .setMessage("Manually allow permissions in App settings")
            .setPositiveButton("Proceed") { dialog, which ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", packageName, null)
                intent.data = uri
                startActivityForResult(intent, 1)
            }
            .setCancelable(false)
            .show()
    }
}