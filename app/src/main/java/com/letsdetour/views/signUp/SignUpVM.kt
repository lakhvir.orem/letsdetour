package com.letsdetour.views.signUp

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.util.Patterns
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.github.dhaval2404.imagepicker.ImagePicker
import com.letsdetour.R
import com.letsdetour.response.CityListRes
import com.letsdetour.response.CountryListRes
import com.letsdetour.response.RegisterRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.login.Login
import com.letsdetour.views.mainActivity.MainActivity
import com.letsdetour.views.verifyMobile.VerifyMobile
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File

class SignUpVM(val context: Context, val signUp: SignUp) : ViewModel() {
    var username = ObservableField("")
    var email = ObservableField("")
    var password = ObservableField("")
    var confPassword = ObservableField("")
    var phone = ObservableField("")
    var country = ObservableField("")
    var state = ObservableField("")
    var countryCode = ObservableField("+91")
    var city = ObservableField("")
    var isImageSet = ObservableBoolean(false)
    var file: File? = null


    init {
        getCountryList()
    }

    fun onclickBack() {
        (context as Activity).finish()
    }

    fun onclickLogin() {
        context.startActivity(Intent(context, Login::class.java))
    }

    fun onclickPrivacy() {
        context.startActivity(
            Intent(context, MainActivity::class.java).putExtra(
                CommonKeys.Privacy,
                "privacy"
            )
        )
    }

    fun onclickTerms() {
        context.startActivity(
            Intent(context, MainActivity::class.java).putExtra(
                CommonKeys.Privacy,
                "terms"
            )
        )
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun onClickImage() {
        checkPermission()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkPermission() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) !=
            PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                context as Activity, arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 121
            )
        } else {

            openCamera()

        }
    }


    fun openCamera() {
        ImagePicker.with(signUp)
            .compress(1024)
            .crop()
            .maxResultSize(400, 400)
            .start()
    }

    fun validate(): Boolean {
        when {

            !isImageSet.get() -> {
                MethodsUtil.showSnackBar(
                    context,
                    signUp.signUpBinding.btSignUp,
                    context.getString(R.string.select_image)
                )
                return false
            }
            username.get().toString().isNullOrEmpty() -> {
                MethodsUtil.showSnackBar(
                    context,
                    signUp.signUpBinding.btSignUp,
                    context.getString(R.string.select_userName)
                )
                return false
            }
            email.get().toString().isNullOrEmpty() -> {
                MethodsUtil.showSnackBar(
                    context,
                    signUp.signUpBinding.btSignUp,
                    context.getString(R.string.enter_email)
                )
                return false
            }

            !email.get().toString().trim().matches(Patterns.EMAIL_ADDRESS.pattern().toRegex()) -> {
                MethodsUtil.showSnackBar(
                    context,
                    signUp.signUpBinding.btSignUp,
                    context.getString(R.string.please_enter_your_valid_email)
                )
                return false
            }

            password.get().toString().isNullOrEmpty() -> {
                MethodsUtil.showSnackBar(
                    context,
                    signUp.signUpBinding.btSignUp,
                    context.getString(R.string.enter_password)
                )
                return false
            }
            /*   password.get().toString().trim().length < 8 -> {
                   MethodsUtil.showSnackBar(
                       context,
                       signUp.signUpBinding.btSignUp,
                       context.getString(R.string.please_enter_your_valid_password)

                   )
                   return false
               }*/

            confPassword.get().toString().isNullOrEmpty() -> {
                MethodsUtil.showSnackBar(
                    context,
                    signUp.signUpBinding.btSignUp,
                    context.getString(R.string.enter_conf_password)
                )
                return false
            }
            confPassword.get().toString() != password.get().toString() -> {
                MethodsUtil.showSnackBar(
                    context,
                    signUp.signUpBinding.btSignUp,
                    context.getString(R.string.password_not_match)
                )
                return false
            }

            country.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(context,signUp.signUpBinding.btSignUp,context.getString(R.string.please_select_country))
                return false
            }
            city.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(context,signUp.signUpBinding.btSignUp,context.getString(R.string.please_select_city))
                return false
            }

            phone.get().toString().isNullOrEmpty() -> {
                MethodsUtil.showSnackBar(
                    context,
                    signUp.signUpBinding.btSignUp,
                    context.getString(R.string.please_enter_phone)
                )
                return false
            }
            else -> {
                return true
            }
        }
    }

    fun submit() {
        if (validate()) {
            MethodsUtil.hideSoftKeyboard(context as Activity)
            signUpService()
        }
    }

    private fun getCountryList() {

        if (MethodsUtil.isNetworkAvailable(context)) {

            try {
                RetrofitCall.callService(context, true, "", object :
                    RequestProcess<Response<CountryListRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<CountryListRes> {
                        return retrofitApi.getCountryList()
                    }

                    override fun onResponse(res: Response<CountryListRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.data != null) {
                                Log.e("SignUpRegister", "Res$response")
                                var countyList = response.data
                                setCountrySpinner(countyList)
                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    signUp.signUpBinding.btSignUp,
                                    response.message
                                )
                            }
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }

    private fun getCityList(cityid: Int) {

        if (MethodsUtil.isNetworkAvailable(context)) {

            try {
                RetrofitCall.callService(context, true, "", object :
                    RequestProcess<Response<CityListRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<CityListRes> {
                        return retrofitApi.getStateList(cityid)
                    }

                    override fun onResponse(res: Response<CityListRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.data != null) {
                                Log.e("SignUpRegister", "Res$response")
                                var cityList = response.data
                                setCitySpinner(cityList)
                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    signUp.signUpBinding.btSignUp,
                                    response.message
                                )
                            }
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setCountrySpinner(countyList: List<CountryListRes.Data>) {
        val countyNameList = countyList.map { it.name }
        val countryIdList = countyList.map { it.id }

        Log.e("countyNameList", "===>>>" + countyNameList.size)
        val adapter =
            ArrayAdapter<String>(
                context,
                android.R.layout.simple_spinner_dropdown_item,
                countyNameList
            )
        signUp.signUpBinding.spinnerCountry.setAdapter(adapter)

        signUp.signUpBinding.spinnerCountry.setOnTouchListener { v, event ->
            signUp.signUpBinding.spinnerCountry.showDropDown()
            true
        }

        signUp.signUpBinding.spinnerCountry.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                country.set(adapter.getItem(position).toString())
                val index = countyNameList.indexOf(country.get().toString())
                Log.e("selectValCountry", "===>>" + country.get().toString())
                var selectid = countryIdList[index]
                Log.e("SelectedidCountry", "==>>$selectid")
                //  getCityList(selectid)

            }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setCitySpinner(cityList: List<CityListRes.Data>) {
        val cityNameList = cityList.map { it.name }
        val cityIdList = cityList.map { it.id }

        Log.e("employeeListSpinner", "===>>>" + cityNameList.size)
        val adapter =
            ArrayAdapter<String>(
                context,
                android.R.layout.simple_spinner_dropdown_item,
                cityNameList
            )
        signUp.signUpBinding.spinnerCity.setAdapter(adapter)

        signUp.signUpBinding.spinnerCity.setOnTouchListener { v, event ->
            signUp.signUpBinding.spinnerCity.showDropDown()
            true
        }

        signUp.signUpBinding.spinnerCity.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                city.set(adapter.getItem(position).toString())
                Log.e("selectValCity", "===>>>${city.get().toString()}")
                var selectcityid = cityIdList.indexOf(city.get().toString())
                Log.e("SelectedidCity", "==>>$selectcityid")
                getCityList(selectcityid)
            }
    }

    private fun signUpService() {

        if (MethodsUtil.isNetworkAvailable(context)) {

            try {
                val name =
                    RequestBody.create(MediaType.parse("text/plain"), username.get().toString())
                val email1 =
                    RequestBody.create(MediaType.parse("text/plain"), email.get().toString())
                val password =
                    RequestBody.create(MediaType.parse("text/plain"), password.get().toString())
                val country =
                    RequestBody.create(MediaType.parse("text/plain"), country.get().toString())
                val city = RequestBody.create(MediaType.parse("text/plain"), city.get().toString())
                //   val state= RequestBody.create(MediaType.parse("text/plain"),  state.get().toString())
                val countryCode =
                    RequestBody.create(MediaType.parse("text/plain"), countryCode.get().toString())
                val phone =
                    RequestBody.create(MediaType.parse("text/plain"), phone.get().toString())

                val imagebody = RequestBody.create(MediaType.parse("multipart/form-data"), file!!)
                val image = MultipartBody.Part.createFormData("image", file!!.name, imagebody)

                RetrofitCall.callService(context, true, "", object :
                    RequestProcess<Response<RegisterRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<RegisterRes> {
                        return retrofitApi.signUp(
                            name,
                            email1,
                            password,
                            phone, country, countryCode, city, image
                        )
                    }

                    override fun onResponse(res: Response<RegisterRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.user != null) {
                                Log.e("SignUpRegister", "Res$response")
                                Toast.makeText(context, response.message, Toast.LENGTH_LONG).show()
                                PrefferenceFile.storeKey(context,CommonKeys.TOKEN,response.token.toString())
                                PrefferenceFile.storeKey(context,CommonKeys.FIRSTNAME,response.user.user_name.toString())
                                PrefferenceFile.storeKey(context,CommonKeys.IMAGE,response.user.image.toString())
                                PrefferenceFile.storeKey(context,CommonKeys.USERID,response.user.id.toString())

                                context.startActivity(
                                    Intent(
                                        context,
                                        VerifyMobile::class.java
                                    ).putExtra(CommonKeys.Email, email.get().toString())
                                        .putExtra(CommonKeys.OTP, response.otp.toString())
                                )
                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    signUp.signUpBinding.btSignUp,
                                    response.message
                                )
                            }
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }

}