package com.letsdetour.views.createPlan

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.letsdetour.R
import com.letsdetour.networking.Api
import com.letsdetour.response.FeedDetailsRes
import com.letsdetour.response.GetPlanListRes
import com.letsdetour.response.MessageRes
import com.letsdetour.response.NearBySearchRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.mainActivity.MainActivity
import com.letsdetour.views.map.MapFrag
import com.letsdetour.views.map.MapViewModel
import com.letsdetour.views.map.PutAddressInterface
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class CreatePlanVM(val context: Context, val createPlan: CreatePlan) : ViewModel(),
    PutAddressInterface {
    var startDate = ObservableField("")
    var endDate = ObservableField("")
    var latti = ObservableField("")
    var lnggi = ObservableField("")
    var address = ObservableField("")
    var PlanId = ObservableField("")
    var placeId = ObservableField("")
    var startTimeLong=0L
    var endTimeLong=0L
    var forUpdate=ObservableBoolean(false)
    var cal = Calendar.getInstance()
    var model: GetPlanListRes.Data?=null


    init {
        MapViewModel.putAddressInterface = this
        getBundleData()
    }
    fun getBundleData(){
        if (createPlan.arguments!=null){
            var data=createPlan.arguments
            if (data?.containsKey(CommonKeys.PlanData)!!){
                model=data?.getParcelable<GetPlanListRes.Data>(CommonKeys.PlanData)
                Log.e("Travel_Plan_Data","==>>"+model.toString())
                startDate.set(model?.start_date)
                endDate.set(model?.end_date)
                latti.set(model?.lat)
                latti.set(model?.lat)
                address.set(model?.address)
                placeId.set(model?.place_id)
                PlanId.set(model?.id.toString())
                forUpdate.set(true)
            }
        }
    }


    fun onClickBack() {
        (context as Activity).onBackPressed()
    }

    fun openStartDatePicker() {
        val myCalendar = Calendar.getInstance()

        val datePicker =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "yyyy/MM/dd" //In which you need put here
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                startDate.set(sdf.format(myCalendar.time))
                startTimeLong=MethodsUtil.getMilliseconds(startDate.get().toString())
            }

        val dialog = DatePickerDialog(
            context, datePicker, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
            myCalendar.get(Calendar.DAY_OF_MONTH)
        )

        dialog.datePicker.minDate = System.currentTimeMillis() - 1000


        dialog.show()
    }

    fun openEndDatePicker() {
        val myCalendar = Calendar.getInstance()

        val date =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "yyyy/MM/dd" //In which you need put here
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                endDate.set(sdf.format(myCalendar.time))
                endTimeLong=MethodsUtil.getMilliseconds(endDate.get().toString())

                Log.e("StartTime", "==>>$startTimeLong")
                Log.e("EndTime", "==>>$endTimeLong")

            }

        val dialog = DatePickerDialog(
            context, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
            myCalendar.get(Calendar.DAY_OF_MONTH)
        )

        dialog.datePicker.minDate = System.currentTimeMillis() - 1000


        dialog.show()
    }

    fun onClickLocation() {
        context.startActivity(Intent(context, MapFrag::class.java))
    }

    override fun putAddress(lat: String, lng: String, location: String) {
        latti.set(lat)
        lnggi.set(lng)
        address.set(location)
        getPlaceidApi()
    }

    fun validation():Boolean{
        when{
            address.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(context,createPlan.createPlansBinding.btLogin,context.getString(R.string.please_select_address))
                return false
            }

            startDate.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(context,createPlan.createPlansBinding.btLogin,context.getString(R.string.please_select_startDate))
                return false
            }
            endDate.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(context,createPlan.createPlansBinding.btLogin,context.getString(R.string.please_select_endDate))
                return false
            }
            endTimeLong<startTimeLong->{
                MethodsUtil.showSnackBar(context,createPlan.createPlansBinding.btLogin,context.getString(R.string.end_date_exception))
                return false
            }
            else->{
                return true
            }
        }
    }

    fun submitForCreatePlan(){
        if (validation()){
            if (!forUpdate.get()){
                createPlan()
            }else{
                updatePlan()
            }
        }
    }

    private fun getPlaceidApi() {

        if (MethodsUtil.isNetworkAvailable(AppController.context)) {

            try {
                Api.instance.getPlaceId(
                    latti.get().toString() + "," + lnggi.get().toString(),
                    context.getString(R.string.map_apy_key)
                )
                    .enqueue(object : Callback<NearBySearchRes> {
                        override fun onFailure(call: Call<NearBySearchRes>?, t: Throwable?) {
                            Log.e("onFailureLogin", "exe" + t?.message)
                            showToast(context, t?.message.toString())
                            //view.onFailure(t?.localizedMessage!!)
                        }

                        override fun onResponse(
                            call: Call<NearBySearchRes>?,
                            response: Response<NearBySearchRes>?
                        ) {
                            if (response != null) {

                                if (response.body() != null) {
                                    var res = response.body()
                                    Log.e("ifPlaceIdget", "exe" + response.body().toString())
                                    placeId.set(res!!.results[0].place_id)
                                    Log.e("Placeid", "==>>" + placeId.get().toString())
                                } else {
                                    var jsonObject = JSONObject(response.errorBody()?.string())
                                    Log.e("ifPlaceIdgetError", "exe$jsonObject")

                                }
                            }
                        }

                    })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(
                AppController.context,
                AppController.context.getString(R.string.internet_issue)
            )
        }
    }

    fun createPlan() {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token =
                    "Bearer " + PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()

                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<FeedDetailsRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<FeedDetailsRes> {
                        return retrofitApi.createPlan(
                            address.get().toString(),
                            latti.get().toString(),
                            lnggi.get().toString(),
                            startDate.get().toString(),
                            endDate.get().toString(),
                            placeId.get().toString()
                        )
                    }

                    override fun onResponse(res: Response<FeedDetailsRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            Log.e("AddCommentRes", "==>>$response")
                            context.startActivity(Intent(context,MainActivity::class.java))
                            (context as Activity).finishAffinity()
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }

    fun updatePlan() {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token =
                    "Bearer " + PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()

                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.updatePlan(
                            PlanId.get().toString(),
                            address.get().toString(),
                            latti.get().toString(),
                            lnggi.get().toString(),
                            startDate.get().toString(),
                            endDate.get().toString(),
                            placeId.get().toString()
                        )
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            Log.e("AddCommentRes", "==>>$response")
                            context.startActivity(Intent(context,MainActivity::class.java))
                            (context as Activity).finishAffinity()
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }
}

