package com.letsdetour.views.createPlan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.CreatePlanBinding
import android.app.DatePickerDialog

import android.widget.DatePicker

import android.R
import android.app.DatePickerDialog.OnDateSetListener

import android.widget.EditText
import com.letsdetour.generated.callback.OnClickListener
import kotlinx.android.synthetic.main.create_plan.*
import java.text.SimpleDateFormat
import java.util.*
import android.content.DialogInterface
import android.graphics.Color


class CreatePlan:Fragment() {
    lateinit var createPlanVM: CreatePlanVM
    lateinit var createPlansBinding: CreatePlanBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        createPlansBinding=CreatePlanBinding.inflate(LayoutInflater.from(context))
        return createPlansBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createPlanVM = CreatePlanVM(requireContext(), this)
        createPlansBinding.createPlanVM = createPlanVM


    }



}