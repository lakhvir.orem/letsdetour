package com.letsdetour.views.addSchedule

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.widget.TimePicker.OnTimeChangedListener
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.github.dhaval2404.imagepicker.ImagePicker
import com.letsdetour.R
import com.letsdetour.response.MessageRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.map.MapFrag
import com.letsdetour.views.map.MapViewModel
import com.letsdetour.views.map.PutAddressInterface
import okhttp3.MultipartBody
import retrofit2.Response
import java.io.File
import java.util.*


class AddScheduleVM(val context: Context, val addSchedule: AddSchedule):ViewModel() ,
    PutAddressInterface {
    var dateId = ObservableField("")
    var route_time = ObservableField("")
    var address = ObservableField("")
    var lat = ObservableField("")
    var lng = ObservableField("")
    var name = ObservableField("")
    var planDate= ObservableField("")
    var isImageSet=ObservableBoolean(false)

    var file:File?=null
    init {
        getBundleData()
        getTime()
        MapViewModel.putAddressInterface=this
    }

    fun getBundleData() {
        if (addSchedule.arguments != null) {
            var data = addSchedule.arguments
            if (data?.containsKey(CommonKeys.DateID)!!) {
                dateId.set(data.getString(CommonKeys.DateID))
                planDate.set(
                    MethodsUtil.setDateFormatToDisplay(
                        data.getString(CommonKeys.PlanDate).toString()
                    ) + "TH"
                )
                Log.e("DateId", "===>>>" + dateId.get().toString())
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun onClickImage() {
        checkPermission()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkPermission() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) !=
            PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                context as Activity, arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 121
            )
        } else {

            openCamera()

        }
    }


    fun openCamera() {
        ImagePicker.with(addSchedule)
            .compress(1024)
            .crop()
            .maxResultSize(400, 400)
            .start()
    }

    fun onClickLocation(){
        context.startActivity(Intent(context, MapFrag::class.java))
    }

    fun onclickBack() {
        (context as Activity).onBackPressed()
    }


    fun checkValidate():Boolean{
        when{
            name.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(
                    context,
                    addSchedule.addScheduleBinding.btLogin,
                    "Please enter name of place"
                )
                return false
            }
            address.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(
                    context,
                    addSchedule.addScheduleBinding.btLogin,
                    "Please select address"
                )
                return false
            }

            route_time.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(
                    context,
                    addSchedule.addScheduleBinding.btLogin,
                    "Please select time"
                )
                return false
            }

            else ->{
                return true
            }
        }
    }

    fun getTime(){

        var hours: Int = addSchedule.addScheduleBinding.timePicker1.currentHour //get Time in 24 Hours using time Picker
        var minute: Int = addSchedule.addScheduleBinding.timePicker1.currentMinute //get Time in 24 Hours using time Picker


        var hour=""
        var min=""



        if (hours<10){
            hour= "0$hours"
        }else{
            hour=hours.toString()

        }
        if (minute<10){
            min="0$minute"
        }else{
            min=minute.toString()

        }

        route_time.set("$hour:$min")

        Log.e("timePicker3", "Hours$hour")
        Log.e("timePicker3", "Minutes$min")


        addSchedule.addScheduleBinding.timePicker1.setOnTimeChangedListener(OnTimeChangedListener { view, hourOfDay, minuteofDay ->

          //  addSchedule.addScheduleBinding.timePicker1.setIs24HourView(true) //set Timer to 24 hours Format

            var hour=""
            var min=""

            if (hourOfDay<10){
                hour= "0$hourOfDay"
            }else{
                hour=hourOfDay.toString()

            }
            if (minuteofDay<10){
                min="0$minuteofDay"
            }else{
                min=minuteofDay.toString()

            }
            Log.e("timePicker2", "Hours$hour")
            Log.e("timePicker2", "Minutes$min")
            route_time.set("$hour:$min")

        })
    }


    fun addRoute() {
        if (checkValidate()){
            if (MethodsUtil.isNetworkAvailable(context)) {
                try {
                    var token =
                        "Bearer " + PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()
                    Log.e("UpdatedFeedToken", "==>>$token")
                    var builder = MultipartBody.Builder()
                    builder.apply {
                        setType(MultipartBody.FORM)
                        addFormDataPart("date_id", dateId.get().toString())
                        addFormDataPart("route_time", route_time.get().toString())
                        addFormDataPart("lat", lat.get().toString())
                        addFormDataPart("lng", lng.get().toString())
                        addFormDataPart("name", name.get().toString())
                        addFormDataPart("address", address.get().toString())

                        if (file!=null){
                            addPartBody(this, "image", file?.path)
                        }
                    }

                    RetrofitCall.callService(context, true, token, object :
                        RequestProcess<Response<MessageRes>> {
                        override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                            return retrofitApi.addRoute(builder.build())
                        }

                        override fun onResponse(res: Response<MessageRes>) {
                            if (res.isSuccessful) {
                                var body = res.body()
                                Log.e("ResponseofCreateFeed", "==>>>" + body.toString())
                                MethodsUtil.showSnackBar(
                                    context,
                                    addSchedule.addScheduleBinding.btLogin,
                                    body?.message.toString()
                                )
                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    addSchedule.addScheduleBinding.btLogin,
                                    res.message()
                                )
                            }
                        }

                        override fun onException(message: String) {}
                    })
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                CommonAlerts.alert(context, context.getString(R.string.internet_issue))
            }
        }
    }

    override fun putAddress(latti: String, longi: String, location: String) {
        lat.set(latti)
        lng.set(longi)
        address.set(location)
    }
}