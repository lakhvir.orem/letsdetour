package com.letsdetour.views.createPost

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.letsdetour.R
import com.letsdetour.databinding.CreatePostBinding
import com.letsdetour.utils.CommonKeys
import com.letsdetour.utils.showToast
import com.letsdetour.views.mainActivity.MainActivity
import kotlinx.android.synthetic.main.account_details.*
import kotlinx.android.synthetic.main.account_details.civProfile
import kotlinx.android.synthetic.main.create_post.*
import java.io.File

class CreatePost:Fragment() {
    lateinit var createPostVM: CreatePostVM
    lateinit var createPostBinding: CreatePostBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        createPostBinding= CreatePostBinding.inflate(LayoutInflater.from(context))
        return createPostBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createPostVM= CreatePostVM(requireContext(),this)
        createPostBinding.createPostVM=createPostVM
        MainActivity.mainBinding.ivAddHome.setImageResource(R.drawable.ic_footer_add_status_selected)
        MainActivity.mainBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CommonKeys.GOOGLE_PLACES_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        Log.e("TAG", "Place: ${place.name}, ${place.id}")
                        Log.e("TAG", "Place: ${place.address}")
                        Log.e("TAG", "Place: ${place.latLng!!.latitude}")
                        Log.e("TAG", "Place: ${place.latLng!!.longitude}")
                        createPostVM.address.set(place.address)
                        createPostVM.lattitude.set(place.latLng!!.latitude.toString())
                        createPostVM.longitude.set(place.latLng!!.longitude.toString())
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                        Log.i("TAG", status.statusMessage!!)

                        createPostVM.address.set("")
                        createPostVM.lattitude.set("")
                        createPostVM.longitude.set("")
                        showToast(requireContext(),status.statusMessage!!)
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
            return
        }
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            val filePath = ImagePicker.getFilePath(data)!!
            ivUploadImage.setImageURI(fileUri)
                createPostVM.file = File(filePath)
            createPostVM.isImageSet.set(true)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            showToast(requireContext(), ImagePicker.getError(data))
        } else {
            showToast(requireContext(), "Task Cancelled")
        }
    }



    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            121 -> {
                var read = grantResults[0] == PackageManager.PERMISSION_GRANTED
                var write = grantResults[1] == PackageManager.PERMISSION_GRANTED
                if (grantResults.isNotEmpty() && read && write) {
                    createPostVM.openCamera()
                } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) && !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ) {
                    rationale()
                } else {
                    ActivityCompat.requestPermissions(
                        context as Activity, arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 121
                    )
                }
            }
        }
    }
    private fun rationale() {
        val builder: AlertDialog.Builder =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AlertDialog.Builder(
                    requireContext(),
                    android.R.style.Theme_Material_Light_Dialog_Alert
                )
            } else {
                AlertDialog.Builder(requireContext())
            }
        builder.setTitle("Mandatory Permissions")
            .setMessage("Manually allow permissions in App settings")
            .setPositiveButton("Proceed") { dialog, which ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", (context as Activity).packageName, null)
                intent.data = uri
                startActivityForResult(intent, 1)
            }
            .setCancelable(false)
            .show()
    }
}