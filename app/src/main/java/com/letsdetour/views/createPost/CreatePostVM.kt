package com.letsdetour.views.createPost

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.letsdetour.R
import com.letsdetour.response.LoginRes
import com.letsdetour.response.MessageRes
import com.letsdetour.response.UserDetailsRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.mainActivity.MainActivity
import okhttp3.MultipartBody
import retrofit2.Response
import java.io.File
import java.util.*

class CreatePostVM(val context: Context, val createPost: CreatePost):ViewModel() {

    var details= ObservableField("")
    var address= ObservableField("")
    var text= ObservableField("Create")
    var lattitude= ObservableField("")
    var longitude= ObservableField("")
    var photo= ObservableField("")
    var feedid= ObservableField("")
    var file:File?=null
    var isImageSet=ObservableBoolean(false)
    var forUpdate=ObservableBoolean(false)
    var model:UserDetailsRes.User.Feed?=null

    init {
        getBundleData()
    }


    fun getBundleData(){
        if (createPost.arguments!=null){
            var data=createPost.arguments
            if (data?.containsKey(CommonKeys.FeedData)!!){
                model= data.getParcelable<UserDetailsRes.User.Feed>(CommonKeys.FeedData)
                Log.e("RequiredData", "==>>$model")
                text.set("Update")
                details.set(model?.detail)
                address.set(model?.address)
                feedid.set(model?.id.toString())
                lattitude.set(model?.lat)
                longitude.set(model?.lng)
                forUpdate.set(true)
                var img=model?.image
                if (!img.isNullOrEmpty()){
                    isImageSet.set(true)
                    photo.set(img)
                    Glide.with(context).load(img).error(R.drawable.ic_default_upload_image).into(createPost.createPostBinding.ivUploadImage)
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun checkPermission() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) !=
            PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                context as Activity, arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 121
            )
        } else {
            openCamera()
        }
    }


    fun openCamera() {
        ImagePicker.with(createPost)
            .compress(1024)
            .crop()
            .maxResultSize(400, 400)
            .start()
    }

    fun onClickLocation(){

        if (!Places.isInitialized()) {
            Places.initialize(context, context.getString(R.string.map_apy_key), Locale.US)
        }
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
            .build(context)
        createPost.startActivityForResult(intent, CommonKeys.GOOGLE_PLACES_REQUEST_CODE)
    }

    fun validate():Boolean{
        when{
            !isImageSet.get()->{
                MethodsUtil.showSnackBar(
                    context,
                    createPost.createPostBinding.btLogin,context.getString(R.string.please_select_image))
                return false
            }
            address.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(
                    context,
                    createPost.createPostBinding.btLogin,context.getString(R.string.please_select_address))
                return false
            }
            details.get().toString().isNullOrEmpty()->{
                MethodsUtil.showSnackBar(
                    context,
                    createPost.createPostBinding.btLogin,context.getString(R.string.please_enter_descritopns))
                return false
            }
            else->{
                return true
            }
        }
    }
    fun createFeed(){
        if (validate()){
            if (forUpdate.get()){
                updateFeedApi()
            }else{
                createFeedApi()
            }
        }
    }

    fun createFeedApi() {

        if (MethodsUtil.isNetworkAvailable(context)) {

            try {
                var token="Bearer "+PrefferenceFile.retrieveKey(context,CommonKeys.TOKEN).toString()
                Log.e("CreateFeedToken", "==>>$token")
                var builder = MultipartBody.Builder()
                builder.apply {
                    setType(MultipartBody.FORM)
                    addFormDataPart("detail", details.get().toString())
                    addFormDataPart("address", address.get().toString())
                    addFormDataPart("lat", lattitude.get().toString())
                    addFormDataPart("lng", longitude.get().toString())

                    if (file!=null){
                        addPartBody(this,"image",file?.path)
                    }

                    RetrofitCall.callService(context, true, token, object :
                        RequestProcess<Response<MessageRes>> {
                        override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                            return retrofitApi.createFeed(builder.build())
                        }
                        override fun onResponse(res: Response<MessageRes>) {
                            if (res.isSuccessful) {
                                var body=res.body()
                                Log.e("ResponseofCreateFeed","==>>>"+body.toString())
                                MethodsUtil.showSnackBar(
                                    context,
                                    createPost.createPostBinding.btLogin,
                                    body?.message.toString()
                                )
                                context.startActivity(Intent(context,MainActivity::class.java))
                                (context as Activity).finishAffinity()
                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    createPost.createPostBinding.btLogin,
                                    res.message()
                                )
                            }
                        }
                        override fun onException(message: String) {}
                    })
                }


            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }

    fun updateFeedApi() {

        if (MethodsUtil.isNetworkAvailable(context)) {

            try {
                var token="Bearer "+PrefferenceFile.retrieveKey(context,CommonKeys.TOKEN).toString()
                Log.e("UpdatedFeedToken", "==>>$token")
                var builder = MultipartBody.Builder()
                builder.apply {
                    setType(MultipartBody.FORM)
                    addFormDataPart("feed_id", feedid.get().toString())
                    addFormDataPart("detail", details.get().toString())
                    addFormDataPart("address", address.get().toString())
                    addFormDataPart("lat", lattitude.get().toString())
                    addFormDataPart("lng", longitude.get().toString())

                    if (file!=null){
                        addPartBody(this,"image",file?.path)
                    }else{
                        addFormDataPart("image", photo.get().toString())

                    }

                    RetrofitCall.callService(context, true, token, object :
                        RequestProcess<Response<MessageRes>> {
                        override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                            return retrofitApi.updateFeed(builder.build())
                        }
                        override fun onResponse(res: Response<MessageRes>) {
                            if (res.isSuccessful) {
                                var body=res.body()
                                Log.e("ResponseofCreateFeed","==>>>"+body.toString())
                                MethodsUtil.showSnackBar(
                                    context,
                                    createPost.createPostBinding.btLogin,
                                    body?.message.toString()
                                )
                                context.startActivity(Intent(context,MainActivity::class.java))
                                (context as Activity).finishAffinity()
                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    createPost.createPostBinding.btLogin,
                                    res.message()
                                )
                            }
                        }
                        override fun onException(message: String) {}
                    })
                }


            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }
}