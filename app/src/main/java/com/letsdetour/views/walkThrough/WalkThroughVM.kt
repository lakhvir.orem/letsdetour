package com.letsdetour.views.walkThrough

import android.content.Context
import androidx.lifecycle.ViewModel


class WalkThroughVM(val context: Context,var walkThrough: Walkthrough):ViewModel() {


    data class WalkThroughModel(
        var image: Int = 0
    )
}