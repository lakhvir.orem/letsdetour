package com.letsdetour.views.walkThrough

import android.content.Intent
import android.database.DatabaseUtils
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.letsdetour.R
import com.letsdetour.databinding.ActivityWalkthroughBinding
import com.letsdetour.views.login.Login
import com.letsdetour.views.loginOrCreateAccount.LoginOrCreateAccount

import com.zhpan.indicator.IndicatorView
import com.zhpan.indicator.enums.IndicatorSlideMode
import com.zhpan.indicator.enums.IndicatorStyle
import kotlinx.android.synthetic.main.activity_walkthrough.*

class Walkthrough : AppCompatActivity() {
    lateinit var walkThroughVM: WalkThroughVM
    var walkThroughList = ArrayList<WalkThroughVM.WalkThroughModel>()

    lateinit var walkthroughBinding: ActivityWalkthroughBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = Color.TRANSPARENT
        }

        walkthroughBinding=DataBindingUtil.setContentView(this,R.layout.activity_walkthrough)
        walkThroughVM=WalkThroughVM(this,this)
        walkthroughBinding.walkThroughVM=walkThroughVM


        tvSkip.setOnClickListener {
            startActivity(Intent(this,LoginOrCreateAccount::class.java))
        }
     //   val viewPager = findViewById<ViewPager>(R.id.view_pager)
      //  val adapter = ViewPagerAdapter()
      //  viewPager.adapter = adapter
      //  dotsIndicator.setViewPager(viewPager)
        setWalkListData()

    }

    private fun setWalkListData() {
        walkThroughList.clear()

        walkThroughList.add(
            WalkThroughVM.WalkThroughModel(
                R.drawable.walk1
            )
        )
        walkThroughList.add(
            WalkThroughVM.WalkThroughModel(
                R.drawable.walk2
            )
        )
        walkThroughList.add(
            WalkThroughVM.WalkThroughModel(
                R.drawable.walk3
            )
        )

        val viewPager = findViewById<ViewPager>(R.id.ViewPager_Slider)
        val adapter = WalkThroughAdapter(this,walkThroughList)
        viewPager.adapter = adapter

        val indicatorView = findViewById<IndicatorView>(R.id.indicator_view)
        indicatorView.apply {
            setSliderColor(applicationContext!!.resources.getColor(R.color.light_white), applicationContext!!.resources.getColor(R.color.white))
            setSliderWidth(resources.getDimension(R.dimen._16sdp))
            setSliderHeight(resources.getDimension(R.dimen._4sdp))
            setSlideMode(IndicatorSlideMode.WORM)
            setIndicatorStyle(IndicatorStyle.ROUND_RECT)
            setupWithViewPager(viewPager)
        }


     /*   val dotsIndicator = findViewById<WormDotsIndicator>(R.id.dotsindicator)
        val viewPager = findViewById<ViewPager>(R.id.ViewPager_Slider)
        val adapter = WalkThroughAdapter(this,walkThroughList)
        viewPager.adapter = adapter
        dotsIndicator.setViewPager(viewPager)*/
    }
}