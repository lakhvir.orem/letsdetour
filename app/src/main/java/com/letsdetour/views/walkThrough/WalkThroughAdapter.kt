package com.letsdetour.views.walkThrough

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.letsdetour.R

class WalkThroughAdapter(
    var context: Context,
    var walkThroughList: ArrayList<WalkThroughVM.WalkThroughModel>
) : PagerAdapter() {
    var layoutInflater: LayoutInflater? = null

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return walkThroughList.size
    }

    @SuppressLint("ServiceCast")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = layoutInflater!!.inflate(R.layout.custom_layout_view_pager, container, false)

        val image = view.findViewById(R.id.ImageView_slider) as ImageView


        val model = walkThroughList[position]

        Glide.with(context).load(context.resources.getDrawable(model.image))
            .apply(RequestOptions().override(0, 200)).into(image)



        val vp = container as ViewPager
        vp.addView(view, 0)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }

}