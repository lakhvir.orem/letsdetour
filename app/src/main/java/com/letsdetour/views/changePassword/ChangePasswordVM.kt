package com.letsdetour.views.changePassword

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.letsdetour.R
import com.letsdetour.response.MessageRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.login.Login
import com.letsdetour.views.mainActivity.MainActivity
import retrofit2.Response

class ChangePasswordVM(val context: Context, val changePassword: ChangePassword):ViewModel(){

    var currentPass=ObservableField("")
    var newPass=ObservableField("")
    var conformPass=ObservableField("")

    fun onClickBack(){
        (context as Activity).onBackPressed()
    }

    fun validate():Boolean{
      when{
          currentPass.get().toString().isNullOrEmpty()->{
              MethodsUtil.showSnackBar(context,changePassword.changePasswordBinding.btLogin,context.getString(R.string.enter_enter_old_pass))
              return false
          }
          newPass.get().toString().isNullOrEmpty()->{
              MethodsUtil.showSnackBar(context,changePassword.changePasswordBinding.btLogin,context.getString(R.string.enter_new_password))
              return false
          }
          conformPass.get().toString().isNullOrEmpty()->{
              MethodsUtil.showSnackBar(context,changePassword.changePasswordBinding.btLogin,context.getString(R.string.pls_conf_pass))
              return false
          }
          conformPass.get().toString()!=newPass.get().toString()->{
              MethodsUtil.showSnackBar(context,changePassword.changePasswordBinding.btLogin,context.getString(R.string.password_not_match_newPss))
              return false
          }
          else->{
              return true
          }
      }
    }
    fun onClickDone(){
        if (validate()){
            changePassApi()
        }
    }
    fun changePassApi() {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token="Bearer "+ PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()

                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.changePassword(currentPass.get().toString(),newPass.get().toString(),conformPass.get().toString())
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        val response = res.body()!!

                        if (res.isSuccessful && res.code()==200) {
                            context.startActivity(
                                Intent(
                                    context,
                                    MainActivity::class.java
                                )
                            )
                            (context as Activity).finishAffinity()

                        }
                        else {
                            MethodsUtil.showSnackBar(
                                context,
                                changePassword.changePasswordBinding.btLogin,
                                response.message
                            )
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }
}