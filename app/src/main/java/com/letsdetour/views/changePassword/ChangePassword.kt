package com.letsdetour.views.changePassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.ChangePasswordBinding

class ChangePassword:Fragment() {
    lateinit var changePasswordVM: ChangePasswordVM
    lateinit var changePasswordBinding: ChangePasswordBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        changePasswordBinding= ChangePasswordBinding.inflate(LayoutInflater.from(context))
        return changePasswordBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changePasswordVM= ChangePasswordVM(requireContext(),this)
        changePasswordBinding.changePasswordVM=changePasswordVM
    }
}
