package com.letsdetour.views.filters

import android.R
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.DialogFragment
import com.letsdetour.databinding.FiltersBinding


class Filters:DialogFragment() {
    lateinit var filterVM: FilterVM
    lateinit var filtersBinding: FiltersBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        filtersBinding= FiltersBinding.inflate(LayoutInflater.from(context))
        return   filtersBinding.root


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        filterVM= FilterVM(requireContext(), this)
        filtersBinding.filterVM=filterVM

     //   MainActivity.mainBinding.btmNav.visibility=View.GONE
     //   MainActivity.mainBinding.ivAddHome.visibility=View.GONE
    }


    override fun onStart() {
        super.onStart()

        val dialog: Dialog =dialog!!

        if (dialog!=null){

            val width=ViewGroup.LayoutParams.MATCH_PARENT
            val height=ViewGroup.LayoutParams.MATCH_PARENT

            dialog.window!!.setLayout(width,height)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        }
    }
}