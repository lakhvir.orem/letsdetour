package com.letsdetour.views.filters

import android.app.Activity
import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel

class FilterVM(val context: Context, val filters: Filters):ViewModel() {
    var isAccomodation=ObservableBoolean(true)
    var isRestaurant=ObservableBoolean(false)
    var isAttraction=ObservableBoolean(false)
    var isNightLife=ObservableBoolean(false)

    init {


    }
    fun onclickBack(){
        filters.dismiss()
    }

    fun onclickAccomodation(){
      isAccomodation.set(true)
        isRestaurant.set(false)
      isAttraction.set(false)
      isNightLife.set(false)

    }

    fun onclickRestaurant(){
        isAccomodation.set(false)
        isRestaurant.set(true)
        isAttraction.set(false)
        isNightLife.set(false)
    }

    fun onclickAttraction(){
        isAccomodation.set(false)
        isRestaurant.set(false)
        isAttraction.set(true)
        isNightLife.set(false)
    }

    fun onclickNightLife(){
        isAccomodation.set(false)
        isRestaurant.set(false)
        isAttraction.set(false)
        isNightLife.set(true)
    }
}