package com.letsdetour.views.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.letsdetour.R
import com.letsdetour.response.LoginRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.forgotPassword.ForgotPassword
import com.letsdetour.views.mainActivity.MainActivity
import com.letsdetour.views.signUp.SignUp
import com.letsdetour.views.verifyMobile.VerifyMobile
import retrofit2.Response
import java.util.*

class LoginVM(val context: Context, val login: Login) : ViewModel() {

    var password = ObservableField("")
    var image=ObservableField("")
    var phone=ObservableField("")
    var email=ObservableField("")
    var socailToken=ObservableField("")
    var gender=""
    var fbName=""
    var name=ObservableField("")


    val sharedPreferenceUtils by lazy {SharedPreferenceUtils(context)}


    fun onclickBack() {
        (context as Activity).finish()
    }

    fun onClickForgot() {
        context.startActivity(Intent(context, ForgotPassword::class.java))
    }

    fun onClickSignUp() {
        context.startActivity(Intent(context, SignUp::class.java))
    }

    fun validate(): Boolean {
        when {
            email.get().toString().isNullOrEmpty() -> {
                MethodsUtil.showSnackBar(
                    context,
                    login.loginBinding.btLogin,
                    context.getString(R.string.enter_email)
                )
                return false
            }

            !email.get().toString().trim().matches(Patterns.EMAIL_ADDRESS.pattern().toRegex()) -> {
                MethodsUtil.showSnackBar(
                    context,
                    login.loginBinding.btLogin,
                    context.getString(R.string.please_enter_your_valid_email)
                )
                return false
            }

            password.get().toString().isNullOrEmpty() -> {
                MethodsUtil.showSnackBar(
                    context,
                    login.loginBinding.btLogin,
                    context.getString(R.string.enter_password)
                )
                return false
            }
            else -> {

                return true
            }
        }
    }

    fun loginSubmit() {

        if (validate()) {
            MethodsUtil.hideSoftKeyboard(context as Activity)
            loginApi()
        }
    }

    fun loginApi() {

        if (MethodsUtil.isNetworkAvailable(context)) {

            try {
                RetrofitCall.callService(context, true, "", object :
                    RequestProcess<Response<LoginRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<LoginRes> {
                        return retrofitApi.login(email.get().toString(), password.get().toString())
                    }

                    override fun onResponse(loginResponse: Response<LoginRes>) {
                        if (loginResponse.isSuccessful) {
                            val response = loginResponse.body()!!
                            Log.e("LoginRes", "Res$response")

                            if (response.otp != null) {
                                context.startActivity(
                                    Intent(
                                        context,
                                        VerifyMobile::class.java
                                    ).putExtra(CommonKeys.Email, email.get().toString())
                                        .putExtra(CommonKeys.OTP, response.otp.toString())
                                )
                                return
                            } else if (response.token != null) {

                                if (!response.user.image.isNullOrEmpty()){

                                    PrefferenceFile.storeKey(context,CommonKeys.IMAGE,response.user.image)
                                }

                                PrefferenceFile.storeKey(context,CommonKeys.FIRSTNAME,response.user.user_name)
                                PrefferenceFile.storeKey(context,CommonKeys.USERID,response.user.id.toString())


                                PrefferenceFile.storeKey(
                                    context,
                                    CommonKeys.TOKEN,
                                    response.token.toString()
                                )
                                PrefferenceFile.storeKey(context, CommonKeys.ISUSERLOGIN, "yes")
                                context.startActivity(
                                    Intent(
                                        context,
                                        MainActivity::class.java
                                    )
                                )
                              //  sharedPreferenceUtils.setprefObject(CommonKeys.USER, response)

                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    login.loginBinding.btLogin,
                                    response.message
                                )
                            }
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }
}