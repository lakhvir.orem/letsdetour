package com.letsdetour.views.chat

import android.content.Context
import androidx.lifecycle.ViewModel
import android.R.attr.button
import android.view.View
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.Toast

import com.letsdetour.views.mainActivity.MainActivity

import android.R
import android.app.Activity
import android.content.Intent
import com.letsdetour.views.addMember.AddMember
import com.letsdetour.views.archieveList.ArchiveList
import com.letsdetour.views.memberlist.MemberList


class ChatVM(val context: Context, chat: Chat):ViewModel() {



    var chatAdapter = ChatAdapter()

    fun onClickBack(view: View){
        (context as Activity).onBackPressed()
    }

    fun onClickAddMember(){
        context.startActivity(Intent(context,AddMember::class.java))
    }

    fun openPopUpmenu(view:ImageView) {
        val popupMenu = PopupMenu(context, view)
        popupMenu.menuInflater.inflate(com.letsdetour.R.menu.menu_popup, popupMenu.menu)

        popupMenu.setOnMenuItemClickListener { menuItem -> // Toast message on menu item clicked

            if (menuItem.title==context.getString(com.letsdetour.R.string.member_list)){
                context.startActivity(Intent(context,MemberList::class.java))
            }
            Toast.makeText(
                context,
                "You Clicked " + menuItem.title,
                Toast.LENGTH_SHORT
            ).show()
            true
        }
        popupMenu.show()
    }
}
