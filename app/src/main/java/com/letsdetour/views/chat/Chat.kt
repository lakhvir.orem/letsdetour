package com.letsdetour.views.chat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import com.letsdetour.R
import com.letsdetour.databinding.ActivityChatBinding
import com.letsdetour.views.mainActivity.MainActivity

class Chat : AppCompatActivity() {
    lateinit var chatBinding:ActivityChatBinding
    lateinit var chatVM: ChatVM
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chatBinding=DataBindingUtil.setContentView(this,R.layout.activity_chat)
        chatVM= ChatVM(this,this)
        chatBinding.chatVM=chatVM

    }
}