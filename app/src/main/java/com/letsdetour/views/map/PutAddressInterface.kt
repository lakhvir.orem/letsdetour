package com.letsdetour.views.map

interface PutAddressInterface {

    fun putAddress(lat:String,lng:String,location:String)
}