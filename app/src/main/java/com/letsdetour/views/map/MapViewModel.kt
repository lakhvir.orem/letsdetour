package com.letsdetour.views.map

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.letsdetour.R
import com.letsdetour.utils.CommonKeys
import java.util.*

class MapViewModel(val context: Context, val mapFrag: MapFrag):ViewModel() {
    var location=ObservableField("")
    var lat=ObservableField("")
    var lng=ObservableField("")


    companion object{
        var putAddressInterface:PutAddressInterface?=null
    }

    init {
        Log.e("LOcation", "===>>>${location.get().toString()}")
    }

    fun  onClickCancel (){
        (context as Activity).finish()
    }

    fun  onClickSelect (){
        putAddressInterface!!.putAddress(lat.get().toString(),lng.get().toString(),location.get().toString())
        (context as Activity).finish()

    }


    fun onClickLocation(){

        if (!Places.isInitialized()) {
            Places.initialize(context, context.getString(R.string.map_apy_key), Locale.US)
        }
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
            .build(context)
        mapFrag.startActivityForResult(intent, CommonKeys.GOOGLE_PLACES_REQUEST_CODE)
    }

}