package com.letsdetour.views.map

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.letsdetour.R
import com.letsdetour.databinding.MapLayoutBinding
import com.letsdetour.utils.CommonKeys
import com.letsdetour.utils.GpsTracker
import com.letsdetour.utils.showToast

import java.util.*

class MapFrag:AppCompatActivity() {

    lateinit var mapViewModel: MapViewModel
    lateinit var mapLayoutBinding: MapLayoutBinding
    private  var gpsTracker: GpsTracker? = null
    private var LOCATION_PERMISSION_RC = 1234
    var swipeTimer: Timer? = null
    private lateinit var myMap: GoogleMap
    private lateinit var address: String
    private var lattitude:Double?=null
    private  var longitude:Double?=null

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mapLayoutBinding=DataBindingUtil.setContentView(this, R.layout.map_layout)
        mapViewModel= MapViewModel(this,this)
        mapLayoutBinding.mapViewModel=mapViewModel
        mapPermission()
    }




    @RequiresApi(Build.VERSION_CODES.M)
    private fun mapPermission() {


        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {


            var showRationale =
                shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION)

            if (showRationale) {

                Log.e("showWorkAlert", "Execute")

                showViewWorkAlert()
            } else {

                requestPermissions(
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_PERMISSION_RC
                )
            }

        } else {

            gpsTracker = GpsTracker(this!!)

            if ((gpsTracker!!.canGetLocation) == true) {


                initMap()

                Log.e("getcurrentLongLatt", "Execute")

            } else {
                Log.e("showSettingsAlert", "Execute")

                showSettingsAlert()

            }
        }
    }
    var viewWork: AlertDialog? = null

    private fun showViewWorkAlert() {

        if (viewWork != null) {
            if (viewWork!!.isShowing) {
                viewWork!!.dismiss()
            }
        }

        var view: View? = null
        var layoutInflater = LayoutInflater.from(this!!)
        var builder = AlertDialog.Builder(this!!)
        view = layoutInflater.inflate(R.layout.alert_of_location_turn_on, null)
        var btAlertSelectLocation: TextView = view!!.findViewById(R.id.tvSettingAlert)

        builder.setView(view)
        builder.setCancelable(false)
        viewWork = builder.create()
        viewWork!!.show()

        viewWork!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        btAlertSelectLocation.setOnClickListener {
            var intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            var uri = Uri.fromParts("package", this!!.packageName, null)
            intent.data = uri
            startActivityForResult(intent, 8)
            viewWork!!.dismiss()
        }

    }

    private fun initMap() {

        try {


            var mapFragment: SupportMapFragment?
            mapFragment = supportFragmentManager.findFragmentById(R.id.mapAddress) as SupportMapFragment
            mapFragment.getMapAsync { googleMap ->

                myMap = googleMap
                myMap.uiSettings.isZoomControlsEnabled = true
                myMap.uiSettings.isMyLocationButtonEnabled = true

                if (gpsTracker!!.getLatitude() != 0.toDouble() && gpsTracker!!.getLongitude() != 0.toDouble()) {

                   lattitude = gpsTracker!!.getLatitude()
                  longitude = gpsTracker!!.getLongitude()

                    var latLng = LatLng(gpsTracker!!.getLatitude(), gpsTracker!!.getLongitude())
                    var cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15f)

                    myMap.animateCamera(cameraUpdate)

                    checkCamerMove()
                    checkCamerIdle()

                }


            }


        }catch (e: Exception){

            e.printStackTrace()
        }


    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMISSION_RC) {
            if (grantResults.isNotEmpty()) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    Toast.makeText(this!!, "Permission granted", Toast.LENGTH_LONG).show()

                    Log.e("ReqPermResult", "Executed")

                    gpsTracker = GpsTracker(this!!)
                    if ((gpsTracker!!.canGetLocation) == true) {
                        initMap()

                    } else {

                        showSettingsAlert()

                    }


                } else {

                    mapPermission()
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_LONG).show()
                }
            }
        }

    }

    private fun checkCamerIdle() {
        myMap.setOnCameraIdleListener(object : GoogleMap.OnCameraIdleListener {

            override fun onCameraIdle() {

                //  locationic.visibility = View.GONE

                mapViewModel.lat.set(myMap.cameraPosition.target.latitude.toString())
                mapViewModel.lng.set(myMap.cameraPosition.target.longitude.toString())
                myMap.addMarker(MarkerOptions().position(myMap.cameraPosition.target))
                // .setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pindraw))
                getOnlyAddress(myMap.cameraPosition.target)


            }
        })

    }

    fun checkCamerMove() {
        myMap.setOnCameraMoveListener(object : GoogleMap.OnCameraMoveListener {

            override fun onCameraMove() {

                try {

                    myMap.clear()

                    //    locationic.visibility = View.VISIBLE
                    var latLng: LatLng = myMap.cameraPosition.target


                    mapViewModel.lat.set(latLng.latitude.toString())
                    mapViewModel.lng.set(latLng.longitude.toString())

                } catch (e: Exception) {

                    e.printStackTrace()
                }


            }
        })
    }

    private fun getOnlyAddress(latLng: LatLng) {
        try {
            val addresses: List<Address>
            val geocoder: Geocoder = Geocoder(this, Locale.getDefault())
            addresses = geocoder.getFromLocation(
                latLng.latitude,
                latLng.longitude,
                2
            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            Log.e("AddressSize", addresses.size.toString())
            for (i in addresses.indices) {
                Log.e("MyAddress", java.lang.String.valueOf(addresses[i]))
            }
            address = addresses[0]
                .getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            Log.e("AddressofMy", address)

            val city: String = addresses[0].locality
            val state: String = addresses[0].adminArea
            val country: String = addresses[0].countryName
            val postalCode: String = addresses[0].postalCode
            val knownName: String = addresses[0].featureName

            Log.e("hello", "uygf dfgdg$country")
            // tvRestauAddress!!.text = address
            mapViewModel.location.set(address)



        } catch (e: Exception) {
            e.printStackTrace()
        }
    }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.e("resukt ", "latlong")

        when (requestCode) {

            CommonKeys.GOOGLE_PLACES_REQUEST_CODE-> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        data?.let {
                            val place = Autocomplete.getPlaceFromIntent(data)
                            Log.e("TAG", "Place: ${place.name}, ${place.id}")
                            Log.e("TAG", "Place: ${place.address}")
                            Log.e("TAG", "Place: ${place.latLng!!.latitude}")
                            Log.e("TAG", "Place: ${place.latLng!!.longitude}")
                            mapViewModel.location.set(place.address)
                            mapViewModel.lat.set(place.latLng!!.latitude.toString())
                            mapViewModel.lng.set(place.latLng!!.longitude.toString())
                            myMap.clear()

                            var latLng = LatLng(place.latLng!!.latitude, place.latLng!!.longitude)
                            var cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15f)

                            myMap.animateCamera(cameraUpdate)

                        }
                    }
                    AutocompleteActivity.RESULT_ERROR -> {
                        data?.let {
                            val status = Autocomplete.getStatusFromIntent(data)
                            Log.i("TAG", status.statusMessage!!)

                            mapViewModel.location.set("")
                            mapViewModel.lat.set("")
                            mapViewModel.lng.set("")
                            showToast(this,status.statusMessage!!)
                        }
                    }
                    Activity.RESULT_CANCELED -> {
                        // The user canceled the operation.
                    }
                }
                return
            }


            7 -> {

                gpsTracker = GpsTracker(this!!)

                if (gpsTracker!!.getLatitude() == 0.toDouble() && gpsTracker!!.getLongitude() == 0.toDouble()) {


                    Log.e("getAnylattitude", "get")


                    initMap()

                    statusTimer()

                }


            }

            8 -> {

                gpsTracker = GpsTracker(this!!)

                if ((gpsTracker!!.canGetLocation) == true) {

                    Log.e("ifGPS", "gps")

                    initMap()

                } else {

                    showSettingsAlert()

                    Log.e("elseGPS", "gps")
                }

                if (gpsTracker!!.getLatitude() != 0.toDouble() && gpsTracker!!.getLongitude() != 0.toDouble()) {

                    initMap()

                }
            }

        }



    }

    fun showSettingsAlert() {
        val alertDialog = AlertDialog.Builder(this!!)
        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings")
        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?")
        // On pressing Settings button
        alertDialog.setCancelable(false)
        alertDialog.setPositiveButton(
            "Settings"
        ) { dialog, which ->
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            this.startActivityForResult(intent, 7)
        }
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {

                alertDialog.show()


/*val i = Intent(this, Login::class.java)
startActivity(i)*/
            }
        })
        // Showing Alert Message
        alertDialog.show()

    }
    fun statusTimer() {

        Log.e("timer", "running")


        swipeTimer = Timer()
        swipeTimer!!.schedule(object : TimerTask() {
            override fun run() {

                Log.e("Is Timer Running", "True")

                runOnUiThread {

                    getcurrentLongLatt()

                }

            }
        }, 1000, 1000)//TimeInMilliSeconds
    }


    fun getcurrentLongLatt() {

        lattitude = gpsTracker!!.getLatitude()
        longitude = gpsTracker!!.getLongitude()

        Log.e("lattitudeAny", "" + lattitude)
        Log.e("lattitudeAny", "" + longitude)

        if (gpsTracker!!.getLatitude() != 0.toDouble() && gpsTracker!!.getLongitude() != 0.toDouble()) {

            Log.e("getCurrentlattitude", "get")


            Log.e("lattitudecurret", "" + lattitude)
            Log.e("lattitudecurrent", "" + longitude)

            mapViewModel.lat.set(lattitude.toString())
            mapViewModel.lng.set(longitude.toString())


            if (swipeTimer != null) {
                swipeTimer!!.cancel()
                Log.e("swipeTimer", "notNull")

            }else{

                Log.e("swipeTimer", "isNull")
            }


        }

    }



}