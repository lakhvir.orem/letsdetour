
package com.letsdetour.views.memberlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.letsdetour.R
import com.letsdetour.databinding.ActivityMemberListBinding
import com.letsdetour.views.archieveList.ArchiveListVM

class MemberList : AppCompatActivity() {
    lateinit var memberListVM: MemberListVM
    lateinit var memberListBinding: ActivityMemberListBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        memberListBinding=DataBindingUtil.setContentView(this,R.layout.activity_member_list)
        memberListVM=MemberListVM(this)
        memberListBinding.memberListVM=memberListVM
    }
}