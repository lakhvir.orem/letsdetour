package com.letsdetour.views.memberlist

import android.app.Activity
import android.content.Context
import androidx.lifecycle.ViewModel

class MemberListVM(val context: Context):ViewModel() {

    var memberListAdapter=MemberListAdapter()
    fun onClickBack(){
        (context as Activity).onBackPressed()
    }
}