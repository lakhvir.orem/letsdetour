package com.letsdetour.views.featuredDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.FeaturedDetailsBinding
import com.letsdetour.views.mainActivity.MainActivity

class FeaturedDetails:Fragment() {
    lateinit var featuredDetailsVM: FeaturedDetailsVM
    lateinit var featuredDetailsBinding: FeaturedDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        featuredDetailsBinding= FeaturedDetailsBinding.inflate(LayoutInflater.from(context))
        return featuredDetailsBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        featuredDetailsVM= FeaturedDetailsVM(requireContext())
        featuredDetailsBinding.featuredDetailsVM=featuredDetailsVM
        MainActivity.mainBinding.btmNav.visibility=View.GONE
        MainActivity.mainBinding.ivAddHome.visibility=View.GONE

    }
}