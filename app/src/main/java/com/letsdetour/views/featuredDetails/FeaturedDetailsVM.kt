package com.letsdetour.views.featuredDetails

import android.app.Activity
import android.content.Context
import androidx.lifecycle.ViewModel
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.views.addReview.AddReview

class FeaturedDetailsVM(val context: Context):ViewModel() {
    var reviewAdapter=ReviewAdapter()

    fun goToAddReview(){
        MethodsUtil.loadFragment(context,AddReview())
    }

    fun onClickBack(){
        (context as Activity).onBackPressed()
    }
}