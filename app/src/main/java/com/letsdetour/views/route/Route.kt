package com.letsdetour.views.route


import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.letsdetour.R
import com.letsdetour.response.TravelPlanDetailsRes
import com.letsdetour.utils.CommonKeys
import com.letsdetour.utils.showToast
import com.letsdetour.views.featuredListing.FeatureListAdapter
import com.letsdetour.views.featuredListing.ScrollrecyelerInterface
import kotlinx.android.synthetic.main.route.*


class Route:Fragment(),ScrollrecyelerInterface, OnMapReadyCallback {
    private var mMap: GoogleMap? = null
    var root: View? = null
    var list=ArrayList<String>()
    var dateList=ArrayList<TravelPlanDetailsRes.Data.Date>()
    var requiredPos=-1
    var featureListAdapter:FeatureListAdapter?=null
    var latlngList= ArrayList<LatLng>()
    var initLat:Double=0.0
    var destiLat:Double=0.0
    var initLng:Double=0.0
    var destiLng:Double=0.0
    var origin=""
    var destination=""
    var wayPoints=""


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (root != null) {
            val parent = root!!.parent as ViewGroup
            parent?.removeView(root)
        }
        try {
            root = inflater.inflate(R.layout.route, container, false)
        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }
        return root
      //  return routeBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FeatureListAdapter.scrollrecyelerInterface=this
        ivBack.setOnClickListener {
            onclickBack()
        }

        cvShare.setOnClickListener {
            shareLocation()
        }

        if (this.arguments!=null){
            var datesData=this.arguments
            if (datesData?.containsKey(CommonKeys.DatesList)!!){
                if (datesData.containsKey(CommonKeys.Position)){
                    requiredPos=datesData.getInt(CommonKeys.Position)
                    if (requiredPos!=null){
                        rvRouteDate.scrollToPosition(requiredPos)
                        featureListAdapter?.notifyItemChanged(requiredPos)
                        Log.e("ScrollToWhich", "==>>$requiredPos")
                    }
                }
                dateList.clear()
                dateList.addAll(
                    datesData.getParcelableArrayList<TravelPlanDetailsRes.Data.Date>(
                        CommonKeys.DatesList
                    )!!
                )
                Log.e("DatesListBundle", "===>>$dateList=====?$requiredPos")
                addDataInList()
            }
        }
    }



    fun onclickBack(){
        (context as Activity).onBackPressed()
    }

    fun shareLocation(){

        /*val gmmIntentUri =
            Uri.parse("https://www.google.com/maps/dir/?api=1&origin=18.519513,73.868315&destination=18.518496,73.879259&waypoints=18.520561,73.872435|18.519254,73.876614|18.52152,73.877327|18.52019,73.879935&travelmode=driving")
        val intent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        intent.setPackage("com.google.android.apps.maps")
        try {
            startActivity(intent)
        } catch (ex: ActivityNotFoundException) {
            try {
                val unrestrictedIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                startActivity(unrestrictedIntent)
            } catch (innerEx: ActivityNotFoundException) {
                Toast.makeText(this, "Please install a maps application", Toast.LENGTH_LONG).show()
            }
        }*/

        for (i in 0 until latlngList.size){
            if (i==0){
               initLat=latlngList[i].latitude
               initLng=latlngList[i].longitude
                 origin="&origin="+initLat+","+initLng

            }
            else if (i==latlngList.size-1){
               destiLat= latlngList[i].latitude
               destiLng= latlngList[i].longitude
                 destination="&destination="+destiLat+","+destiLng

            }else{
                 wayPoints="&waypoints="+latlngList[i].latitude.toString()+","+latlngList[i].longitude+"|"

            }
        }
        val gmmIntentUri: Uri =
            Uri.parse("https://www.google.com/maps/dir/?api=1$origin$destination$wayPoints&travelmode=driving")
        val intent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        intent.setPackage("com.google.android.apps.maps")
        try {
            context?.startActivity(intent)
        } catch (ex: ActivityNotFoundException) {
            try {
                val unrestrictedIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                context?.startActivity(unrestrictedIntent)
            } catch (innerEx: ActivityNotFoundException) {
                Toast.makeText(context, "Please install a maps application", Toast.LENGTH_LONG).show()
            }
        }

    }
    fun addDataInList() {

      var selectedRoute=  dateList[requiredPos].routes
        latlngList.clear()
        for (i in selectedRoute.indices){
            var lat=selectedRoute[i].lat
            var lng=selectedRoute[i].lng
            if (i==0){
                initLat=lat.toDouble()
                initLng=lng.toDouble()
            }
            var latLng = LatLng(lat.toDouble(), lng.toDouble())

            latlngList.add(latLng)
        }
        initMap()
        Log.e("SelectedRoutes", "===>>$latlngList")

        featureListAdapter=FeatureListAdapter(requireContext(), dateList, requiredPos)
        rvRouteDate.adapter=featureListAdapter

        featureListAdapter?.notifyDataSetChanged()

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (activity != null) {
            val mapFragment =activity?.supportFragmentManager!!
                .findFragmentById(com.letsdetour.R.id.map) as SupportMapFragment?
            mapFragment?.getMapAsync(this)
        }
    }



    override fun scrollToPos(pos: Int, routes: List<TravelPlanDetailsRes.Data.Date.Route>) {
        requiredPos=pos
        rvRouteDate.smoothScrollToPosition(pos)
        Log.e("SelectedRoutes", "===>>$routes")
        if (routes.size>0){
            latlngList.clear()
            mMap?.clear()
            for (i in routes.indices){
                var lat=routes[i].lat
                var lng=routes[i].lng
                if (i==0){
                    initLat=lat.toDouble()
                    initLng=lng.toDouble()

                    var latLng1 = LatLng(initLat, initLng)
                    var cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng1, 13f)
                    mMap?.animateCamera(cameraUpdate)
                    mMap?.addMarker(
                        MarkerOptions().position(latLng1)
                    )

                }
                var latLng = LatLng(lat.toDouble(), lng.toDouble())

                latlngList.add(latLng)
            }
            Log.e("SelectedRoutes", "===>>$latlngList")
            if (routes.size>1){
                drawPolyLineOnMap(latlngList)
            }

        }else{
            showToast(requireContext(), "no Route Found")
        }

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val sydney = LatLng(-34.0, 151.0)
        mMap!!.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(sydney))

    }

    fun drawPolyLineOnMap(list: List<LatLng?>) {
        val polyOptions = PolylineOptions()
        polyOptions.color(Color.BLACK)
        polyOptions.width(5f)
        polyOptions.addAll(list)
        mMap?.clear()
        mMap?.addPolyline(polyOptions)
        val builder = LatLngBounds.Builder()

        val markerOptions = MarkerOptions()


        for (latLng in list) {
            builder.include(latLng)
            markerOptions.position(latLng)
            mMap!!.addMarker(markerOptions)
        }
        val bounds = builder.build()

        //BOUND_PADDING is an int to specify padding of bound.. try 100.
        val cu = CameraUpdateFactory.newLatLngBounds(bounds, 100)
        mMap?.animateCamera(cu)
    }

    private fun initMap() {

        try {


            var mapFragment: SupportMapFragment?
            mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
            mapFragment.getMapAsync { googleMap ->

                mMap = googleMap
              //  mMap?.uiSettings?.isZoomControlsEnabled = true
                mMap?.uiSettings?.isMyLocationButtonEnabled = true


                    var latLng = LatLng(initLat, initLng)
                    var cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 13f)
                    mMap?.animateCamera(cameraUpdate)
                mMap?.addMarker(MarkerOptions().position(latLng))
                drawPolyLineOnMap(latlngList)

            }


        }catch (e: Exception){

            e.printStackTrace()
        }


    }
}