package com.letsdetour.views.verifyMobile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.letsdetour.R
import com.letsdetour.response.CityListRes
import com.letsdetour.response.LoginRes
import com.letsdetour.response.OtpVerifyRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.mainActivity.MainActivity
import retrofit2.Response

class VerifyMobileVM(val context: Context, val verifyMobile: VerifyMobile):ViewModel() {
    var otp=ObservableField("")
    var email=ObservableField("")
    val sharedPreferenceUtils by lazy { SharedPreferenceUtils(context) }

    init {
        getBundleData()
    }

    fun getBundleData(){
        if (verifyMobile.intent!=null){
            var data=verifyMobile.intent
            if (data.hasExtra(CommonKeys.Email)){
                email.set(data.getStringExtra(CommonKeys.Email))
                otp.set(data.getStringExtra(CommonKeys.OTP))
            }
        }
    }
    fun onclickBack(){
        (context as Activity).finish()
    }


    fun submit(){
        if (!otp.get().toString().isNullOrEmpty()){
            Log.e("EmailVal","==>>"+email.get().toString())
            Log.e("OTPVal","==>>"+otp.get().toString())
            getVerifOtp()
        }else{
            MethodsUtil.showSnackBar(context,verifyMobile.verifyMobileBinding.btVerify,"Please enter otp")
        }
    }

     fun getVerifOtp() {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                RetrofitCall.callService(context, true, "", object :
                    RequestProcess<Response<LoginRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<LoginRes> {
                        return retrofitApi.getVerifyOtp(otp.get().toString().toLong(),email.get().toString(),1)
                    }
                    override fun onResponse(res: Response<LoginRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.user != null) {
                                Log.e("VerifyOtp", "Res$response")
                                PrefferenceFile.storeKey(context,CommonKeys.ISUSERLOGIN,"yes")

                                context.startActivity(
                                    Intent(
                                        context,
                                        MainActivity::class.java
                                    ))
                             //   sharedPreferenceUtils.setprefObject(CommonKeys.USER, res)

                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    verifyMobile.verifyMobileBinding.btVerify,
                                    response.message
                                )
                            }
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }
}