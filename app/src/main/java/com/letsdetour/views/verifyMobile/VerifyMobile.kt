package com.letsdetour.views.verifyMobile

import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.letsdetour.R
import com.letsdetour.databinding.ActivityVerifyMobileBinding

class VerifyMobile : AppCompatActivity() {
    lateinit var verifyMobileVM: VerifyMobileVM
    lateinit var verifyMobileBinding: ActivityVerifyMobileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = Color.TRANSPARENT
            getWindow().decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;

        }
        verifyMobileBinding=DataBindingUtil.setContentView(this,R.layout.activity_verify_mobile)
        verifyMobileVM= VerifyMobileVM(this,this)
        verifyMobileBinding.verifyMobileVM=verifyMobileVM
    }
}