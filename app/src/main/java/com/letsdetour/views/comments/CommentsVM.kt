package com.letsdetour.views.comments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide
import com.letsdetour.R
import com.letsdetour.response.AddCommentRes
import com.letsdetour.response.FeedDetailsRes
import com.letsdetour.response.MessageRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import kotlinx.android.synthetic.main.feed_adapter.view.*
import retrofit2.Response

class CommentsVM(val context: Context, val comments: Comments):ViewModel() {
    var feedId=ObservableField("")
    var feedname=ObservableField("")
    var time=ObservableField("")
    var userName=ObservableField("")
    var commentCount=ObservableField("")
    var userimage=ObservableField("")
    var comment=ObservableField("")
    var url=ObservableField("")
    var isLike=ObservableBoolean(false)
    var commentsAdapter=CommentsAdapter(context)

    fun onClickBack(){
        (context as Activity).onBackPressed()
    }

    init {
        getBundleData()
    }

    fun getBundleData(){
        if (comments.arguments!=null){
            var data=comments.arguments
            if (data?.containsKey(CommonKeys.FeedId)!!){
                feedId.set(data.getString(CommonKeys.FeedId))
                feedDetails()

            }
        }

    }


    fun share() {
        MethodsUtil.hideSoftKeyboard(context as Activity)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, url.get().toString())
        context.startActivity(Intent.createChooser(intent, "Share via"))
    }

    fun onClickLikeDislike(view: TextView){
        if (!isLike.get()){
            addLike("1",feedId.get().toString(),view)
        }else{
            addLike("0",feedId.get().toString(),view)
        }
    }

    fun feedDetails() {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token="Bearer "+ PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()

                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<FeedDetailsRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<FeedDetailsRes> {
                        return retrofitApi.feedDetail(feedId.get().toString())
                    }

                    override fun onResponse(res: Response<FeedDetailsRes>) {
                        val response = res.body()!!

                        if (res.isSuccessful && res.code()==200) {
                            var image=response.data.user_image
                            if (!image.isNullOrEmpty()){
                                userimage.set(image)
                            }
                            userName.set(response.data.user_name)
                            feedname.set(response.data.detail)
                            time.set(MethodsUtil.getLastMessageTime(response.data.user_name))
                            if (response.data.like_status==1){
                                isLike.set(true)
                                comments.commentsBinding.tvLikeHeart.topDrawable(R.drawable.ic_like_selected)
                            }else{
                                isLike.set(false)
                                comments.commentsBinding.tvLikeHeart.topDrawable(R.drawable.ic_like_normal)

                            }
                          var commentList=response.data.comments
                            commentsAdapter.addDataInlist(commentList)
                            if (commentList.size>0){
                                commentCount.set(commentList.size.toString()+" Comments")
                                comments.commentsBinding.rvComments.scrollToPosition(commentList.size-1)
                            }else{
                                commentCount.set("0 Comment")
                            }
                        }
                        else {
                            MethodsUtil.showSnackBar(
                                context,
                                comments.commentsBinding.ivSend,
                                response.message
                            )
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }

    fun addLike(status: String, feedId: String,view: TextView) {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token="Bearer "+PrefferenceFile.retrieveKey(context,CommonKeys.TOKEN).toString()

                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.addLike(status,feedId)
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.message == "You Like this Post") {
                                Log.e("LoginRes", "Res$response")
                                showToast(context,response.message)
                                isLike.set(true)
                                view.topDrawable(R.drawable.ic_like_selected)
                            } else {
                                isLike.set(false)
                                view.topDrawable(R.drawable.ic_like_normal)
                                showToast(context,response.message)

                            }
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }


    fun sendComment(editText:EditText){
        if (!comment.get().toString().isNullOrEmpty()){
            MethodsUtil.hideSoftKeyboard(context as Activity)
            addComment(editText)
        }else{
            MethodsUtil.showSnackBar(
                context,
                comments.commentsBinding.ivSend,
               "please enter your comment"
            )
        }

    }

    fun addComment(view:EditText) {

              if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token="Bearer "+PrefferenceFile.retrieveKey(context,CommonKeys.TOKEN).toString()

                RetrofitCall.callService(context, true, "", object :
                    RequestProcess<Response<FeedDetailsRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<FeedDetailsRes> {
                        return retrofitApi.addComment(token,feedId.get().toString(),comment.get().toString())
                    }

                    override fun onResponse(res: Response<FeedDetailsRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            view.setText("")
                            var commentList=response.data.comments
                            if (commentList.size>0){
                                commentCount.set(commentList.size.toString()+" Comments")
                                comments.commentsBinding.rvComments.scrollToPosition(commentList.size-1)
                            }else{
                                commentCount.set("0 Comment")
                            }

                            commentsAdapter.addDataInlist(commentList)
                            if (commentList.size>0){
                                comments.commentsBinding.rvComments.scrollToPosition(commentList.size-1)
                            }
                            Log.e("AddCommentRes", "==>>$response")
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }
}