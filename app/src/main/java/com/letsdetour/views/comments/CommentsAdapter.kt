package com.letsdetour.views.comments

import android.content.Context
import android.os.Bundle
import com.bumptech.glide.Glide
import com.letsdetour.R
import com.letsdetour.response.FeedDetailsRes
import com.letsdetour.response.HomeDataRes
import com.letsdetour.utils.BaseAdapter
import com.letsdetour.utils.CommonKeys
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.views.profile.Profile
import kotlinx.android.synthetic.main.comments_adapter.view.*

class CommentsAdapter(val context: Context):BaseAdapter(R.layout.comments_adapter) {
    var commentList=ArrayList<FeedDetailsRes.Data.Comment>()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.apply {
            commentList.let {
                tvDesc.text= it[position].comment
                tvWelcome.text=it[position].user_name
                Glide.with(context).load(it[position].user_image).error(R.drawable.user_avatar).into(civHome)
            }
        }
        holder.itemView.tvWelcome.setOnClickListener {
            var bundle=Bundle()
            bundle.putString(CommonKeys.OtherProfile,"other")
            var profile=Profile()
            profile.arguments=bundle
            MethodsUtil.loadFragment(context,profile)
        }
    }

    override fun getItemCount(): Int {
        return commentList.size

    }

    fun addDataInlist(list: List<FeedDetailsRes.Data.Comment>){
        if (!list.isNullOrEmpty()){
            commentList.clear()
            commentList.addAll(list)
            notifyDataSetChanged()
        }

    }
}