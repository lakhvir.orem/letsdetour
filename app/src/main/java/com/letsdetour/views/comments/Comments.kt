package com.letsdetour.views.comments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.CommentsBinding
import com.letsdetour.views.mainActivity.MainActivity

class Comments:Fragment() {
    lateinit var commentsBinding: CommentsBinding
    lateinit var commentsVM: CommentsVM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        commentsBinding= CommentsBinding.inflate(LayoutInflater.from(context))
        return commentsBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        commentsVM= CommentsVM(requireContext(),this)
        commentsBinding.commentsVM=commentsVM
        MainActivity.mainBinding.btmNav.visibility=View.GONE
        MainActivity.mainBinding.ivAddHome.visibility=View.GONE
    }
}