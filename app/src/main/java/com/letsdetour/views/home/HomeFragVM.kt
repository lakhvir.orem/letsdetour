package com.letsdetour.views.home

import android.content.Context
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.letsdetour.R
import com.letsdetour.response.HomeDataRes
import com.letsdetour.utils.CommonAlerts
import com.letsdetour.utils.CommonKeys
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.utils.PrefferenceFile
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.featuredListing.FeaturedListing
import com.letsdetour.views.filters.Filters
import com.letsdetour.views.notifications.Notifications
import retrofit2.Response
import java.util.*

class HomeFragVM(val context: Context, val home: Home, val childFragmentManager: FragmentManager):ViewModel() {

    var name=ObservableField("")
    var image=ObservableField("")
    var accomodationAdapter=AccomodationAdapter(context)
    var featuredRestaurantsAdapter=FeaturedRestaurantsAdapter(context)
    var feedAdapter=FeedAdapter(context)
    var lat=ObservableField("")
    var lng=ObservableField("")
    var address=ObservableField("")
    var type=ObservableField("restaurant")
    var isResponse=ObservableBoolean(false)

    init {
        name.set(PrefferenceFile.retrieveKey(context,CommonKeys.FIRSTNAME))
        if (!PrefferenceFile.retrieveKey(context, CommonKeys.IMAGE).isNullOrEmpty()) {
            image.set(PrefferenceFile.retrieveKey(context, CommonKeys.IMAGE))
        }
    }


    fun gotoFeaturedListing(){
        MethodsUtil.loadFragment(context, FeaturedListing())
    }

    fun onclickNotifications(){
        MethodsUtil.loadFragment(context, Notifications())
    }

    fun onclickSearch(){
        val dialogFragment =
            Filters()
        dialogFragment.show(childFragmentManager, "dialogFrag")
    }

    fun getHomeData() {

        if (MethodsUtil.isNetworkAvailable(context)) {
            var token="Bearer "+PrefferenceFile.retrieveKey(context,CommonKeys.TOKEN).toString()
            Log.e("token","dhjshdush"+token)
            try {
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<HomeDataRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<HomeDataRes> {
                        return retrofitApi.getHomeData(lat.get().toString(),lng.get().toString(),type.get().toString())
                    }

                    override fun onResponse(res: Response<HomeDataRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            Log.e("LoginRes", "Res$response")
                            isResponse.set(true)
                            var feedList=response.data.feeds
                            feedAdapter.addDataInlist(feedList)

                        }  else if (res.code()==401) {
                            PrefferenceFile.logout(context)
                                MethodsUtil.showSnackBar(
                                    context,
                                    home.homeFragBinding.tvLetsdetour,
                                    res.message().toString()
                                )
                            }else{
                            MethodsUtil.showSnackBar(
                                context,
                                home.homeFragBinding.tvLetsdetour,
                                res.message().toString()
                            )
                        }
                        }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }

    fun onClickLocation(){

        if (!Places.isInitialized()) {
            Places.initialize(context, context.getString(R.string.map_apy_key), Locale.US)
        }
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
            .build(context)
        home.startActivityForResult(intent, CommonKeys.GOOGLE_PLACES_REQUEST_CODE)
    }
}