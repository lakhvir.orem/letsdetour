package com.letsdetour.views.home

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import com.letsdetour.R
import com.letsdetour.response.HomeDataRes
import com.letsdetour.response.MessageRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.comments.Comments
import kotlinx.android.synthetic.main.feed_adapter.view.*
import retrofit2.Response

class FeedAdapter(val context: Context):com.letsdetour.utils.BaseAdapter(R.layout.feed_adapter){
    var feedList=ArrayList<HomeDataRes.Data.Feed>()
    var status="1"
    var userId=""
    init {
        userId=PrefferenceFile.retrieveKey(context,CommonKeys.USERID).toString()
        Log.e("userId", "==>>$userId")
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.apply {
            feedList.let {
                Glide.with(context).load(feedList[position].user_image).error(R.drawable.user_avatar).into(civHome)
                Glide.with(context).load(feedList[position].image).error(R.drawable.ic_default_upload_image).into(ivFeedImage)
                tvWelcome.text=feedList[position].user_name
                tvDesc.text=feedList[position].detail

                if (feedList[position].user_id.toString()!=userId.toString()){
                    if (feedList[position].follow_status=="1"){
                        tvFollow.text="Unfollow"
                    }
                    else if  (feedList[position].follow_status=="2"){
                        tvFollow.text="Requested"
                    }else{
                        tvFollow.text="Follow"
                    }
                }else{
                    tvFollow.visibility=View.GONE
                }

                tvFollow.setOnClickListener {
                    if (feedList[position].follow_status=="0"){
                        addFollow("1",feedList[position].user_id.toString(),tvFollow,position)
                    }
                    if (feedList[position].follow_status=="1"){
                        addFollow("0",feedList[position].user_id.toString(),tvFollow,position)
                    }
                }

                if (feedList[position].total_comments=="1"){
                    tvComments.text=feedList[position].total_comments+" Comment"
                }else{
                    tvComments.text=feedList[position].total_comments+" Comments"

                }
                if (feedList[position].total_likes==1){
                    tvLikes.text=feedList[position].total_likes.toString()+" Like"
                }else{
                    tvLikes.text=feedList[position].total_likes.toString()+" Likes"

                }
                if (feedList[position].like_status==1){
                    tvLike.topDrawable(R.drawable.ic_like_selected)
                }else{
                    tvLike.topDrawable(R.drawable.ic_like_normal)

                }
                tvTime.text=MethodsUtil.getLastMessageTime(feedList[position].created_at)
            }
        }

        holder.itemView.ivFeedImage.setOnClickListener {
            var bundle= Bundle()
            bundle.putString(CommonKeys.FeedId,feedList[position].id.toString())
            val comments=Comments()
            comments.arguments=bundle
            MethodsUtil.loadFragment(context,comments)
        }

        holder.itemView.tvComment.setOnClickListener {
            var bundle= Bundle()
            bundle.putString(CommonKeys.FeedId,feedList[position].id.toString())
            val comments=Comments()
            comments.arguments=bundle
            MethodsUtil.loadFragment(context,comments)
        }
        holder.itemView.tvShare.setOnClickListener {
            share(feedList[position].image)
        }
        holder.itemView.tvLike.setOnClickListener {
            if (feedList[position].like_status==1){
                status="0"
                feedList[position].like_status=0
                addLike(status,feedList[position].id.toString(),holder.itemView.tvLike,holder.itemView.tvLikes,position)

            }else{
                status="1"
                feedList[position].like_status=1
                addLike(status,feedList[position].id.toString(),holder.itemView.tvLike,holder.itemView.tvLikes,position)

            }
        }
    }

    override fun getItemCount(): Int {
        return feedList.size
    }

    fun addDataInlist(list: List<HomeDataRes.Data.Feed>){
        if (!list.isNullOrEmpty()){
            feedList.clear()
            feedList.addAll(list)
            notifyDataSetChanged()
        }

    }

    fun share(url:String) {
        MethodsUtil.hideSoftKeyboard(context as Activity)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, url)
        context.startActivity(Intent.createChooser(intent, "Share via"))
    }



    fun addLike(status: String, feedId: String, view: TextView, totalLikes: TextView, position: Int) {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token="Bearer "+PrefferenceFile.retrieveKey(context,CommonKeys.TOKEN).toString()

                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.addLike(status,feedId)
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.message == "You Like this Post") {
                                Log.e("LoginRes", "Res$response")
                                showToast(context,response.message)
                                var likeCount=feedList[position].total_likes.toInt()+1
                                Log.e("likeCount", "Res$likeCount")
                                feedList[position].total_likes=feedList[position].total_likes+1
                                totalLikes.text= "$likeCount Likes"
                                view.topDrawable(R.drawable.ic_like_selected)
                                notifyDataSetChanged()

                            } else {
                                var dislikeCount=feedList[position].total_likes.toInt()-1
                                Log.e("dislikeCount", "Res$dislikeCount")
                                feedList[position].total_likes=feedList[position].total_likes-1

                                totalLikes.text= "$dislikeCount Like"
                                view.topDrawable(R.drawable.ic_like_normal)
                                showToast(context,response.message)
                                notifyDataSetChanged()

                            }
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }

    fun addFollow(status: String, followerId: String,  textview: TextView, position: Int) {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token="Bearer "+PrefferenceFile.retrieveKey(context,CommonKeys.TOKEN).toString()

                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.addFollow(status,followerId)
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (res.code() == 200) {
                                Log.e("LoginRes", "Res$response")
                                showToast(context,response.message)
                                var userid=feedList[position].user_id
                                if (status=="1"){
                                    for (i in 0 until feedList.size){
                                        if (feedList[i].user_id==userid){
                                            feedList[i].follow_status="2"
                                            textview.text= "Requested"
                                            notifyDataSetChanged()
                                        }
                                    }
                                }

                                if (status=="0"){
                                    for (i in 0 until feedList.size){
                                        if (feedList[i].user_id==userid){
                                            feedList[i].follow_status="0"
                                            textview.text= "Follow"
                                            notifyDataSetChanged()
                                        }
                                    }
                                }

                            } else {
                                showToast(context,response.message)
                            }
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }
}