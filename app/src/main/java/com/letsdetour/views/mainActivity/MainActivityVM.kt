package com.letsdetour.views.mainActivity

import android.content.Context
import android.view.Gravity
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.letsdetour.R
import com.letsdetour.utils.CommonKeys
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.views.chatList.ChatList
import com.letsdetour.views.createPost.CreatePost
import com.letsdetour.views.home.Home
import com.letsdetour.views.privacyPolicy.PrivacyPolicy
import com.letsdetour.views.profile.Profile
import com.letsdetour.views.termsAndConditions.TermsAndConditions
import com.letsdetour.views.travelPlans.TravelPlans

class MainActivityVM(val context: Context, val mainActivity: MainActivity):ViewModel() {

    var isTravelClick=ObservableBoolean(false)
    init {
        getBundleData()
    }

    fun getBundleData(){
        if(mainActivity.intent.hasExtra(CommonKeys.Privacy) || mainActivity.intent.hasExtra(CommonKeys.Terms)){
            if (mainActivity.intent.getStringExtra(CommonKeys.Privacy)=="privacy"){
                MethodsUtil.addFragment(context,PrivacyPolicy())
            }
            else if ((mainActivity.intent.getStringExtra(CommonKeys.Privacy)=="terms")){
                MethodsUtil.addFragment(context,TermsAndConditions())

            }else{
                MethodsUtil.addFragment(context,Home())
            }
        }
        else{
            MethodsUtil.addFragment(context,Home())
        }
    }

    fun openDrawer(){
        MainActivity.mainBinding.drawerLayout.openDrawer(Gravity.LEFT)

    }

    fun closeDrawer(){
        MainActivity.mainBinding.drawerLayout.closeDrawer(Gravity.RIGHT)

    }

    private fun openFragment(fragment: Fragment) {
        (context as FragmentActivity).supportFragmentManager.beginTransaction()
            .replace(R.id.frame, fragment)
            .commit()
    }

    var navigationClick =
        BottomNavigationView.OnNavigationItemSelectedListener { p0 ->
            when (p0.itemId) {

                R.id.iHome -> {
                    mainActivity.clearStack()
                    val fragmentInFrame: Fragment? = mainActivity.supportFragmentManager
                        .findFragmentById(R.id.frame)
                    isTravelClick.set(false)

                    if (fragmentInFrame !is Home) {
                        openFragment(Home())
                    }

                }

                R.id.iTravel -> {
                    val fragmentInFrame: Fragment? = mainActivity.supportFragmentManager
                        .findFragmentById(R.id.frame)
                    isTravelClick.set(false)

                    if (fragmentInFrame !is TravelPlans) {
                        MethodsUtil.loadFragment(context, TravelPlans())
                    }

                }
                R.id.iadd -> {
                    val fragmentInFrame: Fragment? = mainActivity.supportFragmentManager
                        .findFragmentById(R.id.frame)
                    isTravelClick.set(true)
                    if (fragmentInFrame !is CreatePost) {
                        MethodsUtil.loadFragment(context, CreatePost())
                    }
                }
                R.id.iChat -> {
                    val fragmentInFrame: Fragment? = mainActivity.supportFragmentManager
                        .findFragmentById(R.id.frame)
                    isTravelClick.set(false)

                    if (fragmentInFrame !is ChatList) {
                        MethodsUtil.loadFragment(context, ChatList())
                    }
                }

                R.id.iProfile -> {
                    val fragmentInFrame: Fragment? = mainActivity.supportFragmentManager
                        .findFragmentById(R.id.frame)
                    isTravelClick.set(false)

                    if (fragmentInFrame !is Profile) {
                        MethodsUtil.loadFragment(context, Profile())
                    }
                }
            }
            true

        }
}