package com.letsdetour.views.mainActivity

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentManager
import com.letsdetour.R
import com.letsdetour.databinding.ActivityMainBinding
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.views.aboutUs.AboutUs
import com.letsdetour.views.account_details.AccountDetials
import com.letsdetour.views.chat.Chat
import com.letsdetour.views.chatList.ChatList
import com.letsdetour.views.contctUs.ConatctUs
import com.letsdetour.views.createPost.CreatePost
import com.letsdetour.views.home.Home
import com.letsdetour.views.privacyPolicy.PrivacyPolicy
import com.letsdetour.views.profile.OpenDrawerInterface
import com.letsdetour.views.profile.Profile
import com.letsdetour.views.termsAndConditions.TermsAndConditions
import com.letsdetour.views.travelPlans.TravelPlans
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OpenDrawerInterface {
    lateinit var mainActivityVM: MainActivityVM

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var mainBinding: ActivityMainBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = Color.TRANSPARENT
            getWindow().decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;

        }
        mainBinding = DataBindingUtil.setContentView(this, com.letsdetour.R.layout.activity_main)
        mainActivityVM = MainActivityVM(this, this)
        mainBinding.mainActivityVM = mainActivityVM
        setStatusBarColor(this.resources.getColor(com.letsdetour.R.color.white))
        Profile.openDrawerInterface = this

        managerClickOfDrawer()

        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


    }

    fun setStatusBarColor(@ColorInt color: Int) {
        drawer_layout.setStatusBarBackgroundColor(color)
    }

    fun clearStack() {
        val fm: FragmentManager =
            supportFragmentManager // or 'getSupportFragmentManager();'
        val count = fm.backStackEntryCount
        for (i in 0 until count) {
            fm.popBackStack()
        }
    }

    override fun onClickMenu() {
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        openDrawer()
    }

    fun openDrawer() {

        if (drawer_layout?.isDrawerOpen(Gravity.RIGHT)!!) {
            drawer_layout?.closeDrawer(Gravity.RIGHT)!!
        } else {
            drawer_layout?.openDrawer(Gravity.RIGHT)!!
        }
    }

    fun managerClickOfDrawer() {

        tvAbout.setOnClickListener {
            openDrawer()
            MethodsUtil.loadFragment(this, AboutUs())
        }

        tvAccDetails.setOnClickListener {
            openDrawer()
            MethodsUtil.loadFragment(this, AccountDetials())
        }

        tvPrivacyText.setOnClickListener {
            openDrawer()
            MethodsUtil.loadFragment(this, PrivacyPolicy())
        }

        tvTermsAndCon.setOnClickListener {
            openDrawer()
            MethodsUtil.loadFragment(this, TermsAndConditions())
        }

        tvContactUs.setOnClickListener {
            openDrawer()
            MethodsUtil.loadFragment(this, ConatctUs())
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()

        var f = supportFragmentManager.findFragmentById(R.id.frame)
        val fl = findViewById<View>(R.id.frame) as FrameLayout

        Log.e("FragWhichis", "===>>>$f")
        when (f) {

            is Home -> {
                clearStack()
                mainBinding.ivAddHome.setImageResource(R.drawable.ic_footer_add_status_normal)
                mainBinding.btmNav.visibility = View.VISIBLE
                mainBinding.btmNav.menu.getItem(0).isChecked = true
                mainBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

            }

            is TravelPlans -> {
                mainBinding.ivAddHome.setImageResource(R.drawable.ic_footer_add_status_normal)
                mainBinding.btmNav.visibility = View.VISIBLE
                mainBinding.btmNav.menu.getItem(1).isChecked = true
                mainBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

            }

            is CreatePost -> {
                mainBinding.ivAddHome.setImageResource(R.drawable.ic_footer_add_status_selected)
                mainBinding.btmNav.visibility = View.VISIBLE
                mainBinding.btmNav.menu.getItem(2).isChecked = true
                mainBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

            }

            is ChatList -> {
                mainBinding.ivAddHome.setImageResource(R.drawable.ic_footer_add_status_normal)
                mainBinding.btmNav.visibility = View.VISIBLE
                mainBinding.btmNav.menu.getItem(3).isChecked = true
                mainBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

            }

            is Profile -> {
                mainBinding.ivAddHome.setImageResource(R.drawable.ic_footer_add_status_normal)
                mainBinding.ivAddHome.visibility=View.VISIBLE

                mainBinding.btmNav.visibility = View.VISIBLE
                mainBinding.btmNav.menu.getItem(4).isChecked = true
                mainBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


            }

        }


    }


}