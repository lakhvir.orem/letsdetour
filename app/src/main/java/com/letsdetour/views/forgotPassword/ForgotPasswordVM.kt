package com.letsdetour.views.forgotPassword

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.letsdetour.R
import com.letsdetour.response.MessageRes
import com.letsdetour.utils.CommonAlerts
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.utils.showToast
import com.letsdetour.views.login.Login
import com.letsdetour.views.login.LoginVM
import retrofit2.Response

class ForgotPasswordVM(val context: Context, val forgotPassword: ForgotPassword):ViewModel() {

    var email=ObservableField("")
    fun onclickBack(){
        (context as Activity).finish()
    }
    fun submit(){
        if (!email.get().toString().isNullOrEmpty()){
            forgotPassApi()
        }else{
            MethodsUtil.showSnackBar(
                context,
                forgotPassword.forgotPasswordBinding.btLogin,
                context.getString(R.string.enter_email)
            )
        }
    }

     fun forgotPassApi() {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                RetrofitCall.callService(context, true, "", object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.forgotPassword(email.get().toString())
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            if (response.message == "Reset Password Link Sent") {
                                Log.e("LoginRes", "Res$response")
                                showToast(context,response.message)
                                context.startActivity(
                                    Intent(
                                        context,
                                        Login::class.java
                                    )
                                )
                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    forgotPassword.forgotPasswordBinding.btLogin,
                                    response.message
                                )
                            }
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }
}