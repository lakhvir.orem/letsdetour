package com.letsdetour.views.travelsDetails

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.letsdetour.R
import com.letsdetour.databinding.ViewPagerAdapterBinding
import com.letsdetour.response.MessageRes
import com.letsdetour.response.TravelPlanDetailsRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.mainActivity.MainActivity
import com.letsdetour.views.previewImage.PreviewImageActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.view_pager_adapter.view.*
import retrofit2.Response


class TravelViewPager(var context: Context, val travelDetailsVM: TravelDetailsVM)  : PagerAdapter() {

    var viewPagerAdapterBinding: ViewPagerAdapterBinding? = null

    var productlList= ArrayList<TravelPlanDetailsRes.Data.Images>()

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        Log.e("imgesLIst","size===="+productlList.size)
        return productlList.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        viewPagerAdapterBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.view_pager_adapter,
            container,
            false
        )
        var imageview=viewPagerAdapterBinding!!.root.ivSlider
        var ivDelete=viewPagerAdapterBinding!!.root.ivDelete

        container.addView(viewPagerAdapterBinding!!.root)

        if (productlList.size >= 0) {
            Picasso.get().load(productlList[position].image.toString()).resize(400, 200)
                .into(imageview);
        }

        ivDelete.setOnClickListener {
            deletePlanimages(productlList[position].plan_id.toString(),productlList[position].id.toString(),position)
        }

        imageview.setOnClickListener {
            context.startActivity(Intent(context,PreviewImageActivity::class.java).putExtra(CommonKeys.IMAGE,productlList[position].image))
        }

        return viewPagerAdapterBinding!!.root
    }

    override fun getItemPosition(`object`: Any): Int {
        if (productlList.contains(`object`)) {
                return productlList.indexOf(`object`);
            }
        else {
            return POSITION_NONE;
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    fun addList(list2: List<TravelPlanDetailsRes.Data.Images>) {
        if (list2 != null) {
            productlList.clear()
            productlList.addAll(list2)
            Log.e("listSizeeeeeeeee", productlList.toString())
            notifyDataSetChanged()
        }
    }

    fun deletePlanimages(planid:String,imageId:String,position: Int) {
        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token =
                    "Bearer " + PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.deletePlanImage(planid,imageId)
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            showToast(context,response.message.toString())
                            productlList.removeAt(position)
                            notifyDataSetChanged()
                        } else {
                            showToast(context, res.message().toString())
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }

    }
}