package com.letsdetour.views.travelsDetails

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import com.letsdetour.R
import com.letsdetour.response.TravelPlanDetailsRes
import com.letsdetour.utils.BaseAdapter
import com.letsdetour.utils.CommonKeys
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.views.addSchedule.AddSchedule
import com.letsdetour.views.route.Route
import kotlinx.android.synthetic.main.trip_schedule_adapter.view.*

class TravelScheduleAdapter(val context: Context):BaseAdapter(R.layout.trip_schedule_adapter) {
    var placesAdapter:PlacesAdapter?=null

    var isAdapterset=false

    var dateList=ArrayList<TravelPlanDetailsRes.Data.Date>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvAddOtherPlaces.setOnClickListener {
            var bundle=Bundle()
            bundle.putString(CommonKeys.DateID,dateList[position].id.toString())
            bundle.putString(CommonKeys.PlanDate,dateList[position].plan_date)
            var addSchedule=AddSchedule()
            addSchedule.arguments=bundle
            MethodsUtil.loadFragment(context, addSchedule)
        }
        if (!isAdapterset){
             placesAdapter=PlacesAdapter(context,dateList)
            holder.itemView.rvAddPlaces.adapter=placesAdapter
        }else{
            notifyDataSetChanged()
        }

        if (dateList[position].routes.isNotEmpty()){
            holder.itemView.tvRoute.visibility=View.VISIBLE
            var routeList=dateList[position].routes
            placesAdapter?.addDataInList(routeList)
        }else{
            holder.itemView.tvRoute.visibility=View.GONE
        }

        holder.itemView.rvAddPlaces.setOnClickListener {
            Log.e("RecyclerviewClieck","==->>true")
        }

        holder.itemView.apply {
            dateList.let {
                tvTitleDetails.text=MethodsUtil.getDateName(dateList[position].plan_date)+"TH"
            }
        }

        holder.itemView.tvRoute.setOnClickListener {
            var bundle=Bundle()
            bundle.putParcelableArrayList(CommonKeys.DatesList,dateList)
            bundle.putInt(CommonKeys.Position,position)
            var route=Route()
            route.arguments=bundle
            MethodsUtil.loadFragment(context,route)
        }
    }

    override fun getItemCount(): Int {
        return dateList.size
    }

    fun addDataInList(list: List<TravelPlanDetailsRes.Data.Date>) {
        if (!list.isNullOrEmpty()){
            dateList.clear()
            dateList.addAll(list)
            notifyDataSetChanged()
        }
    }
}