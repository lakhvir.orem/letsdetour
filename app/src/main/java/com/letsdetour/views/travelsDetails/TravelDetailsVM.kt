package com.letsdetour.views.travelsDetails

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentResolver
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.github.dhaval2404.imagepicker.ImagePicker
import com.letsdetour.R
import com.letsdetour.databinding.CommonAlertBinding
import com.letsdetour.response.FollowerListRes
import com.letsdetour.response.GetPlanListRes
import com.letsdetour.response.MessageRes
import com.letsdetour.response.TravelPlanDetailsRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.calender.CalenderUI
import com.letsdetour.views.createPlan.CreatePlan
import com.letsdetour.views.forwardEmail.ForwardEmail
import com.letsdetour.views.mainActivity.MainActivity
import com.letsdetour.views.userList.UserList
import okhttp3.MultipartBody
import retrofit2.Response
import java.io.File


class TravelDetailsVM(val context: Context, val travelDetails: TravelDetails):ViewModel(),FetchTravelDetailsInterface {
    var travelScheduleAdapter=TravelScheduleAdapter(context)
    var travelDetailsId=ObservableField("")
    var travelAddress=ObservableField("")
    var fromTo=ObservableField("")
    var range=ObservableField("")
    var Travelid=ObservableField("")
    var first=ObservableField("")
    var second=ObservableField("")
    var third=ObservableField("")
    var firstImage=ObservableBoolean(false)
    var secondImage=ObservableBoolean(false)
    var thirdImage=ObservableBoolean(false)
    var viewPagerList:List<TravelPlanDetailsRes.Data.Images>?=null
    var imagesList=ArrayList<String>()
    var model= GetPlanListRes.Data()
    var followerList=ArrayList<TravelPlanDetailsRes.Data.Followers>()
    var file:File?=null
    var viewPagerAdapter=TravelViewPager(context,this)
    var isImage=ObservableBoolean(true)
    var isImageSet=ObservableBoolean(true)

   companion object{
       var isOverView=ObservableBoolean(true)
       var isITinary=ObservableBoolean(false)
   }

    init {
        getBundleData()
        PlacesAdapter.fetchTravelDetailsInterface=this
    }

    private fun getContactList() {
        val cr: ContentResolver = context.contentResolver
        val cur: Cursor? = cr.query(
            ContactsContract.Contacts.CONTENT_URI,
            null, null, null, null
        )
        if ((cur?.count ?: 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                val id: String = cur.getString(
                    cur.getColumnIndex(ContactsContract.Contacts._ID)
                )
                val name: String = cur.getString(
                    cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME
                    )
                )
                if (cur.getInt(
                        cur.getColumnIndex(
                            ContactsContract.Contacts.HAS_PHONE_NUMBER
                        )
                    ) > 0
                ) {
                    val pCur: Cursor? = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        arrayOf(id),
                        null
                    )
                    while (pCur?.moveToNext()!!) {
                        val phoneNo: String = pCur.getString(
                            pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER
                            )
                        )
                        Log.i(TAG, "Name: $name")
                        Log.i(TAG, "Phone Number: $phoneNo")
                    }
                    pCur?.close()
                }
            }
        }
        cur?.close()
    }

   fun  getBundleData(){
       if (travelDetails.arguments!=null){
           var data=travelDetails.arguments
           if (data?.containsKey(CommonKeys.TRAEVELID)!!){
               var travelid=data.getString(CommonKeys.TRAEVELID)
               Travelid.set(travelid)
               planDetails(travelid,true)
           }
       }
    }


    fun onClickImage(){
        var bundle=Bundle()
        var userList=UserList()
        bundle.putString(CommonKeys.TRAEVELID2,Travelid.get().toString())
        bundle.putParcelableArrayList(CommonKeys.FOLLOWERSLIST,followerList)
        userList.arguments=bundle
        MethodsUtil.loadFragment(context,userList)
    }

    @RequiresApi(Build.VERSION_CODES.M)
     fun checkPermission() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) !=
            PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                context as Activity, arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 121
            )
        } else {
            openCamera()
        }
    }

    fun openCalender(){
        context?.startActivity(Intent(context, CalenderUI::class.java))
    }

    fun openPopUpmenu(view: ImageView) {
        val popupMenu = PopupMenu(context, view)
        popupMenu.menuInflater.inflate(com.letsdetour.R.menu.travel_details_menu, popupMenu.menu)

        popupMenu.setOnMenuItemClickListener { menuItem -> // Toast message on menu item clicked

            if (menuItem.title==context.getString(com.letsdetour.R.string.share)){
                share(travelAddress.get().toString())
            }
            else if (menuItem.title==context.getString(com.letsdetour.R.string.edit)){
                var bundle= Bundle()
                bundle.putParcelable(CommonKeys.PlanData, model)
                val createPlans= CreatePlan()
                createPlans.arguments=bundle
                MethodsUtil.loadFragment(context, createPlans)
            }
            else{

                alertForDelte("Are you sure you want to delete this travel plan", "delete")


                Toast.makeText(
                    context,
                    "You Clicked " + menuItem.title,
                    Toast.LENGTH_SHORT
                ).show()
            }

            true
        }
        popupMenu.show()
    }

    fun updateImageApi() {

        if (MethodsUtil.isNetworkAvailable(context)) {

            try {
                var token="Bearer "+PrefferenceFile.retrieveKey(context,CommonKeys.TOKEN).toString()
                Log.e("UpdatedFeedToken", "==>>$token")
                var builder = MultipartBody.Builder()
                builder.apply {
                    setType(MultipartBody.FORM)
                    addFormDataPart("plan_id", Travelid.get().toString())

                    if (imagesList.size>0){
                        for (i in 0 until imagesList.size){
                            var file= File(imagesList[i])
                            addPartBody(this,"images[]",file.path)
                        }
                    }

                    RetrofitCall.callService(context, true, token, object :
                        RequestProcess<Response<MessageRes>> {
                        override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                            return retrofitApi.addImages(builder.build())
                        }
                        override fun onResponse(res: Response<MessageRes>) {
                            if (res.isSuccessful) {
                                var body=res.body()
                                Log.e("ResponseofCreateFeed","==>>>"+body.toString())
                                MethodsUtil.showSnackBar(
                                    context,
                                    travelDetails.travelPlansBinding.btLogin,
                                    body?.message.toString()
                                )
                                planDetails(Travelid.get().toString(),false)

                            } else {
                                MethodsUtil.showSnackBar(
                                    context,
                                    travelDetails.travelPlansBinding.btLogin,
                                    res.message()
                                )
                            }
                        }
                        override fun onException(message: String) {}
                    })
                }


            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }


    fun alertForDelte(message: String, type: String){
        try{
            val builder = AlertDialog.Builder(context)
            val layout = CommonAlertBinding.inflate(LayoutInflater.from(context), null)
            builder.setCancelable(false)
            builder.setView(layout.root)
            val tvMessage = layout.tvMessage
            val clOk = layout.clOk
            val clYes = layout.clYes
            val clNo = layout.clNo
            val dialog = builder.create()

            tvMessage.text = message

            if (type=="delete"){
                clOk.visibility=View.GONE
                clNo.visibility=View.VISIBLE
                clYes.visibility=View.VISIBLE
            }else{
                clOk.visibility=View.VISIBLE
                clNo.visibility=View.GONE
                clYes.visibility=View.GONE
            }

            clOk.setOnClickListener {
                dialog.dismiss()
            }
            clYes.setOnClickListener {
                deletePlan()
                dialog.dismiss()
            }

            clNo.setOnClickListener {
                dialog.dismiss()
            }

            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }catch (e: Exception){
            e.printStackTrace()
        }
    }


    fun deletePlan() {

        if (MethodsUtil.isNetworkAvailable(context)) {
            var token =
                "Bearer " + PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()

            try {
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.deletePlan(travelDetailsId.get().toString())
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        if (res.code() == 200) {
                            showToast(context, res.body()?.message.toString())
                            context.startActivity(Intent(context, MainActivity::class.java))
                            (context as Activity).finishAffinity()
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }


        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }


    fun share(url: String) {
        MethodsUtil.hideSoftKeyboard(context as Activity)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, url)
        context.startActivity(Intent.createChooser(intent, "Share via"))
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun onCLickShare(){

        val builder = AlertDialog.Builder(context)

        val customLayout: View = LayoutInflater.from(context)
            .inflate(
                com.letsdetour.R.layout.share_travel_plan,
                null
            )
        builder.setView(customLayout)

        var dialog=builder.create()
        dialog.getWindow()?.setBackgroundDrawableResource(android.R.color.transparent);
        val close = customLayout.findViewById(com.letsdetour.R.id.ivClose) as ImageView

        val followList = customLayout.findViewById(com.letsdetour.R.id.tvFollowerList) as TextView
        val contectList = customLayout.findViewById(com.letsdetour.R.id.tvContactList) as TextView

        close.setOnClickListener {
            dialog.dismiss()
        }
        followList.setOnClickListener {
            dialog.dismiss()
            var bundle=Bundle()
            bundle.putString(CommonKeys.TRAEVELID,Travelid.get().toString())
            var userList=UserList()
            userList.arguments=bundle
            MethodsUtil.loadFragment(context, userList)
        }
        contectList.setOnClickListener {
            dialog.dismiss()
            travelDetails.checkPermission()
        }
        dialog.show()
    }


    fun planDetails(travelid: String?,boolean: Boolean) {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token="Bearer "+ PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()

                RetrofitCall.callService(context, boolean, token, object :
                    RequestProcess<Response<TravelPlanDetailsRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<TravelPlanDetailsRes> {
                        return retrofitApi.travelDetails(travelid.toString())
                    }

                    override fun onResponse(res: Response<TravelPlanDetailsRes>) {
                        val response = res.body()!!
                        if (res.isSuccessful && res.code() == 200) {
                            Log.e("Travel DetailsRes", "==>>$response")
                            travelDetailsId.set(response.data.id.toString())
                            travelAddress.set(response.data.address.toString())
                            fromTo.set(
                                MethodsUtil.setDateFormatToDisplay(response.data.start_date) + " to " + MethodsUtil.setDateFormatToDisplay(
                                    response.data.end_date
                                )
                            )
                            range.set(
                                MethodsUtil.getFutureDatesTime(
                                    response.data.start_date,
                                    response.data.end_date
                                )
                            )
                            model.address = response.data.address
                            model.start_date = response.data.start_date
                            model.end_date = response.data.end_date
                            model.place_id = response.data.place_id
                            model.id = response.data.id
                            model.lat = response.data.lat
                            model.lng = response.data.lng

                            var followers=response.data.followers
                            followerList.clear()
                            followerList?.addAll(followers)
                            if (followers!=null){
                               var  size=followers.size
                                Log.e("FolowerList", "==>>+$size")
                                if (size==1){
                                    firstImage.set(true)
                                    first.set(followers[0].user_image)
                                }else if (size==2){
                                    firstImage.set(true)
                                    secondImage.set(true)
                                    first.set(followers[0].user_image)
                                    second.set(followers[1].user_image)
                                }else if (size>=3){
                                    firstImage.set(true)
                                    secondImage.set(true)
                                    thirdImage.set(true)
                                    first.set(followers[0].user_image)
                                    second.set(followers[1].user_image)
                                    third.set(followers[2].user_image)
                                }else{
                                    firstImage.set(false)
                                    secondImage.set(false)
                                    thirdImage.set(false)
                                }
                            }
                            var dateList=response.data.dates

                            travelScheduleAdapter.addDataInList(dateList)

                            viewPagerList=response.data.images
                            if (viewPagerList?.size!!>0){
                                isImage.set(false)
                            }
                            viewPagerAdapter.addList(viewPagerList!!)
                         //   travelDetails.travelPlansBinding.TabLayout.setViewPager(travelDetails.travelPlansBinding.viewPagerSlider)
                        }
                    }

                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }


    fun openCamera() {
        ImagePicker.with(travelDetails)
            .compress(1024)
            .crop()
            .maxResultSize(400, 400)
            .start()
    }

    fun signcWithGmailAlert(){

        val builder = AlertDialog.Builder(context)

        val customLayout: View = LayoutInflater.from(context)
            .inflate(
                com.letsdetour.R.layout.signc_with_gmail,
                null
            )
        builder.setView(customLayout)

        var dialog=builder.create()
        dialog.getWindow()?.setBackgroundDrawableResource(android.R.color.transparent);
        val close = customLayout.findViewById(com.letsdetour.R.id.ivClose1) as ImageView

        val tvForwardEmail = customLayout.findViewById(com.letsdetour.R.id.tvForwardEmail) as TextView
        val tvSigncGmail = customLayout.findViewById(com.letsdetour.R.id.tvSigncGmail) as TextView

        close.setOnClickListener {
            dialog.dismiss()
        }
        tvForwardEmail.setOnClickListener {
            dialog.dismiss()
            MethodsUtil.loadFragment(context, ForwardEmail())
        }
        tvSigncGmail.setOnClickListener {
            dialog.dismiss()

        }
        dialog.show()
    }



    fun onClickBack() {
        (context as Activity).onBackPressed()
    }

    fun onClickOverView(){
        isOverView.set(true)
        isITinary.set(false)
    }

    fun onClickItinerary(){
        isITinary.set(true)
        isOverView.set(false)
        travelScheduleAdapter.notifyDataSetChanged()

    }

    override fun fetchTravelDetils() {
        planDetails(Travelid.get().toString(),false)

    }
}