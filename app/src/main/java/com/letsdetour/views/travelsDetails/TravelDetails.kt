package com.letsdetour.views.travelsDetails

import android.Manifest
import android.R
import android.app.Activity
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.ContentResolver
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.letsdetour.databinding.TravelDetailsBinding
import com.letsdetour.views.mainActivity.MainActivity
import com.wafflecopter.multicontactpicker.ContactResult
import com.wafflecopter.multicontactpicker.LimitColumn
import com.wafflecopter.multicontactpicker.MultiContactPicker
import androidx.activity.result.ActivityResultLauncher
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import com.github.dhaval2404.imagepicker.ImagePicker
import com.letsdetour.utils.showToast
import kotlinx.android.synthetic.main.activity_sign_up.*
import java.io.File


class TravelDetails:Fragment() {

    var activityResultLauncher: ActivityResultLauncher<*>? = null

    private val results: ArrayList<ContactResult> = ArrayList()

    companion object {
        val PERMISSIONS_REQUEST_READ_CONTACTS = 100
    }
    
    var CONTACT_PICKER_REQUEST=128
    lateinit var travelDetailsVM: TravelDetailsVM
    lateinit var travelPlansBinding: TravelDetailsBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        travelPlansBinding= TravelDetailsBinding.inflate(LayoutInflater.from(context))
        return travelPlansBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        travelDetailsVM= TravelDetailsVM(requireContext(),this)
        travelPlansBinding.travelDetailsVM=travelDetailsVM
        MainActivity.mainBinding.btmNav.visibility=View.GONE
        MainActivity.mainBinding.ivAddHome.visibility=View.GONE


            /*val builder = MaterialDatePicker.Builder.dateRangePicker()
            val picker = builder.build()
            picker.show(childFragmentManager, picker.toString())

            picker.addOnCancelListener {
                Log.d("DatePicker Activity", "Dialog was cancelled")
            }

            picker.addOnPositiveButtonClickListener {
                Log.d("DatePicker Activity", "Date String = ${picker.headerText}::  Date epoch values::${it.first}:: to :: ${it.second}")
            }
*/

    }

    @RequiresApi(Build.VERSION_CODES.M)
     fun checkPermission() {

        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_CONTACTS) !=
            PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_CONTACTS

                ), 258
            )
        }else{

            openContactPicker()

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {

        when (requestCode) {
            258 -> {
                var camera = grantResults[0] == PackageManager.PERMISSION_GRANTED

                if (grantResults.isNotEmpty() &&  camera) {
                    openContactPicker()
                    //OpenImageData()
                } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_CONTACTS
                    )
                ) {
                    rationale()
                } else {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.READ_CONTACTS,
                        ),
                        258
                    )
                }
            }
           PERMISSIONS_REQUEST_READ_CONTACTS ->{
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadContacts()
                } else {
                    //  toast("Permission must be granted in order to display contacts information")
                }
            }
            121 -> {
                var read = grantResults[0] == PackageManager.PERMISSION_GRANTED
                var write = grantResults[1] == PackageManager.PERMISSION_GRANTED
                if (grantResults.isNotEmpty() && read && write) {
                    travelDetailsVM.openCamera()
                } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) && !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ) {
                    rationale()
                } else {
                    ActivityCompat.requestPermissions(
                        context as Activity, arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 121
                    )
                }
            }
        }

    }

    private fun loadContacts() {
        var builder = StringBuilder()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(requireContext(),
                Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS),
                PERMISSIONS_REQUEST_READ_CONTACTS)
            //callback onRequestPermissionsResult
        } else {
            builder = getContacts()
            Log.e("GetContactBuider", "===>>$builder")
        }
    }

    private fun getContacts(): StringBuilder {
        val builder = StringBuilder()
        val resolver: ContentResolver = context?.contentResolver!!
        val cursor = resolver.query(
            ContactsContract.Contacts.CONTENT_URI, null, null, null,
            null)

        if (cursor?.count!! > 0) {
            while (cursor.moveToNext()) {
                val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val phoneNumber = (cursor.getString(
                    cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))).toInt()

                if (phoneNumber > 0) {
                    val cursorPhone = context?.contentResolver!!.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", arrayOf(id), null)

                    if(cursorPhone?.count!! > 0) {
                        while (cursorPhone.moveToNext()) {
                            val phoneNumValue = cursorPhone.getString(
                                cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            builder.append("Contact: ").append(name).append(", Phone Number: ").append(
                                phoneNumValue).append("\n\n")
                            Log.e("Name ===>",phoneNumValue);
                        }
                    }
                    cursorPhone?.close()
                }
            }
        } else {
            //   toast("No contacts available!")
        }
        cursor.close()
        return builder
    }

    private fun rationale() {
        val builder: AlertDialog.Builder =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AlertDialog.Builder(
                    requireContext(),
                    android.R.style.Theme_Material_Light_Dialog_Alert
                )
            } else {
                AlertDialog.Builder(requireContext())
            }
        builder.setTitle("Mandatory Permissions")
            .setMessage("Manually allow permissions in App settings")
            .setPositiveButton("Proceed") { dialog, which ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", (context as Activity).packageName, null)
                intent.data = uri
                startActivityForResult(intent, 1)
            }
            .setCancelable(false)
            .show()
    }


    fun openContactPicker(){
            MultiContactPicker.Builder(requireActivity()) //Activity/fragment context
                .theme(com.letsdetour.R.style.MyCustomToolbarTheme) //Optional - default: MultiContactPicker.Azure
                .hideScrollbar(false) //Optional - default: false
                .showTrack(true) //Optional - default: true
                .searchIconColor(Color.WHITE) //Option - default: White
                .setChoiceMode(MultiContactPicker.CHOICE_MODE_MULTIPLE) //Optional - default: CHOICE_MODE_MULTIPLE
                .handleColor(
                    ContextCompat.getColor(
                        requireContext(),
                        com.letsdetour.R.color.button_color
                    )
                ) //Optional - default: Azure Blue
                .bubbleColor(
                    ContextCompat.getColor(
                        requireContext(),
                        com.letsdetour.R.color.button_color
                    )
                ) //Optional - default: Azure Blue
                .bubbleTextColor(Color.WHITE) //Optional - default: White
                .setTitleText("Select Contacts") //Optional - default: Select Contacts
                .setSelectedContacts(
                    "10",
                    "5"
                ) //Optional - will pre-select contacts of your choice. String... or List<ContactResult>
                .setLoadingType(MultiContactPicker.LOAD_ASYNC) //Optional - default LOAD_ASYNC (wait till all loaded vs stream results)
                .limitToColumn(LimitColumn.NONE) //Optional - default NONE (Include phone + email, limiting to one can improve loading time)
                .setActivityAnimations(
                    R.anim.fade_in, R.anim.fade_out,
                    R.anim.fade_in,
                    R.anim.fade_out
                ) //Optional - default: No animation overrides
        .showPickerForResult(128)
        }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.e("requestCode", "==$requestCode")
        Log.e("resultCode", "==$resultCode")
        if (resultCode == Activity.RESULT_OK && requestCode == 128) {
            results.addAll(MultiContactPicker.obtainResult(data));
            if(results.size > 0) {
                Log.d("MyContactName", results.get(0).getDisplayName());
            }
            Log.d("MyContactList", results.toString())
        }
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            Log.e("data", "==$fileUri")

            //  civSignUp.setImageURI(fileUri)
            travelDetailsVM.isImageSet.set(true)
            //You can also get File Path from intent
            val filePath = ImagePicker.getFilePath(data)!!
            travelDetailsVM.imagesList.add(filePath)
            travelDetailsVM.updateImageApi()

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            showToast(requireContext(), ImagePicker.getError(data))
        }
        else if (resultCode == RESULT_CANCELED) {
            println("User closed the picker without selecting items.")
        }
    }


    }
