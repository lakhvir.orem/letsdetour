package com.letsdetour.views.travelsDetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.letsdetour.R
import com.letsdetour.response.MessageRes
import com.letsdetour.response.TravelPlanDetailsRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import com.letsdetour.views.previewImage.PreviewImageActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.places_adapter.view.*
import retrofit2.Response

class PlacesAdapter(val context: Context, var dateList: ArrayList<TravelPlanDetailsRes.Data.Date>):com.letsdetour.utils.BaseAdapter(R.layout.places_adapter) {

    var viewBinderHelper= ViewBinderHelper()
    var routeList=ArrayList<TravelPlanDetailsRes.Data.Date.Route>()

    companion object{
        var fetchTravelDetailsInterface:FetchTravelDetailsInterface?=null
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataObject = position
        viewBinderHelper.bind(holder.itemView.swipe, dataObject.toString())
        viewBinderHelper.setOpenOnlyOne(true)
        holder.itemView.apply {
            routeList.let {
                tvTime.text=MethodsUtil.getTime(routeList[position].route_time)
                tvPlaceName.text=routeList[position].name
                var image=routeList[position].image
                if(!image.isNullOrEmpty())
                Picasso.get().load(image).placeholder(R.drawable.ic_default_upload_image).into(ivPlaces)
            }

            ivDelete.setOnClickListener {
                deleteRoute(routeList[position].id.toString(),position)
            }
        }

        holder.itemView.ivPlaces.setOnClickListener {
            context.startActivity(Intent(context, PreviewImageActivity::class.java).putExtra(CommonKeys.IMAGE,routeList[position].image))
        }
    }

    override fun getItemCount(): Int {
        return routeList.size
    }

    fun addDataInList(routList: List<TravelPlanDetailsRes.Data.Date.Route>) {
        if (!routList.isNullOrEmpty()){
            routeList.clear()
            routeList.addAll(routList)
            notifyDataSetChanged()
        }
    }

    fun restoreStates(inState: Bundle?) {
        viewBinderHelper.restoreStates(inState)
    }

    fun deleteRoute(routeid:String,position: Int) {
        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token =
                    "Bearer " + PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.deleteRoute(routeid)
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            showToast(context,response.message)
                            routeList.removeAt(position)
                            viewBinderHelper.closeLayout(position.toString())
                            notifyDataSetChanged()
                            fetchTravelDetailsInterface!!.fetchTravelDetils()
                        } else {
                            showToast(context, res.message().toString())
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }

    }
}