package com.letsdetour.views.forwardEmail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.ForwardEmailBinding

class ForwardEmail:Fragment() {
    lateinit var forwardEmailVM: ForwardEmailVM
    lateinit var forwardEmailBinding: ForwardEmailBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        forwardEmailBinding= ForwardEmailBinding.inflate(LayoutInflater.from(context))
        return forwardEmailBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        forwardEmailVM= ForwardEmailVM(requireContext())
        forwardEmailBinding.forwardEmailVM=forwardEmailVM
    }
}