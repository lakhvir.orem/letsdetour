package com.letsdetour.views.forwardEmail

import android.app.Activity
import android.content.Context
import androidx.lifecycle.ViewModel

class ForwardEmailVM(val context: Context):ViewModel() {
    fun onClickBack() {
        (context as Activity).onBackPressed()
    }
}