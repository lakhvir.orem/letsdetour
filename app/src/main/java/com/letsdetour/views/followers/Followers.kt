package com.letsdetour.views.followers

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.letsdetour.R
import com.letsdetour.databinding.FollowersBinding
import com.letsdetour.views.followers.viewPagerAdapter.ViewPagerAdapter
import kotlinx.android.synthetic.main.followers.*

class Followers:Fragment() {
    lateinit var followersVM: FollowersVM
    lateinit var followersBinding: FollowersBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        followersBinding= FollowersBinding.inflate(LayoutInflater.from(context))
        return followersBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        followersVM= FollowersVM(requireContext(),this)
        followersBinding.followersVM=followersVM
        setViewPager()
    }

    fun setViewPager(){

        tabLayout!!.addTab(tabLayout!!.newTab().setText(requireContext().getString(R.string.followers )))
        tabLayout!!.addTab(tabLayout!!.newTab().setText(requireContext().getString(R.string.follings)))
        val typeface = Typeface.createFromAsset(requireContext().assets, "Barlow-Regular.ttf")
        tabLayout!!.applyFont(typeface)
        taskViewPager.adapter= ViewPagerAdapter(childFragmentManager,tabLayout!!.tabCount)
        taskViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {


            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {

                taskViewPager!!.currentItem = tab!!.position
            }
        })
    }

    fun TabLayout.applyFont(typeface: Typeface) {
        val viewGroup = getChildAt(0) as ViewGroup
        val tabsCount = viewGroup.childCount
        for (j in 0 until tabsCount) {
            val viewGroupChildAt = viewGroup.getChildAt(j) as ViewGroup
            val tabChildCount = viewGroupChildAt.childCount
            for (i in 0 until tabChildCount) {
                val tabViewChild = viewGroupChildAt.getChildAt(i)
                if (tabViewChild is TextView) {
                    tabViewChild.typeface = typeface
                }
            }
        }
    }
}