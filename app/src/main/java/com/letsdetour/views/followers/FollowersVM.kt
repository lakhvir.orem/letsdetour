package com.letsdetour.views.followers

import android.app.Activity
import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.letsdetour.utils.CommonKeys

class FollowersVM(val context: Context, val followers: Followers):ViewModel() {
    var followerVal= ObservableField("")
    var followingVal= ObservableField("")
    init {

        getBundleData()
    }

    fun getBundleData(){
        if (followers.arguments!=null){
            var data=followers.arguments
            if (data?.containsKey(CommonKeys.FOLLOWINGS)!!){
                followingVal.set(data.getString(CommonKeys.FOLLOWINGS)+" FOLLOWINGS")
                followerVal.set(data.getString(CommonKeys.FOLLOWERS)+" FOLLOWERS")
            }
        }
    }


    fun onClickBack(){
        (context as Activity).onBackPressed()
    }
}