package com.letsdetour.views.followers.followersInner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.FollowersInnerBinding
import com.letsdetour.databinding.FollowingInnerBinding

class FollowersInner:Fragment() {
    lateinit var followersInnerVM: FollowersInnerVM
    lateinit var followersInnerBinding: FollowersInnerBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        followersInnerBinding= FollowersInnerBinding.inflate(LayoutInflater.from(context))
        return followersInnerBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        followersInnerVM= FollowersInnerVM(requireContext())
        followersInnerBinding.followersInnerVM=followersInnerVM
    }
}