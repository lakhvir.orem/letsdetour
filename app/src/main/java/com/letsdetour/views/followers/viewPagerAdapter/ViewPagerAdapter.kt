package com.letsdetour.views.followers.viewPagerAdapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.letsdetour.views.aboutUs.AboutUs
import com.letsdetour.views.account_details.AccountDetials
import com.letsdetour.views.followers.followersInner.FollowersInner
import com.letsdetour.views.followers.followingsInner.FollowingsInner


class ViewPagerAdapter(fragmentManager: FragmentManager, var tabCount: Int) : FragmentPagerAdapter(fragmentManager) {
    var  title:ArrayList<String>?=null

    override fun getItem(position: Int): Fragment {

        when (position) {
            0 -> {
                //  val homeFragment: HomeFragment = HomeFragment()
                return FollowersInner()
            }
            1 -> {
                return FollowingsInner()
            }
            else ->{

                return FollowersInner()
            }
        }
    }

    override fun getCount(): Int {
        return tabCount
    }


}