package com.letsdetour.views.followers.followingsInner

import android.content.Context
import android.util.Log
import com.bumptech.glide.Glide
import com.letsdetour.R
import com.letsdetour.response.FollowerListRes
import com.letsdetour.response.MessageRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import kotlinx.android.synthetic.main.followers_adapter.view.*
import kotlinx.android.synthetic.main.followers_adapter.view.civFolow
import kotlinx.android.synthetic.main.followings_adapter.view.*
import retrofit2.Response

class FollowingsInnerAdapter(val context: Context):com.letsdetour.utils.BaseAdapter(R.layout.followings_adapter) {
    var followingList=ArrayList<FollowerListRes.Data>()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.apply {
            followingList.let {
                tvFollowing.text=followingList[position].user_name
                Glide.with(context).load(followingList[position].user_image).error(R.drawable.user_avatar).into(civFolowing)
            }

            btUnfollow.setOnClickListener {
             //   unfollowUser(followingList[position].follower_id.toString(),position)
            }
        }
    }

    override fun getItemCount(): Int {
        return followingList.size

    }

    fun addDataInlist(list: List<FollowerListRes.Data>) {
        if (!list.isNullOrEmpty()){
            followingList.clear()
            followingList.addAll(list)
            notifyDataSetChanged()
        }
    }

    fun unfollowUser(folloerId: String?, position: Int) {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token="Bearer "+ PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.addFollow("0",folloerId.toString())
                    }
                    override fun onResponse(res: Response<MessageRes>) {
                        val response = res.body()
                        if (res.isSuccessful && res.code()==200) {
                            Log.e("RemoveFollowersRes", "==>>$response")
                            showToast(context,response?.message.toString())
                            if (followingList.size>0){
                                followingList.removeAt(position)
                                notifyDataSetChanged()
                            }
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }
}