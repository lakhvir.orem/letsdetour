package com.letsdetour.views.followers.followersInner

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import com.letsdetour.R
import com.letsdetour.response.FollowerListRes
import com.letsdetour.response.MessageRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import kotlinx.android.synthetic.main.followers_adapter.view.*
import retrofit2.Response

class FollowerInnerAdapter(val context: Context):com.letsdetour.utils.BaseAdapter(R.layout.followers_adapter) {
    var followerList=ArrayList<FollowerListRes.Data>()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.apply {
            followerList.let {
                tvUserNameFolow.text=followerList[position].user_name
                Glide.with(context).load(followerList[position].user_image).error(R.drawable.user_avatar).into(civFolow)
            }
            btRemoveUser.setOnClickListener {
                removeFollowers(followerList[position].user_id.toString(),position)
            }
        }
    }

    override fun getItemCount(): Int {
        return followerList.size
    }

    fun addDataInlist(list: List<FollowerListRes.Data>) {
        if (!list.isNullOrEmpty()){
            followerList.clear()
            followerList.addAll(list)
            notifyDataSetChanged()
        }
    }

    fun removeFollowers(folloerId: String?, position: Int) {

        if (MethodsUtil.isNetworkAvailable(context)) {
            try {
                var token="Bearer "+ PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()
                RetrofitCall.callService(context, true, token, object :
                    RequestProcess<Response<MessageRes>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<MessageRes> {
                        return retrofitApi.removefollowers(folloerId.toString())
                    }

                    override fun onResponse(res: Response<MessageRes>) {
                        val response = res.body()
                        if (res.isSuccessful && res.code()==200) {
                            Log.e("RemoveFollowersRes", "==>>$response")
                            showToast(context,response?.message.toString())
                            followerList.removeAt(position)
                            notifyDataSetChanged()
                        }
                    }
                    override fun onException(message: String) {}
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            CommonAlerts.alert(context, context.getString(R.string.internet_issue))
        }
    }
}