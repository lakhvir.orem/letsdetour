package com.letsdetour.views.followers.followingsInner

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.FollowingInnerBinding

class FollowingsInner:Fragment() {
    lateinit var followingInnerBinding: FollowingInnerBinding
    lateinit var followingsInnerVM: FollowingsInnerVM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        followingInnerBinding= FollowingInnerBinding.inflate(LayoutInflater.from(context))
        return followingInnerBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        followingsInnerVM= FollowingsInnerVM(requireContext())
        followingInnerBinding.followingsInnerVM=followingsInnerVM
    }
}