package com.letsdetour.views.followers.followersInner

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.letsdetour.R
import com.letsdetour.response.FollowerListRes
import com.letsdetour.utils.*
import com.letsdetour.utils.retrofit.RequestProcess
import com.letsdetour.utils.retrofit.RetrofitApi
import com.letsdetour.utils.retrofit.RetrofitCall
import retrofit2.Response

class FollowersInnerVM(val context: Context):ViewModel() {
    var followerInnerAdapter=FollowerInnerAdapter(context)

    init {
        getFollowers()
    }
    fun getFollowers() {
            if (MethodsUtil.isNetworkAvailable(context)) {
                try {
                    var token="Bearer "+ PrefferenceFile.retrieveKey(context, CommonKeys.TOKEN).toString()
                    RetrofitCall.callService(context, true, token, object :
                        RequestProcess<Response<FollowerListRes>> {
                        override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<FollowerListRes> {
                            return retrofitApi.follower_list("1")
                        }
                        override fun onResponse(res: Response<FollowerListRes>) {
                            if (res.isSuccessful ) {
                                val response = res.body()!!
                             var list=response.data
                                Log.e("FollowerList", "===>>>$list")
                                if (list!=null){
                                    followerInnerAdapter.addDataInlist(list)
                                }
                            }
                            else {
                                showToast(context,res.message().toString())
                            }
                        }
                        override fun onException(message: String) {}
                    })
                } catch (e: Exception) {
                    e.printStackTrace()
                }


            } else {
                CommonAlerts.alert(context, context.getString(R.string.internet_issue))
            }

    }
}