package com.letsdetour.views.chatList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.letsdetour.databinding.ChatBinding
import com.letsdetour.views.mainActivity.MainActivity

class ChatList:Fragment() {
    lateinit var chatListVM: ChatListVM
    lateinit var chatBinding: ChatBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        chatBinding= ChatBinding.inflate(LayoutInflater.from(context))
        return chatBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        chatListVM= ChatListVM(requireContext())
        chatBinding.chatVM=chatListVM
        MainActivity.mainBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }
}