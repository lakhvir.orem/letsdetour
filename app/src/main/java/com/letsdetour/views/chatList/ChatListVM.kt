package com.letsdetour.views.chatList

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.letsdetour.views.archieveList.ArchiveList

class ChatListVM(val context: Context):ViewModel() {
var chatAdapter =ChatListAdapter(context)

    fun onClickArchiveList(){
        context.startActivity(Intent(context, ArchiveList::class.java))

    }
}