package com.letsdetour.views.chatList

import android.content.Context
import android.content.Intent
import com.letsdetour.R
import com.letsdetour.utils.BaseAdapter
import com.letsdetour.utils.MethodsUtil
import com.letsdetour.views.chat.Chat

class ChatListAdapter(val context: Context):BaseAdapter(R.layout.chat_adapter) {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            context.startActivity(Intent(context,Chat::class.java))
        }
    }

    override fun getItemCount(): Int {
        return 2
    }
}