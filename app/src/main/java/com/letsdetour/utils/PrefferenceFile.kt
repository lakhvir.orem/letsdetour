package com.letsdetour.utils

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.letsdetour.views.login.Login


object PrefferenceFile {
    internal lateinit var sharedPreferences: SharedPreferences
    internal lateinit var editor: SharedPreferences.Editor
    val SHARED_PREFERENCE_KEY = "Letsdetour"
    val FCM_PREFERENCE_KEY = "Letsdetour_fcm"

    const val foreverSharedPref = "forever"
    const val rememberSharedPref = "remember"


    fun storeForverKey(context: Context, key: String, value: String) {
        sharedPreferences =
            context.getSharedPreferences(foreverSharedPref, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.commit()

    }

    fun storeRememberKey(context: Context, key: String, value: String) {
        sharedPreferences =
            context.getSharedPreferences(rememberSharedPref, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.commit()

    }




    fun retriveRememberKey(context: Context, key: String): String? {
        sharedPreferences =
            context.getSharedPreferences(rememberSharedPref, Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, null)
    }

    fun retrieveForeverKey(context: Context, key: String): String? {
        sharedPreferences =
            context.getSharedPreferences(foreverSharedPref, Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, null)
    }

    fun storeKey(context: Context, key: String, value: String) {
        sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.commit()

    }

    fun removeRemember(context: Context) {
        sharedPreferences =
            context.getSharedPreferences(rememberSharedPref, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.clear()
        editor.commit()
    }

    fun EditstoreKey(context: Context, key: String, value: String) {
        sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.commit()

    }

    fun storeIntKey(context: Context, key: String, value: Int) {
        sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putInt(key, value)
        editor.commit()

    }

    fun storeBooleanKey(context: Context, key: String, value: Boolean) {
        sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putBoolean(key, value)
        editor.commit()

    }

    fun retrieveBooleanKey(context: Context, key: String): Boolean? {
        sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean(key, false)
    }

    fun retrieveKey(context: Context, key: String): String? {
        sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, "")
    }


    fun EditretrieveKey(context: Context, key: String): String? {
        sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, "")
    }

    fun removeAll(context: Context) {
        sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.clear()
        editor.commit()
    }

    fun removekey(context: Context, key: String) {
        sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.remove(key)
        editor.commit()
    }

//    fun storeAndParseJsonData(context: Context, jsonObject: JSONObject) {
//        sharedPreferences =
//            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
//        editor = sharedPreferences.edit()
//        try {
//            if (!jsonObject.isNull("result")) {
//                val objResult: JSONObject = jsonObject.getJSONObject("result")
//                editor.putString(CommonKeys.USER_ID, objResult.getString(CommonKeys.USER_ID))
//                editor.putString(CommonKeys.FIRST_NAME, objResult.getString(CommonKeys.FIRST_NAME))
//                editor.putString(CommonKeys.LAST_NAME, objResult.getString(CommonKeys.LAST_NAME))
//                editor.putString(CommonKeys.EMAIL, objResult.getString(CommonKeys.EMAIL))
//                editor.putString(
//                    CommonKeys.MOBILE_NUMBER,
//                    objResult.getString(CommonKeys.MOBILE_NUMBER)
//                )
//                editor.putString(CommonKeys.DOB, objResult.getString(CommonKeys.DOB))
//                editor.putString(CommonKeys.GENDER, objResult.getString(CommonKeys.GENDER))
//                editor.putString(
//                    CommonKeys.COUNTRY_CODE,
//                    objResult.getString(CommonKeys.COUNTRY_CODE)
//                )
//                editor.putString(CommonKeys.TIMEZONE, objResult.getString(CommonKeys.TIMEZONE))
//
//                if(objResult.has("is_online"))
//                {
//                    editor.putString(CommonKeys.USER_STATUS, objResult.getString("is_online"))
//                }
//
//
//
//                var cityId = ""
//
//                if (!objResult.isNull("city")) {
//                    cityId = objResult.getString("city")
//                }
//                editor.putString(CommonKeys.CITY_ID, cityId)
//
//
//                if (!objResult.isNull("profile_image")) {
//                    editor.putString(
//                        CommonKeys.USER_IMAGE,
//                        objResult.getString(CommonKeys.USER_IMAGE)
//                    )
//
//                } else {
//                    editor.putString(CommonKeys.USER_IMAGE, "")
//
//                }
//
//                if (!objResult.isNull(CommonKeys.HOME_TOWN)) {
//                    editor.putString(
//                        CommonKeys.HOME_TOWN,
//                        objResult.getString(CommonKeys.HOME_TOWN)
//                    )
//                } else {
//                    editor.putString(CommonKeys.HOME_TOWN, "")
//                }
//
//                if (!objResult.isNull(CommonKeys.CITY)) {
//                    editor.putString(CommonKeys.CITY, objResult.getString(CommonKeys.HOME_TOWN))
//                } else {
//                    editor.putString(CommonKeys.CITY, "")
//
//                }
//
//
//                var schoolId = ""
//                var collegeId = ""
//
//                if (!objResult.isNull("school_name")) {
//                    schoolId = objResult.getString("school_name")
//                }
//                if (!objResult.isNull("college_name")) {
//                    collegeId = objResult.getString("college_name")
//                }
//                editor.putString(CommonKeys.SCHOOL_ID, schoolId)
//                editor.putString(CommonKeys.COLLEGE_ID, collegeId)
//
//
//
//                if(!objResult.isNull("locate_me"))
//                {
//                    editor.putString(CommonKeys.LOCATE_ME,objResult.getString("locate_me"))
//                }
//                else
//                {
//                    editor.putString(CommonKeys.LOCATE_ME,"no")
//
//                }
//
//
//                editor.commit()
//
//            }
//
//        } catch (e: JSONException) {
//            e.printStackTrace()
//        }
//
//    }


    /*To Store Fcm Device ID*/
    fun storeFcmDeviceId(
        context: Context,
        token: String?
    ) {
        sharedPreferences = context.getSharedPreferences(FCM_PREFERENCE_KEY, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putString(CommonKeys.TOKEN, token)
        editor.commit()
    }

    fun retrieveFcmDeviceId(context: Context): String? {
        sharedPreferences = context.getSharedPreferences(FCM_PREFERENCE_KEY, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        return sharedPreferences.getString(CommonKeys.TOKEN, null)
    }

    fun logout(context: Context) {
        sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.clear()
        editor.commit()
        context.startActivity(
            Intent(
                context,
                Login::class.java
            ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        )
    }
}