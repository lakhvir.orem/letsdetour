package com.letsdetour.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter(val layout:Int):RecyclerView.Adapter<BaseAdapter.ViewHolder>() {

    inner class ViewHolder(var view:View):RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view=LayoutInflater.from(parent.context).inflate(layout,parent,false)
        return ViewHolder(view)
    }

}