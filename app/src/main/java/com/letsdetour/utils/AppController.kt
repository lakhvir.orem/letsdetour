package com.letsdetour.utils

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate


class AppController : Application() {
    val preference by lazy { SharedPreferenceUtils(this) }

    companion object{
        lateinit var application: AppController
        lateinit var context: Context
        @JvmStatic
        fun getApp() = application
    }

    override fun onCreate() {

        super.onCreate()

        context=applicationContext
        application =this
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        Log.e("setFonts", "True")
        FontOverrideClass.setDefaultFont(this, "SERIF", "Barlow-Regular.ttf")
    }








    /*  override fun attachBaseContext(base: Context) {

          super.attachBaseContext(LocaleHelper.getLanguage(base)?.let {
              LocaleHelper.onAttach(
                  base,
                  it
              )
          })
      }*/

}

fun Any.getPref(): SharedPreferenceUtils {
    return AppController.getApp().preference
}