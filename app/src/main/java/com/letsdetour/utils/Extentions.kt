package com.letsdetour.utils

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.net.Uri
import android.provider.Settings
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.HttpMethod
import com.facebook.login.LoginManager
import com.letsdetour.R
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}


fun showToast(context: Context,message:String){
    Toast.makeText(context,message, Toast.LENGTH_SHORT).show()
}

fun Uri?.getFilePath(context: Context): String {
    return this?.let { uri -> RealPathUtil.getRealPath(context, uri) ?: "" } ?: ""
}



@SuppressLint("NewApi")
fun getRandom(): String {
    var date = Date()
    var format = android.icu.text.SimpleDateFormat("yyMMddhhmmssMs")
    return format.format(date)
}

/**
 *return base64 String
 */
fun String.toBase64String(): String {
    val data=this.toByteArray()
    return Base64.encodeToString(data, Base64.DEFAULT)
}



/**
 * @return Boolean value
 */
fun String.isValidEmail(): Boolean {
    val pattern: Pattern
    val matcher: Matcher

    /* val EMAIL_PATTERN2 =
         "^[_A-Za-z0-9-]+||.+[_A-Za-z0-9-]*@[_A-Za-z0-9-]+\\.(com|net|org|in|co.in)$"*/
    pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    matcher = pattern.matcher(this)
    return matcher.matches()
}


/**
 * @param str2
 * @return boolean value
 */
fun String.isMatches(str2: String?): Boolean {
    return TextUtils.equals(this, str2)
}


/**
 * @return unique device id
 */
fun getDeviceId():String{
    return Settings.Secure.getString(
        AppController.application?.applicationContext?.contentResolver,
        Settings.Secure.ANDROID_ID
    )
}

fun diconnectFromFb() {

    if (AccessToken.getCurrentAccessToken() == null) {
        Log.e("ifffffffffff","AccessTokenNull")
        return
    }else{
        Log.e("ifffffffffff","AccessTokenNotttNull")
        var graphRequest = GraphRequest(
            AccessToken.getCurrentAccessToken(),
            "/me/permissions/",
            null,
            HttpMethod.DELETE,
            GraphRequest.Callback { LoginManager.getInstance().logOut() })

        graphRequest.executeAsync()
    }

}



private var progressDialog: AlertDialog? = null
fun initProgress(context:Context){
    try {
        val customAlertBuilder = AlertDialog.Builder(context)
        val customAlertView =
            LayoutInflater.from(context).inflate(R.layout.dialog_progress, null)
        customAlertBuilder.setView(customAlertView)
        customAlertBuilder.setCancelable(false)
        progressDialog = customAlertBuilder.create()
        progressDialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        progressDialog!!.show()
    }catch (e:java.lang.Exception){
        e.printStackTrace()
    }

}

fun dismissProgress(){
  if(progressDialog!=null){
        if (progressDialog!!.isShowing){
        try {
            progressDialog!!.cancel()

        }catch (e:Exception){
            e.printStackTrace()
        }
    }
  }
}


fun TextView.leftDrawable(@DrawableRes id: Int = 0) {
    this.setCompoundDrawablesWithIntrinsicBounds(id, 0, 0, 0)
}

fun TextView.topDrawable(@DrawableRes id: Int = 0) {
    this.setCompoundDrawablesWithIntrinsicBounds(0, id, 0, 0)
}

fun EditText.OnFocusChange(textView: TextView, hint:String){
    this.setOnFocusChangeListener { view, b ->
        if(b){
            textView.visibility= View.VISIBLE
            this.hint=""
            for (drawable in this.compoundDrawables) {
                if (drawable != null) {
                    drawable.colorFilter = PorterDuffColorFilter(
                        ContextCompat.getColor(
                            textView.context,
                            R.color.black
                        ), PorterDuff.Mode.DST_IN
                    )
                }
            }
        }
        else if(this.text.length==0){
            textView.visibility= View.GONE
            this.hint=hint
            for (drawable in this.compoundDrawables) {
                if (drawable != null) {
                    drawable.colorFilter = PorterDuffColorFilter(
                        ContextCompat.getColor(
                            textView.context,
                            R.color.grey
                        ), PorterDuff.Mode.DST_IN
                    )
                }
            }
        }
    }
}

fun EditText.onChange(cb: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            cb(s.toString())
        }
    })
}

fun View.click(block: () -> Unit) = setOnClickListener { block.invoke() }

fun EditText.observeTextChange(body:(String?)->Unit){
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {

        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            body.invoke(p0?.toString())
        }

    })
}


fun String.getMonthDate(): String {

    var format = SimpleDateFormat("yyyy-MM-dd")
    val newDate: Date = format.parse(this)

    format = SimpleDateFormat("MMM dd")
    val date: String = format.format(newDate)
    return date
}

fun String.getDateInformat3(): String {

    var format = SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy")
    val newDate: Date = format.parse(this)

    format = SimpleDateFormat("yyyy-MM-dd")
    val date: String = format.format(newDate)
    return date
}
fun String.getDateInformat2(): String {

    var format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    val newDate: Date = format.parse(this)

    format = SimpleDateFormat("yyyy-MM-dd")
    val date: String = format.format(newDate)
    return date
}

fun addPartBody(builder: MultipartBody.Builder, name: String, path: String?) {
    path?.let {
        val file = File(it)
        val body = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        builder.addFormDataPart(name, file.name, body)
    }
}
