package com.letsdetour.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.letsdetour.R
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

object MethodsUtil {

  /*  fun loadFragment(context: Context, fragment:Fragment) {
        (context as FragmentActivity).supportFragmentManager.beginTransaction()
            .replace(R.id.frameHome, fragment)
            .addToBackStack(null)
            .commitAllowingStateLoss()
   }*/

    fun removeFragment(context: Context, fragment: Fragment) {
        (context as FragmentActivity).supportFragmentManager.beginTransaction()
            .replace(R.id.frame, fragment)
            .commitAllowingStateLoss()
    }



    fun loadFragment( context:Context,fragment: Fragment) {
        (context as FragmentActivity).supportFragmentManager.beginTransaction().setCustomAnimations(
            R.anim.slide_in,
            0,
            R.anim.fade_in,0

        )
            .replace(R.id.frame, fragment)
            .addToBackStack(null)
            .commit()
    }

    fun addFragment(context: Context, fragment: Fragment) {
        (context as FragmentActivity).supportFragmentManager.beginTransaction().setCustomAnimations(
                R.anim.slide_in,
                0,
                R.anim.fade_in,0

        )
            .add(R.id.frame, fragment)
            .commitAllowingStateLoss()
    }


    fun showSnackBar(context: Context,view: View,message:String) {

        val snackbar= Snackbar.make(view, message, Snackbar.LENGTH_LONG)
            .setAction("OK") {
// Responds to click on the action

            }
       /* snackbar.setActionTextColor(ContextCompat.getColor(context as Activity,R.color.white))
        val view=snackbar.view
        view.setBackgroundColor(ContextCompat.getColor(context,R.color.red))*/
        snackbar.show()
    }


    fun isValidEmail(email: String?): Boolean {
        val EMAIL_PATTERN = ("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
        val pattern: Pattern = Pattern.compile(EMAIL_PATTERN)
        val matcher: Matcher = pattern.matcher(email)
        return matcher.matches()
    }


    fun hideSoftKeyboard(activity: Activity) {

        val inputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        Log.e("hideKeyboard", "inside")
    }

    var preHtml = "<html>\n" +
            "<head>\n" +
            "<style>\n" +
            "body {\n" +
            "    font-family: 'Merriweather';font-size: 14px; color: black\n" +
            "}\n" +
            "</style>\n" +
            "</head>\n" +
            "<body>\n"+
            "<p align=justify style=font-family:Merriweather>"


    var postHtml = "</p>\n" +
            "</body>\n" +
            "</html>"


    fun String.getDateInformat(): String {

        var format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss a")
        val newDate: Date = format.parse(this)

        format = SimpleDateFormat("dd-MMM-yyyy HH:mm:ss a")
        val date: String = format.format(newDate)
        return date
    }

    fun String.getDateInformat2(): String {

        var format = SimpleDateFormat("yyyy-MM-dd")
        val newDate: Date = format.parse(this)

        format = SimpleDateFormat("dd-MMM-yyyy")
        val date: String = format.format(newDate)
        return date
    }

    @SuppressLint("NewApi")
    fun getLastMessageTime(date: String): String {
        var result = "Just now"
        val dateFormat =
            android.icu.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
      /*  val dateFormat =
            android.icu.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        dateFormat.timeZone = android.icu.util.TimeZone.getTimeZone("UTC")*/
        try {
            var dateNewData = dateFormat.parse(date)
            var destDf = android.icu.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            destDf.timeZone = android.icu.util.TimeZone.getDefault()

            var dataa = destDf.format(dateNewData)
            val oldDate = destDf.parse(dataa)
            println("Old Date" + oldDate)
            val currentDate = Date()
            println("Old Date" + destDf.format(currentDate)+"")
            val diff = currentDate.time - oldDate.time
            val seconds = diff / 1000
            val minutes = seconds / 60
            val hours = minutes / 60
            val days = hours / 24

            if (oldDate.before(currentDate)) {
                if (days.toInt() > 0) {
                    if (days.toInt() == 1) {
                        result = "${days.toInt()} day"
                    } else {
                        result = "${days.toInt()} days"
                    }
                } else if (hours.toInt() > 0) {
                    if (hours.toInt() == 1) {
                        result = "${hours.toInt()} hour"
                    } else {
                        result = "${hours.toInt()} hours"
                    }
                } else if (minutes.toInt() > 0) {
                    result = "${minutes.toInt()} mins"
                }else{
                    result = "Just now"
                }
            }
        } catch (e: java.lang.Exception) {
            if (date.isEmpty()) {
                result = ""
            }else{
                result="Just now"
            }
            println("Old Date" + e.message)
        }
        return result
    }

    @SuppressLint("NewApi")
    fun getFutureDatesTime(startdate: String,endDate:String): String {
        var result = "travel plan is running"
        val dateFormat =
            android.icu.text.SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        dateFormat.timeZone = android.icu.util.TimeZone.getTimeZone("UTC")
        try {
            var dateNewData = dateFormat.parse(startdate)
            var dateOldData = dateFormat.parse(endDate)
            var destDf = android.icu.text.SimpleDateFormat("yyyy MM dd")
            destDf.timeZone = android.icu.util.TimeZone.getDefault()

            var dataa = destDf.format(dateNewData)
            var dataaOld = destDf.format(dateOldData)
            val futureDate = destDf.parse(dataa)
            val endD = destDf.parse(dataaOld)
            println("futureDate$futureDate")
            val currentDate = Date()
          //  println("Current Date" + destDf.format(currentDate)+"")
            println("Current Date$currentDate")
            val diff = futureDate?.time!!-currentDate.time
            val seconds = diff / 1000
            val minutes = seconds / 60
            val hours = minutes / 60
            val days = hours / 24
            Log.e("Diffrent", "==>>$diff")
            Log.e("Hours", "==>>$hours")
            Log.e("Days", "==>>$days")




            if (futureDate.after(currentDate) || futureDate.equals(currentDate)) {
                if (days.toInt() > 0) {
                    if (days.toInt() == 1) {
                        result = "In ${days.toInt()} day"
                    } else {
                        result = "In ${days.toInt()} days"
                    }
                } else if (hours.toInt() > 0) {
                    if (hours.toInt() == 1) {
                        result = "In ${hours.toInt()} hour"
                    } else {
                        result = "In ${hours.toInt()} hours"
                    }
                } else if (minutes.toInt() > 0) {
                    result = "In ${minutes.toInt()} mins"
                }else{
                    result = "travel plan is running"
                }
            }else if (endD.before(currentDate)){
                Log.e("EndDate", "==>>$endD")
                Log.e("CurreDate", "==>>$currentDate")
                result=" travel plan expired"

            }
        } catch (e: java.lang.Exception) {
            if (endDate.isEmpty()) {
                result = ""
            }else{
                result=" travel plan"
            }
            println("Old Date" + e.message)
        }
        return result
    }



    fun setDateFormatToDisplay(input: String): String {
        var outputDate = ""

        try {

            var simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
            var date = simpleDateFormat.parse(input)

            var simpleDateFormat1 = SimpleDateFormat("MMM dd")
            outputDate = simpleDateFormat1.format(date)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return outputDate
    }

    fun getTime(input: String): String {
        var outputDate = ""

        try {

            var simpleDateFormat = SimpleDateFormat("hh:mm")
            var date = simpleDateFormat.parse(input)

            var simpleDateFormat1 = SimpleDateFormat("hh:mm a")
            outputDate = simpleDateFormat1.format(date)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return outputDate
    }

    fun setDateDisplay(input: String): String {
        var outputDate = ""

        try {

            var simpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
            var date = simpleDateFormat.parse(input)

            var simpleDateFormat1 = SimpleDateFormat("yyyy-MM-dd")
            outputDate = simpleDateFormat1.format(date)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return outputDate
    }

    fun convertLocalTimeToUTC(input: String):String{

        var outputDate = ""

        try {

            var simpleDateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH)
            var date = simpleDateFormat.parse(input)

            var simpleDateFormat1 = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
            simpleDateFormat1.timeZone = TimeZone.getTimeZone("UTC")
            outputDate = simpleDateFormat1.format(date)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return outputDate
    }


    fun getMilliseconds(input: String): Long {

        var output: Long = 0

        try {

            var simpleDateFormat = SimpleDateFormat("yyyy/MM/dd")
            var date = simpleDateFormat.parse(input)
            output = date.time


        } catch (e: Exception) {
            e.printStackTrace()
        }

        return output
    }
    fun getMillisecondsFormat(input: String): Long {

        var output: Long = 0

        try {
            var simpleDateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
            var date = simpleDateFormat.parse(input)
            output = date.time


        } catch (e: Exception) {
            e.printStackTrace()
        }

        return output
    }

    fun getNameOfDay(year: Int, dayOfYear: Int): String? {
        val calendar = Calendar.getInstance()
        calendar[Calendar.YEAR] = year
        calendar[Calendar.DAY_OF_YEAR] = dayOfYear
        val days = arrayOf(
            "Sun",
            "Mon",
            "Tue",
            "Wed",
            "Thu",
            "Fri",
            "Sat"
        )
        val dayIndex = calendar[Calendar.DAY_OF_WEEK]
        return days[dayIndex - 1]
    }
    fun getMonthShortName(monthNumber: Int): String? {
        var monthName = ""
        if (monthNumber >= 0 && monthNumber < 12) try {
            val calendar = Calendar.getInstance()
            calendar[Calendar.MONTH] = monthNumber
            val simpleDateFormat = SimpleDateFormat("MMM")
            //simpleDateFormat.setCalendar(calendar);
            monthName = simpleDateFormat.format(calendar.time)
        } catch (e: java.lang.Exception) {
            e?.printStackTrace()
        }
        return monthName
    }

    fun getDateName(date:String):String{

        val inFormat = SimpleDateFormat("yyyy-MM-dd")
        val date: Date = inFormat.parse(date)
        val outFormat = SimpleDateFormat("EEE, MMM d")
        val goal: String = outFormat.format(date)

        return goal
    }

    fun getDateNamewithYear(date:String):String{

        val inFormat = SimpleDateFormat("yyyy-MM-dd")
        val date: Date = inFormat.parse(date)
        val outFormat = SimpleDateFormat("EEE, d MMM yyyy")
        val goal: String = outFormat.format(date)

        return goal
    }

    fun getTimeFromUTC(input: String): String{

        var output = ""
        try {

            val sdf =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
            sdf.timeZone = TimeZone.getTimeZone("UTC")
            val date = sdf.parse(input)
            sdf.timeZone = TimeZone.getDefault()
            output = sdf.format(date)

        }catch (e:Exception){

            e.printStackTrace()
        }

        return output
    }



    fun isNetworkAvailable(context: Context): Boolean {

        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val wm = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        // val ip = Formatter.formatIpAddress(wm.connectionInfo.ipAddress)

        val wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        if (wifi != null && wifi.isConnected) {
            return true
        }

        val mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        if (mobile != null && mobile.isConnected) {
            return true
        }

        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }


}