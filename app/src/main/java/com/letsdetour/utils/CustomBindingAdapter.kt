package com.letsdetour.utils

import android.widget.*
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.chaek.android.RatingBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.letsdetour.R
import com.letsdetour.views.travelsDetails.TravelViewPager
import com.letsdetour.views.walkThrough.WalkThroughAdapter
import de.hdodenhof.circleimageview.CircleImageView


class CustomBindingAdapter {

    companion object {

        @BindingAdapter(value = ["setRecyclerAdapter"], requireAll = false)
        @JvmStatic
        fun setRecyclerAdapter(
            recyclerView: RecyclerView,
            adapter: RecyclerView.Adapter<*>
        ) {
            if (adapter != null) {
                recyclerView.adapter = adapter
            }
        }

        @BindingAdapter(value = ["setViewPagerAdapter"], requireAll = false)
        @JvmStatic
        fun setViewPagerAdapter(viewPager: ViewPager, adapter: WalkThroughAdapter) {
            viewPager.adapter = adapter
        }

        @BindingAdapter(value = ["setTravelViewPager"], requireAll = false)
        @JvmStatic
        fun setTravelViewPager(viewPager: ViewPager, adapter: TravelViewPager) {
            viewPager.adapter = adapter
        }

        @BindingAdapter(value = ["setBottomNavAdapter"], requireAll = false)
        @JvmStatic
        fun setBottomNavAdapter(
            bottomNav: BottomNavigationView,
            position: ObservableField<Int>
        ) {

            bottomNav.menu.findItem(position.get()!!).isChecked = true
        }
        @BindingAdapter(value = ["navigationClick"], requireAll = false)
        @JvmStatic
        fun navigationClick(
            bottomNavigationView: BottomNavigationView,
            click: BottomNavigationView.OnNavigationItemSelectedListener
        ) {
            bottomNavigationView.setOnNavigationItemSelectedListener(click)
        }

        @BindingAdapter(value = ["changeImageDrawable"])
        @JvmStatic
        fun changeImageDrawable(
            imageView: ImageView,
            observableBoolean: ObservableBoolean
        ) {
            if (observableBoolean.get()) {
                imageView.setImageResource(R.drawable.ic_footer_add_status_selected)
            } else {
                imageView.setImageResource(R.drawable.ic_footer_add_status_normal)
            }
        }

        @BindingAdapter(value = ["changeTextBackground"])
        @JvmStatic
        fun changeTextBackground(
            textView: TextView,
            observableBoolean: ObservableBoolean
        ) {
            if (observableBoolean.get()) {
                textView.setBackgroundResource(R.drawable.button_ripple3)
            } else {
                textView.setBackgroundResource(R.color.white)
            }
        }

        @BindingAdapter(value = ["changeTextBackground2"])
        @JvmStatic
        fun changeTextBackground2(
            textView: TextView,
            observableBoolean: ObservableBoolean
        ) {
            if (observableBoolean.get()) {
                textView.setBackgroundResource(R.drawable.button_ripple)
            } else {
                textView.setBackgroundResource(R.color.editext_border)
            }
        }

        @BindingAdapter(value = ["changeCheckDrawable"])
        @JvmStatic
        fun changeCheckDrawable(
            textView: TextView,
            observableBoolean: ObservableBoolean
        ) {
            if (observableBoolean.get()) {
                textView.leftDrawable(R.drawable.ic_round_checkbox_selected)
            } else {
                textView.leftDrawable(R.drawable.ic_round_checkbox_normal)
            }
        }

        @BindingAdapter(value = ["ratingBarListner"])
        @JvmStatic
        fun ratingBarListner(
            ratingBar: RatingBar,
            listner: RatingBar.RatingBarListener
        ) {
            ratingBar.setRatingBarListener(listner)
        }

        @BindingAdapter(value = ["loadCircleImage"], requireAll = false)
        @JvmStatic
        fun loadCircleImage(imageView: CircleImageView, url: String?) {
            // Log.e("pathIs", url)
            if (url!=null) {
                Glide.with(imageView.context)
                    .asBitmap()
                    .apply(
                        RequestOptions().override(
                            200,
                            200
                        )
                    ).placeholder(R.drawable.user_avatar)
                    .load(url)
                    .into(imageView)
            }
        }

        @BindingAdapter(value = ["loadSimpleImage"], requireAll = false)
        @JvmStatic
        fun loadSimpleImage(imageView: ImageView, url: String?) {
            // Log.e("pathIs", url)
            if (url!=null) {
                Glide.with(imageView.context)
                    .asBitmap()
                    .placeholder(R.drawable.ic_default_upload_image)
                  /*  .apply(
                        RequestOptions().override(
                            800,
                            400
                        )
                    )*/
                    .load(url)
                    .into(imageView)
            }
        }
    }

}