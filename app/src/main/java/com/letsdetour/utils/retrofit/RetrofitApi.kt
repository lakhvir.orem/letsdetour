package com.letsdetour.utils.retrofit


import com.letsdetour.response.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface RetrofitApi {

    @Multipart
    @POST("register")
    suspend fun signUp(
        @Part("user_name") username: RequestBody?=null,
        @Part("email") email : RequestBody?=null,
        @Part("password") password: RequestBody?=null,
        @Part("phone") phone: RequestBody?=null,
        @Part("country") country: RequestBody?=null,
        @Part("country_code") country_code: RequestBody?=null,
        @Part("city") city: RequestBody?=null,
        @Part image: MultipartBody.Part
        ):Response<RegisterRes>


    @GET("country_list")
   suspend fun getCountryList(): Response<CountryListRes>

    @FormUrlEncoded
    @POST("state_list")
    suspend fun getStateList(
        @Field("state_id") state_id: Int
    ):Response<CityListRes>


    @FormUrlEncoded
    @POST("verifyOtp")
    suspend fun getVerifyOtp(
        @Field("otp") otp: Long,
        @Field("email") email: String,
        @Field("type") type: Int,
    ):Response<LoginRes>

    @FormUrlEncoded
    @POST("forgot_password")
    suspend fun forgotPassword(
        @Field("email") email: String
    ):Response<MessageRes>

    @FormUrlEncoded
    @POST("login")
    suspend fun login(
        @Field("email") email: String,
        @Field("password") password: String,
    ):Response<LoginRes>

    @FormUrlEncoded
    @POST("social_login")
    suspend fun socialLogin(
        @Field("social_id") social_id: String,
        @Field("social_type") social_type: String,
        @Field("email") email: String,
        @Field("device_type") device_type: String,
        @Field("device_token") device_token: String,
        @Field("country_code") country_code: String,
        @Field("phone") phone: String,
        @Field("user_name") user_name: String
    ):Response<CommonLoginRes>




    @POST("user/update_profile")
    suspend fun updateProfile(
        @Header ("Authorization") token:String,
        @Header ("Accept") accept:String,
        @Body requestBody: RequestBody
    ):Response<LoginRes>

    @GET("user/details")
    suspend fun getUserDetails(
        @Header ("Authorization") token:String,
        @Header ("Accept") accept:String
    ): Response<LoginRes>

    @GET("user/details")
    suspend fun getUserDetail(
        @Header ("Accept") accept:String="application/json"
    ): Response<UserDetailsRes>

    @FormUrlEncoded
    @POST("user/change_password")
    suspend fun changePassword(
        @Field("current_password") current_password: String,
        @Field("new_password") new_password: String,
        @Field("new_confirm_password") new_confirm_password: String,
        @Header ("Accept") accept:String="application/json"

    ):Response<MessageRes>

    @FormUrlEncoded
    @POST("user/homedata")
    suspend fun getHomeData(
        @Field("lat") lat: String,
        @Field("lng") password: String,
        @Field("type") type: String,
        @Header ("Accept") accept:String="application/json"
    ):Response<HomeDataRes>


    @FormUrlEncoded
    @POST("user/addLike")
    suspend fun addLike(
        @Field("status") status: String,
        @Field("feed_id") feed_id: String
    ):Response<MessageRes>

    @FormUrlEncoded
    @POST("user/feedDetail")
    suspend fun feedDetail(
        @Field("feed_id") feed_id: String
    ):Response<FeedDetailsRes>

    @FormUrlEncoded
    @POST("user/addComment")
    suspend fun addComment(
        @Header("Authorization") token: String,
        @Field("feed_id") feed_id: String,
        @Field("comment") comment: String,
        @Header ("Accept") accept:String="application/json"
    ):Response<FeedDetailsRes>


    @POST("user/createFeed")
    suspend fun createFeed(
        @Body requestBody: RequestBody,
        @Header ("Accept") accept:String="application/json",
        ):Response<MessageRes>


    @GET("place/nearbysearch/json?")
     fun getPlaceId(
        @Query("location") location: String,
        @Query("key") key: String,
        @Query("radius") radius: String="0.1"
        ): Call<NearBySearchRes>

    @FormUrlEncoded
    @POST("user/createPlan")
    suspend fun createPlan(
        @Field("address") address: String,
        @Field("lat") lat: String,
        @Field("lng") lng: String,
        @Field("start_date") start_date: String,
        @Field("end_date") end_date: String,
        @Field("place_id") place_id: String,
        @Field("permission") permission: String="1"
        ):Response<FeedDetailsRes>

    @GET("user/planList")
    suspend fun getPanList(): Response<GetPlanListRes>


    @FormUrlEncoded
    @POST("user/deletePlan")
    suspend fun deletePlan(
        @Field("plan_id") feed_id: String,
        @Header ("Accept") accept:String="application/json"
    ):Response<MessageRes>

    @POST("user/updateFeed")
    suspend fun updateFeed(
        @Body requestBody: RequestBody,
        @Header ("Accept") accept:String="application/json",
    ):Response<MessageRes>


    @FormUrlEncoded
    @POST("user/updatePlan")
    suspend fun updatePlan(
        @Field("plan_id") plan_id: String,
        @Field("address") address: String,
        @Field("lat") lat: String,
        @Field("lng") lng: String,
        @Field("start_date") start_date: String,
        @Field("end_date") end_date: String,
        @Field("place_id") place_id: String,
        @Field("permission") permission: String="1",
        @Header ("Accept") accept:String="application/json",
    ):Response<MessageRes>

    @FormUrlEncoded
    @POST("user/addFollow")
    suspend fun addFollow(
        @Field("status") status: String,
        @Field("follower_id") follower_id: String
    ):Response<MessageRes>

    @FormUrlEncoded
    @POST("user/planDetail")
    suspend fun travelDetails(
        @Field("plan_id") plan_id: String
    ):Response<TravelPlanDetailsRes>

    @FormUrlEncoded
    @POST("user/contactAdmin")
    suspend fun contactAdmin(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("message") message: String,
        @Header ("Accept") accept:String="application/json",

        ):Response<MessageRes>


    @GET("user/notification_list")
    suspend fun notification_list(): Response<NotificationRes>



    @FormUrlEncoded
    @POST("user/receiveFollowRequest")
    suspend fun acceptRejectRequest(
        @Field("request_id") plan_id: String,
        @Field("status") status: String
    ):Response<MessageRes>

    @FormUrlEncoded
    @POST("user/follower_list")
    suspend fun follower_list(
        @Field("type") type: String
    ):Response<FollowerListRes>

    @FormUrlEncoded
    @POST("user/addPlanFollower")
    suspend fun addPlanFollower(
        @Field("plan_id") plan_id: String,
        @Field("follower_id") follower_id: String,
        @Field("status") status: String,
        @Field("permission") permission: String
    ):Response<MessageRes>

    @POST("user/updatePlan")
    suspend fun addImages(
        @Body requestBody: RequestBody,
    @Header ("Accept") accept:String="application/json"
    ):Response<MessageRes>


    @FormUrlEncoded
    @POST("user/deletePlanImage")
    suspend fun deletePlanImage(
        @Field("plan_id") plan_id: String,
        @Field("image_id") image_id: String,
        @Header ("Accept") accept:String="application/json"
    ):Response<MessageRes>

    @POST("user/addRoute")
    suspend fun addRoute(
        @Body requestBody: RequestBody,
        @Header ("Accept") accept:String="application/json"
    ):Response<MessageRes>



    @FormUrlEncoded
    @POST("user/deleteRoute")
    suspend fun deleteRoute(
        @Field("route_id") route_id: String,
        @Header ("Accept") accept:String="application/json"
    ):Response<MessageRes>


    @FormUrlEncoded
    @POST("user/otheruserDetail")
    suspend fun otherDetails(
        @Field("user_id") user_id: String,
        @Header ("Accept") accept:String="application/json"
    ):Response<UserDetailsRes>

   //

    @FormUrlEncoded
    @POST("user/removefollowers")
    suspend fun removefollowers(
        @Field("follower_id") user_id: String,
        @Header ("Accept") accept:String="application/json"
    ):Response<MessageRes>


    @FormUrlEncoded
    @POST("user/removePlanFollower")
    suspend fun removePlanFollower(
        @Field("follower_id") follower_id: String,
        @Field("plan_id") plan_id: String
        ):Response<MessageRes>

}