package com.letsdetour.utils.retrofit

import java.text.SimpleDateFormat
import java.util.*

object WebUrls {
    val BASE_URL = "https://dev.appmantechnologies.com/letsdetour/api/"
    val PLACES_API_URL = "https://maps.googleapis.com/maps/api/"
    val IMAGE_URL = "https://dev.appmantechnologies.com/letsdetour/api/"
}