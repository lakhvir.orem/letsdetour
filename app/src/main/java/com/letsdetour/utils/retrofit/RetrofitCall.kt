package com.letsdetour.utils.retrofit

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.letsdetour.R
import com.letsdetour.utils.CommonAlerts
import com.letsdetour.utils.PrefferenceFile
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object RetrofitCall{
    lateinit var okHttpClient: OkHttpClient
    fun <T> callService(
        mContext: Context,
        dialogFlag: Boolean,
        token: String,
        requestProcessor: RequestProcess<T>
    ) {
        try {
            if (dialogFlag) {
                showDialog(mContext)
            }
            val gson = GsonBuilder()
                .setLenient()
                .create()

            if (token.isEmpty()) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                okHttpClient = OkHttpClient.Builder()
                        .addInterceptor(interceptor)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .build()


            } else {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .readTimeout(30, TimeUnit.MINUTES)
                    .connectTimeout(30, TimeUnit.MINUTES)
                    .addInterceptor { chain ->
                        val original = chain.request()

                        val request = original.newBuilder()
                            .header("Authorization", "$token")
                            .method(original.method(), original.body())
                            .build()

                        chain.proceed(request)
                    }

                okHttpClient = client.build()

            }
            val retrofit = Retrofit.Builder()
                .baseUrl(WebUrls.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()


            val retrofitApi = retrofit.create(RetrofitApi::class.java)

            val coRoutineExceptionHandler = CoroutineExceptionHandler { _, t ->
                t.printStackTrace()

                CoroutineScope(Dispatchers.Main).launch {
                    hideDialog()
                    t.message?.let { requestProcessor.onException(it) }

                    if (t.message.equals("Unable to resolve host")) {

                        CommonAlerts.alert(
                            mContext, "Unable to resolve host"
                        )

                    } else { //timeout

                        Log.e("UrlofApi's", "====>>>" + WebUrls.BASE_URL)
                        Log.e("TimeOut", " Error ------- $t")

                        CommonAlerts.alert(
                            mContext, mContext.resources.getString(R.string.timeout)
                        )
                        hideDialog()

                    }

                }
            }

            CoroutineScope(Dispatchers.IO + coRoutineExceptionHandler).launch {

                val response = requestProcessor.sendRequest(retrofitApi) as Response<*>

                CoroutineScope(Dispatchers.Main).launch {
                    Log.e("resCodeIs", "====${response.code()}")

                    if (response.isSuccessful) {
                        Log.e("resCodeIsisSuccessful", "==--->>>>==${response.code()}")
                        hideDialog()
                        requestProcessor.onResponse(response as T)
                    }

                    if (!response.isSuccessful) {
                        hideDialog()
                        Log.e("RetrofirRes", "====>>$response")
                        requestProcessor.onResponse(response as T)

                       /* if (response.code()==401){
                            PrefferenceFile.logout(mContext)
                        }*/
                        val res = response.errorBody()!!.string()
                        if (res == "Unauthorized") {
                            CommonAlerts.alert(
                                mContext, "Unauthorized"
                            )

                        } else {

                            try {

                                val jsonObject = JSONObject(res)

                                Log.e("TAG", "response..... : $jsonObject")

                                when {

                                    jsonObject.has("errors") -> {
                                        val errorObj = jsonObject.getJSONObject("errors")
                                        var message = errorObj.getString("message")
                                        hideDialog()
                                        when (message) {
                                            "EMAIL_NOT_VERIFIED" -> {
                                                /* message =
                                                    mContext.resources.getString(R.string.please_verify_your_email)*/
                                            }
                                        }
                                        CommonAlerts.alert(
                                            mContext,
                                            message
                                        )
                                    }
                                    jsonObject.has("message") -> {
                                        val message = jsonObject.getString("message")

                                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()


                                        hideDialog()

                                    }
                                    else -> {
                                        CommonAlerts.alert(
                                            mContext,
                                            mContext.resources.getString(R.string.server_error)
                                        )
                                        hideDialog()

                                    }
                                }

                            }catch (e: Exception){
                                e.printStackTrace()
                            }

                        }

                    }
                }
            }
        } catch (e: Exception) {
            Log.e("TAG", "Server Error  $e")
            e.printStackTrace()
        }

    }
    private var customDialog : AlertDialog? = null

    private fun showDialog(mContext: Context){

        if (customDialog!=null && customDialog!!.isShowing){
            customDialog?.dismiss()
        }else{
            val customAlertBuilder = AlertDialog.Builder(mContext)
            val customAlertView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_progress, null)
            customAlertBuilder.setView(customAlertView)
            customAlertBuilder.setCancelable(false)
            customDialog = customAlertBuilder.create()
            /*val animation1: Animation = AnimationUtils.loadAnimation(
                mContext,
                R.anim.heartbeat
            )
            customAlertView.ivImage.startAnimation(animation1)*/

            customDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            customDialog!!.show()
        }

    }


    private fun hideDialog() {
        if (customDialog != null) {
            customDialog!!.dismiss()
        }
    }
}