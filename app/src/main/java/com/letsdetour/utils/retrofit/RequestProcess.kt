package com.letsdetour.utils.retrofit

interface RequestProcess <T> {

    suspend fun sendRequest(retrofitApi: RetrofitApi):T

    fun onResponse (res : T)

    fun onException (message : String)

}