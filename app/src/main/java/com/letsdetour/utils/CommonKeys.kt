package com.letsdetour.utils

object CommonKeys {

    const val OtherProfile="other_profile"
    const val Terms="terms"
    const val Email="email"
    const val Privacy="privacy"
    const val OTP="OTP"
    const val TOKEN="TOKEN"
    const val ISUSERLOGIN="ISUSETLOGIN"
    const val USER="USER"
    const val GENDER="gender"
    const val FOLLOWERS="FOLLOWERS"
    const val FOLLOWERSLIST="FOLLOWERS_LIST"
    const val FOLLOWINGS="FOLLOWINGS"
    const val FIRSTNAME="first_name"
    const val USERID="USERID"
    const val LASTNAME="last_name"
    const val IMAGE="image"
    const val FeedId="FeedId"
    const val FeedData="FeedData"
    const val PlanData="FeedData"
    const val GOOGLE_PLACES_REQUEST_CODE = 1010
    const val TRAEVELID="travelId"
    const val TRAEVELID2="travelId2"
    const val DateID="DateID"
    const val PlanDate="PlanDate"
    const val Position="Position"
    const val DatesList="DatesList"

}