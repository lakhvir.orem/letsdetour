package com.letsdetour.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import com.letsdetour.R
import com.letsdetour.databinding.CommonAlertBinding


object CommonAlerts {

    fun alert(context: Context,message : String){
        try{
            val builder = AlertDialog.Builder(context)
            val layout = CommonAlertBinding.inflate(LayoutInflater.from(context),null)
            builder.setCancelable(false)
            builder.setView(layout.root)
            val tvMessage = layout.tvMessage
            val clOk = layout.clOk
            val dialog = builder.create()

            tvMessage.text = message

            clOk.setOnClickListener {
                dialog.dismiss()
            }

            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }catch (e:Exception){
            e.printStackTrace()
        }
    }


    fun alertIntent(context: Context,message : String, intent : Class<*>){
        try{
            val builder = AlertDialog.Builder(context)
            val layout = CommonAlertBinding.inflate(LayoutInflater.from(context),null)
            builder.setCancelable(false)
            builder.setView(layout.root)
            val tvMessage = layout.tvMessage
            val clOk = layout.clOk
            val dialog = builder.create()

            tvMessage.text = message

            clOk.setOnClickListener {
                dialog.dismiss()
                (context as Activity).startActivity(Intent(context,intent))
                context.finishAffinity()
            }

            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }catch (e:Exception){
            e.printStackTrace()
        }
    }


/*    fun alertIntentData(
        context: Context,
        message: String, intent: Class<*>,
        completeProfileDataClass : CompleteProfileDataClass
    ){
        try{
            val builder = AlertDialog.Builder(context)
            val layout = CommonAlertBinding.inflate(LayoutInflater.from(context),null)
            builder.setCancelable(false)
            builder.setView(layout.root)
            val tvMessage = layout.tvMessage
            val clOk = layout.clOk
            val dialog = builder.create()

            tvMessage.text = message

            clOk.setOnClickListener {
                dialog.dismiss()
                Intent(context,intent).also {
                    it.putExtra("completeProfile",completeProfileDataClass)
                    (context as Activity).startActivity(it)
                    context.finish()
                }
            }

            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }catch (e:Exception){
            e.printStackTrace()
        }
    }*/



    /**
     * Loader Alert
     * */
    private var customDialog : AlertDialog? = null
    fun showLoader(mContext: Context){
        try {
            val customAlertBuilder = AlertDialog.Builder(mContext)
            val customAlertView =
                LayoutInflater.from(mContext).inflate(R.layout.dialog_progress, null)
            customAlertBuilder.setView(customAlertView)
            customAlertBuilder.setCancelable(false)
            customDialog = customAlertBuilder.create()
            /*val animation1: Animation = AnimationUtils.loadAnimation(
                mContext,
                R.anim.heartbeat
            )
            customAlertView.ivImage.startAnimation(animation1)*/

            customDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            customDialog!!.show()
        }catch (e:Exception){
            e.printStackTrace()
        }
    }


    fun hideLoader() {
        try {
            if (customDialog != null) {
                customDialog!!.dismiss()
            }
        }catch (e:Exception){
            e.printStackTrace()
        }
    }


}