package com.letsdetour.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class TravelPlanDetailsRes(
    val `data`: Data,
    val message: String
) {
    data class Data(
        val address: String,
        val created_at: String,
        val dates: List<Date>,
        val email_verify_token: String,
        val end_date: String,
        val followers: List<Followers>,
        val id: Int,
        val images: List<Images>,
        val lat: String,
        val lng: String,
        val permission: Any,
        val place_id: String,
        val start_date: String,
        val updated_at: String,
        val user_id: Int
    ) {
        @Parcelize
        data class Date(
            val created_at: String,
            val id: Int,
            val plan_date: String,
            val plan_id: Int,
            val routes: List<Route>,
            val updated_at: String
        ):Parcelable
        {
            @Parcelize
            data class Route(
                val id: Int,
                val user_id: Int,
                var name: String,
                var date_id: Int,
                var image: String,
                var address: String,
                var lat: String,
                var lng: String,
                var route_time: String,
                val created_at: String,
                val updated_at: String
            ):Parcelable
        }

        data class Images(
            val created_at: String,
            val id: Int,
            val user_id: Int,
            val plan_id: Int,
            var image: String,
            val updated_at: String
        )
        @Parcelize
        data class Followers(
            val created_at: String,
            val id: Int,
            val user_id: Int,
            val plan_id: Int,
            var follower_id: String,
            val status: Int,
            val permission: Int,
            val user_name: String,
            var user_image: String?,
            val updated_at: String
        ):Parcelable
    }
}