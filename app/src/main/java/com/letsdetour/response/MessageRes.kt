package com.letsdetour.response

data class MessageRes(
    val message: String
)