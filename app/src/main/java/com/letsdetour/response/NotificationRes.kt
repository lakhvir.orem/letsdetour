package com.letsdetour.response

data class NotificationRes(
    val `data`: List<Data>,
    val message: String
) {
    data class Data(
        val created_at: String,
        val description: String,
        val id: Int,
        val request_status: Int,
        val sender_id: Int,
        var status: Int,
        val status_id: Int,
        val title: String,
        val type: Int,
        val updated_at: String,
        val user_id: Int,
        val user_image: String,
        val user_name: Any
    )
}