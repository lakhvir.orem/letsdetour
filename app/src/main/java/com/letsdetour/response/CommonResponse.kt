package com.letsdetour.response


import androidx.annotation.Keep

@Keep
data class CommonResponse(
    val message: String, // You have been registered successfully and email verification sent on your email .
    val status: Int // 200
)