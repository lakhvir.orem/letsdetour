package com.letsdetour.response

data class AddCommentRes(
    val `data`: Data,
    val message: String
) {
    data class Data(
        val address: String,
        val comments: List<Comment>,
        val created_at: String,
        val detail: String,
        val follow_status: String,
        val id: Int,
        val image: String,
        val lat: String,
        val like_status: Int,
        val lng: String,
        val total_comments: String,
        val total_likes: String,
        val total_unlikes: String,
        val unlike_status: Int,
        val updated_at: String,
        val user_id: Int,
        val user_image: String,
        val user_name: String
    ) {
        data class Comment(
            val comment: String,
            val created_at: String,
            val feed_id: Int,
            val id: Int,
            val updated_at: String,
            val user_id: Int,
            val user_image: String,
            val user_name: String
        )
    }
}