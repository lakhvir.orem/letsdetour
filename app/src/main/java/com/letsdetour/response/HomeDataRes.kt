package com.letsdetour.response

data class HomeDataRes(
    val `data`: Data,
    val message: String
) {
    data class Data(
        val feeds: List<Feed>,
        val places: List<Place>
    ) {
        data class Feed(
            val address: String,
            val comments: List<Comment>,
            val created_at: String,
            val detail: String,
            var follow_status: String,
            val id: Int,
            val image: String,
            val lat: String,
            var like_status: Int,
            val lng: String,
            val total_comments: String,
            var total_likes: Int,
            val total_unlikes: String,
            val unlike_status: Int,
            val updated_at: String,
            val user_id: Int,
            val user_image: String,
            val user_name: String
        ) {
            data class Comment(
                val comment: String,
                val created_at: String,
                val feed_id: Int,
                val id: Int,
                val updated_at: String,
                val user_id: Int,
                val user_image: String,
                val user_name: String
            )
        }

        data class Place(
            val avg_rating: String,
            val created_at: String,
            val distance: Double,
            val follow_status: String,
            val id: Int,
            val image: String,
            val lat: String,
            val lng: String,
            val location: String,
            val name: String,
            val type: Int,
            val updated_at: String
        )
    }
}