package com.letsdetour.response

data class CommonLoginRes(
    val message: String,
    val token: String,
    val user: User
) {
    data class User(
        val availability: Int,
        val bio: String,
        val city: String,
        val country: String,
        val country_code: Int,
        val cover_image: String,
        val created_at: String,
        val current_lat: String,
        val current_lng: String,
        val device_token: String,
        val device_type: String,
        val email: String,
        val email_verify_token: String,
        val fcm_token: String,
        val feeds: List<Feed>,
        val followers: List<Follower>,
        val followings: List<Following>,
        val full_name: String,
        val id: Int,
        val image: String,
        val location: String,
        val notification: Int,
        val notification_count: Int,
        val online: Int,
        val phone: String,
        val privacy: Int,
        val social_id: String,
        val social_type: Int,
        val socket_id: String,
        val state: String,
        val status: Int,
        val total_followers: String,
        val total_following: String,
        val updated_at: String,
        val user_name: String,
        val user_type: Int,
        val verify: Int
    ) {
        data class Feed(
            val address: String,
            val created_at: String,
            val detail: String,
            val id: Int,
            val image: String,
            val lat: String,
            val lng: String,
            val updated_at: String,
            val user_id: Int
        )

        data class Follower(
            val created_at: String,
            val follower_id: Int,
            val id: Int,
            val status: Int,
            val updated_at: String,
            val user_id: Int
        )

        data class Following(
            val created_at: String,
            val follower_id: Int,
            val id: Int,
            val status: Int,
            val updated_at: String,
            val user_id: Int
        )
    }
}