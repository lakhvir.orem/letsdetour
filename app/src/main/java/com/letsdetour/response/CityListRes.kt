package com.letsdetour.response

data class CityListRes(
    val `data`: List<Data>,
    val message: String
) {
    data class Data(
        val id: Int,
        val name: String,
        val state_id: Int
    )
}