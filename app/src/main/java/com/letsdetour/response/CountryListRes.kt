package com.letsdetour.response

data class CountryListRes(
    val `data`: List<Data>,
    val message: String
) {
    data class Data(
        val id: Int,
        val name: String,
        val phonecode: Int,
        val sortname: String
    )
}