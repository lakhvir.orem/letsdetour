package com.letsdetour.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class FollowerListRes(
    val `data`: List<Data>,
    val message: String
) {
    @Parcelize
    data class Data(
        var created_at: String="",
        var follower_id: Int=0,
        var id: Int=0,
        var status: Int=0,
        var updated_at: String="",
        var user_id: Int=0,
        var user_image: String="",
        var isChecked: Boolean=false,
        var user_name: String=""
    ):Parcelable
}