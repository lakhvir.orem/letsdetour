package com.letsdetour.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

data class GetPlanListRes(
    val `data`: List<Data>,
    val message: String
) {
    @Parcelize
    data class Data(
        var address: String="",
        val created_at: String="",
        val dates: @RawValue Any? = null,
       // val dates: List<Date>,
        val email_verify_token: String="",
        var end_date: String="",
        val followers: @RawValue Any?=null,
        var id: Int=0,
        val images: @RawValue Any?=null,
        var lat: String="",
        var lng: String="",
        val permission: @RawValue Any?=null,
        var place_id: String="",
        var start_date: String="",
        val updated_at: String="",
        val user_id: Int=0
    ):Parcelable {
        data class Date(
            val created_at: String,
            val id: Int,
            val plan_date: String,
            val plan_id: Int,
            val routes: List<Any>,
            val updated_at: String
        )
    }
}